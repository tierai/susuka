#!/usr/bin/php
<?php

/**
 * @file
 * 
 * This is a quick hack that merges all lib files into one big file.
 * 
 * This script has to be called from the ROOT of the project, like:
 * $ bin/mkdist.php --lib=lib/susuka > lib/susuka/autoload.dist.php
 * 
 * You have to edit your app.php or .user.php with something like this:
 *   define('SU_AUTOLOAD_INCLUDE', '../lib/susuka/autoload.dist.php');
 *   define('SU_LIB_PATH', __DIR__.'/../lib/');
 *   define('SU_APP_ENVIRONMENT', 'production');
 * 
 * @todo Use code comments to exclude things, example:
 *       /// @mkdist env=dev
 *       class DevOnly ...
 * @todo __FILE__, __LINE__
 * @todo Modules and App support
 * @todo Remove logging
 * @todo Remove comments
 * @todo Selective includes (some sites may not need the data dir)
 * @todo There's probably an existing library that does this better
 */
 
date_default_timezone_set('UTC');

function usage() {
    throw new exception('fixme');
}

class Tokenizer implements \Iterator {
    protected $key;
    protected $tokens;
    
    public function __construct($source) {
        $this->tokens = \token_get_all($source);
    }
    
    public function current() {
        if(!isset($this->tokens[$this->key])) throw new Excpetion;
        if(is_array($this->tokens[$this->key])) {
            return $this->tokens[$this->key];
        }
        return array(T_STRING, $this->tokens[$this->key], -1);
    }
    
    public function key() {
        return $this->key;
    }
    
    public function next() {
        $this->key++;
    }
    
    public function rewind() {
        $this->key = 0;
    }
    
    public function valid() {
        return $this->key < count($this->tokens);
    }
    
    public function get() {
        $this->next();
        return $this->current();
    }
}

function merge($dir) {
    $result = array(
        'files' => array(),
        'class' => array(),
    );
    $it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir, \RecursiveIteratorIterator::SELF_FIRST));
    foreach($it as $file) {
        if(!is_file($file)) continue;
        $info = pathinfo($file);
        //if($info['basename'] != 'test.php') continue;
        switch($info['extension']) {
        case 'php':
            $content = file_get_contents($file);
            $tokens = new Tokenizer($content);
            $class = false;
            $namespace = '';
            $uses = array();
            $depends = array();
            $source = array();
            $ids = array();
            foreach($tokens as $token) {
                list($id, $text) = $token;
                $ids[$id] = true;
                switch($id) {
                case T_NAMESPACE:
                    $source[] = $text;
                    while(list($id, $text) = $tokens->get()) {
                        $source[] = $text;
                        if($text == ';' || $text == '{') break;
                        $namespace .= trim($text);
                    }
                    break;
                case T_USE:
                    $source[] = $text;
                    if(!isset($ids[T_CLASS]) && !isset($ids[T_FUNCTION])) {
                        $as = false;
                        $key = count($uses);
                        $name = '';
                        while(list($id, $text) = $tokens->get()) {
                            $source[] = $text;
                            if($text == ';') break;
                            switch($id) {
                            case T_AS:
                                $as = true;
                                $key = '';
                                continue;
                            case T_NS_SEPARATOR:
                            case T_WHITESPACE:
                            case T_STRING:
                                if($as) $key .= $text;
                                else $name .= $text;
                                break;
                            default:
                                throw new exception('fixme:'.token_name($id));
                            }
                        }
                        $uses[trim($key)] = trim($name);
                    }
                    break;
                case T_INTERFACE:
                case T_CLASS:
                    $source[] = $text;
                    while(list($id, $text) = $tokens->get()) {
                        $source[] = $text;  
                        if($class) break;
                        switch($id) {
                        case T_WHITESPACE:
                            break;
                        case T_STRING:
                            $class = $text;
                            break;
                        default:
                            throw new exception('fixme:'.token_name($id));
                        }
                    }
                    break;
                case T_EXTENDS:
                case T_IMPLEMENTS:
                    $source[] = $text;
                    $depends[] = '';
                    while(list($id, $text) = $tokens->get()) {
                        $source[] = $text;
                        if($text == '{') break;
                        switch($id) {
                        case T_WHITESPACE:
                            break;
                        case T_NS_SEPARATOR:
                        case T_STRING:
                            if($text == ',') {
                                $depends[] = '';
                            } else {
                                $depends[count($depends)-1] .= trim($text);
                            }
                            break;
                        default:
                            throw new exception('fixme:'.token_name($id));
                        }
                    }
                    break;
                /*case T_IF:
                    $source[] = $text;
                    while(list($id, $text) = $tokens->get()) {
                        $temp .= $text;
                        echo token_name($id).':'.$text."\n";
                        if(in_array($text, array('{', ';'))) break;
                        switch($id) {
                        case T_WHITESPACE:
                            continue;
                        case T_STRING:
                            #echo "S=$text,\n";
                            continue;
                        default:
                            break;
                        }
                    }
                    $source[] = $temp;
                    break;*/
                case T_COMMENT:
                case T_DOC_COMMENT:
                    break;
                default:
                    $source[] = $text;
                }
            }
            
            if($class === false) continue;
            
            foreach($depends as $k => &$v) {
                if(isset($uses[$v])) {
                    $v = '\\'.$uses[$v];
                } else if($v[0] != '\\') {
                    $v = '\\'.$namespace.'\\'.$v;
                }
            }
            
            $content = implode('', $source);
            $content = preg_replace('/^\<\?php.*/', '', $content);
            $content = preg_replace('/^namespace (.*);/m', 'namespace $1 {', $content, -1, $count);
            if($count) {
                $content .= '}'.PHP_EOL;
            } else {
                $content = "namespace {\n$content }\n";
            }
            // Replace __DIR__ with lib-path
            // FIXME
            $dir = substr($info['dirname'], 3);
            $dir = 'SU_LIB_PATH.\''.$dir.'/\'';
            $content = preg_replace('/__DIR__/', $dir, $content);
            $content = preg_replace('/if\(SU_LOG\)(.*);/', '//NOLOG', $content);
            $class = $class ? '\\'.$namespace.'\\'.$class : false;
            $result['class'][$class] = (string)$file;
            $result['files'][(string)$file] = array(
                'file' => (string)$file,
                'class' => $class,
                'depends' => $depends,
                'source' => $content,
            );
            break;
        default:
            #echo "// ignore: $file\n";
            break;
        }
    }
    return $result;
}

$options = getopt('', array(
    'all::',
    'lib::',
));

if(isset($options['all'])) {
    $options['lib'] = glob('lib/su*', GLOB_ONLYDIR);
}

if(isset($options['lib'])) {
    if(!is_array($options['lib'])) $options['lib'] = array($options['lib']);
    foreach($options['lib'] as $lib) {
        build($lib);
    }
} else {
    usage();
    exit(1);
}

function write(&$result, $key) {
    if(!isset($result['files'][$key])) return;
    $item = $result['files'][$key];
    //echo "// class = '${item['class']}'\n";
    foreach($item['depends'] as $dep) {
        //echo "// depend = '$dep'\n";
        if(isset($result['class'][$dep])) {
            $dep = $result['class'][$dep];
            if(isset($result['files'][$dep])) {
                write($result, $dep);
            } else {
                //echo "// dep not found: '$dep'\n";
            }
        }
    }
    //echo '// '.$item['file'].PHP_EOL;
    echo trim($item['source']).PHP_EOL;
    unset($result['files'][$key]);
}

function build($lib) {

    $result = merge($lib);

    echo '<?php //'.date(DATE_ATOM).PHP_EOL;
    
    // Include these files before any other
    $files = array(
        $lib.'/autoload.php',
    );
    
    // TODO: $options..
    if($lib == 'lib/susuka') {
        $version_file = $lib.'/version.txt';
        $version = is_file($version_file) ? trim(file_get_contents($version_file)) : '1.0-git';

        $files[] = 'lib/susuka/core/Application.php';

        // Dont include these
        $ignore = array(
            'lib/susuka.lib.php',
            'lib/susuka/log/autoload.php',
            'lib/susuka/log/Access.php',
            'lib/susuka/log/Log.php',
            'lib/susuka/log/Security.php',
        );

        foreach($ignore as $k) {
            unset($result['files'][$k]);
        }
        
        echo 'namespace {'.PHP_EOL;
        echo 'if(!defined(\'SU_LOG\')) define(\'SU_LOG\', 0);'.PHP_EOL;
        echo 'if(!defined(\'SU_VERSION\')) define(\'SU_VERSION\', \''.$version.'\');'.PHP_EOL;
        echo '}'.PHP_EOL;
        
    }

    foreach($files as $k) {
        if(!isset($result['files'][$k])) continue;
        // FIXME: Generic include removal
        $result['files'][$k]['source'] = str_replace('require_once \'core/Autoloader.php\';', '//', $result['files'][$k]['source']);
        $result['files'][$k]['source'] = str_replace('require_once SU_LIB_PATH', '//', $result['files'][$k]['source']);
        $result['files'][$k]['source'] = str_replace('if(!defined(\'SU_VERSION\'))', '//', $result['files'][$k]['source']);
        write($result, $k);
    }

    foreach(array_keys($result['files']) as $key) {
        write($result, $key);
    }

}
