<?php

namespace demo\module\local\controller;

class Index extends \susuka\controller\View {
    
    function indexAction() {
        return array('$title' => 'Welcome');
    }
    
    /**
     * Example action, demonstrates some the basics of an action and it's view.
     */
    function exampleAction() {
        return array('$title' => 'Example', '$message' => 'Hello Example View!');
    }
    
    function aboutAction() {
        return array('$title' => 'About');
    }
}
