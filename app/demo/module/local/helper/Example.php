<?php

namespace demo\module\local\helper;

class Example extends \susuka\helper\View {
    protected $title;
    protected $message;
    
    function __construct($title, $message) {
        $this->title = $title;
        $this->message;
    }
    
    function __toString() {
        $view = $this->getView();
        return sprintf('<b>%s</b><p>%s</p><br />', $view->escape($this->title), $view->escape($this->message));
    }
}
