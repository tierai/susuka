<?php

/**
 * Example router configuration
 * 
 * Routes are processed in order, the first matching route will be used.
 * Regex routes without a prefix cannot be used to build an URI.
 * 
 * @note Like configuration files, any logic here will only run when the routing cache is rebuilt.
 * 
 * @todo Default values (prefix = '/path/:stuff/:id', defaults = ['id' => 0], would match /path/moo and /path/moo/123
 * @todo How to we send stuff to action method (order of params)..
 * 
 * @todo Hmm, split target => rewrite, controller?
 * 
 * @TODO: mod-demo/routes.php, mmenu stuff
 */
 
// Xml format (test)
/*$this->route(null, array(
    'pattern' => '^(?<path>[.*]+).rss$',
    'target' => array('$path', 'format' => 'rss'),
));*/

// Admin route
$this->route('_admin', array(
    'prefix' => '/admin/',
    'target' => '/_admin/$suffix',
    'import' => '@admin/config/routes.php',
));

// Error route
$this->route('_error', array(
    'prefix' => '/error/',
    'target' => '/_error/$suffix',
    'import' => '@error/config/routes.php',
));

// Local route
$this->route('_local', array(
    'format' => '/local/:action',
    'target' => array('@local/controller/Index', 'action' => 'index'),
));

// Demo route
$this->route('_demo', array(
    'prefix' => '/demo/',
    'target' => '/_demo/$suffix',
    'import' => '@demo/config/routes.php',
));

// Wiki route
$this->route('_wiki', array(
    'prefix' => array('/wiki/', '/_wiki/'), // match any of these prefixes
    'target' => array('/wiki/$suffix', '/_wiki/$suffix'), // rewrites the first (/w/) prefix into /wiki/ so it can be matched by wiki routes.php, true does not rewrite the /_wiki/ path (?????)
    'import' => '@wiki/config/routes.php', // continue processing routes from wiki module
));

// Create the index route
$this->route('_index', array(
    'format' => '/',
    'target' => array('@local/controller/Index', 'action' => 'index'),
));

