<?php

/**
 * @file
 *
 * Demo configuration
 *
 * @note Any logic in this file will not run when the configuration is loaded from cache!
 *       You can use Application::initConfig to dynamically override configuration values.
 */

/** Set the application title */
$this->set('app.title', 'SuCMS Demo');

/** Set the hostname, required if you're using subdomain routing */
#$this->set('app.hostname', 'susuka.lan');

/** Configure modules */
$this->set('app.module', array(
    'local' => array(
        'enable' => true,
    ),
    'demo' => array(
        'enable' => true,
        'path' => 'susuka-module-demo',
    ),
    'error' => array(
        'enable' => SU_APP_ENVIRONMENT === 'production',
        'path' => 'susuka-module-error',
    ),
    'wiki' => array(
        'enable' => true,
        'path' => 'susuka-module-wiki',
    ),
    'admin' => array(
        'enable' => true,
        'path' => 'susuka-module-admin',
    ),
));

$this->set('app.menu', array(
    array(
        'title' => 'Sususka CMS',
        'route' => '_index',
    ),
    array(
        'title' => 'Home',
        'route' => '_index',
    ),
    array(
        'title' => 'HelperExample',
        'route' => array('_local', 'action' => 'example'),
    ),
    array(
        'title' => 'DemoModule',
        'route' => '_demo',
    ),
    array(
        'title' => 'WikiPage',
        'route' => array('_wiki', 'page' => 'demopage'),
    ),
    array(
        'title' => 'About',
        'route' => array('_local', 'action' => 'about'),
    ),
));

$this->set('app.menu-footer', array(
    array(
        'title' => 'Admin',
        'route' => '_admin',
    ),
    array(
        'html' => '<a href="http://metrica.se/" title="A product by Metrica.SE">&#169; '.date('Y').'</a>',
    ),
));
