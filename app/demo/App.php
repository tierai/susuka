<?php

namespace demo;

class App extends \susuka\core\Application {
    
    protected function initController($instance) {
        $instance->setViewFormat('application/xhtml+xml');
    }
    
    protected function initView($instance) {
        if($instance instanceof \susuka\view\Html) {
            $instance->setDocType(\susuka\view\Html::DOCTYPE_XHTML1_TRANSITIONAL);
            $instance->addStyle('http://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,400italic');
            $instance->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js');
        }
    }
    
    protected function initAutoloader($instance) {
        $instance->addAlias('$helper', '\\'.SU_APP_NAME.'\\module\\local\\helper');
        parent::initAutoloader($instance);
    }

    /**
     * @note You should override this on production sites (here or in config).
     */
    protected function createCache($config) {
        #return new \susuka\cache\Apc($config);
        return parent::createCache($config);
    }
}
