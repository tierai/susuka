SuMVC - Another (Model)ViewController framework for PHP
=======================================================

Requirements
============
Nginx or Apache
PHP 5.3.5+

Recommended
===========
mod\_rewrite
APC
MySQL or SQLite (if db-storage is needed)
Xdebug for development

Installation
============
TODO: Verify all commands written here, they are just taken from memory..

## Local development domain (optional)
### Dnsmasq (recommended)
$ su -c 'echo "address=/dev/127.0.0.1" >> /etc/dnsmasq.conf'

### Hosts file
$ su -c 'echo "susuka.dev 127.0.01" >> /etc/hosts'

## Nginx configuration
TODO: securing nginx and PHP.
``
server {
    server_name susuka.dev;
    root /srv/www/$server_name/public;

    location / {
        index  app.php;
        try_files $uri $uri/ /app.php;
    }

    location ~ app\.php$ {
        fastcgi_param SU_APP_NAME demo;
        # uncomment these lines for production servers
        #fastcgi_param SU_LOG 0;
        #fastcgi_param SU_APP_ENVIRONMENT production;
        include php.conf;
    }
}
``

## Apache configuration
TODO: Document

TODO: bin/console should take care of log and asset dirs and permissions

1. Clone the stable Git repository
   - $ git clone FIXME-URI-HERE/susuka.git mysite
?. Create a domain for development
   - Use DNS or add mysite.lan to /etc/hosts
?. Configure Apache
   - Add vhost mysite.lan using pubdir mysite/public 
   - Reload apache
?. Init the library (optional)
   - $ bin/console init
?. Create your own app (optional)
   - $ bin/console app new myapp
?. Configure your application
   - Edit the following files in app/myapp to suit your needs
      - config/\*.php
      - layout/\*.phtml 
      - asset/\*
   - Create app/myapp/log and make sure PHP can write to it
   - If you're using SQLite, create app/myapp/db and make sure PHP can write to it
?. Build your site
   - Create public/asset and make sure PHP can write to it
   - $ bin/console site mysite.lan build
?. Visit your site
   - Go to mysite.lan

Goals
=====
Lightweight(-ish)
Few dependencies
Easy to deploy
Multiple sites/apps, one source tree
Fast (enough)
Model is optional (a basic Model implementaion is supplied, but user is free to use anything)
Favor simplicity
It's a PHP-framework, templates & configurations are written in PHP
No generated code (except for caching)

TODO
====

global:
- Move apps to own repos
- Easy config to enable/disable components (auth methods, markup languages) (ACL!?)
- Module class?
- No Hardcoded paths or default values (use config or constants)
- Avoid global cache, specify a cache instance for each system.
- Default strings

console:
- Replace HTTP-requests with CLI (so assets can be published without having to configure or know the site URL).

application:
- component initialization (some has a ::parent call, some dont but might have in the future, this can break old App.php scripts)
- cleanup
- fix/cleanup module loading

auth:
- storage
- multiple logins

cache:
- remove ns, allow array keys
- get method that takes an array of files and checks if any file has been modified

controller:
- view cleanup
- API auth key (automatic for form requests and static for public APIs)
- API-controller vs Xml/Json views, we don't need both.

core:
- Interfaces, move some stuff
- Optional module dependencies (loaded if enabled)

data:
- Fix ugly dependencies...
- Implement dynamic models
- How do we deal with indexes in Model::$values?

router:
- Route.php cleanup and documentation
- Trailing / (for prefix... does /stuff match only /stuff or /stuffabc too?, does /stuff/ match /stuff?)

session:
- database storage

view:
- Allow other templates?
- ContentType, SU_VIEW_FORMAT cleanup

module-user:
- Feed (blog, posts, twitter, yt, etc..)
- Multi-signin:
  - Dropdown menu to change default account
  - "View as user" link, opens page with ?viewAsUserId=123 (so we can have
     multiple tabs with different users open)

module-feed?:
- User feeds (posts, comments, all site activity...)
- External feeds (yt, twitter...)
- User can follow other feeds ("feed-feeds", forums, topics...)
- User can create custom feeds from multiple sources

Plugins
- Adds stuff, like a developer toolbar
- Different from libs as they are automatically added (if enabled) instead of programmatically
- Callbacks/events?
- Helpers (view, controller, stuff)

