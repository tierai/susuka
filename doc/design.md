# Design philosophy
This core framework does not enforce any specific design pattern for the user interface,
but the helper modules does. This document is meant to be used as a guideline for these UI elements.


## JS/AJAX links
Any link that triggers something through JavaScript (load another page, display something) should have a normal URI as a fallback, so it can be opened in a new window or bookmarked.

### Bad
- <a href="#">...

### Good
- <a href="fallback/url">... $(...).click(...)


## Pagination
...

### Alternative to endless/infinite scrolling
Do not use endless scrolling, it rarely works reliably, it's hard to control.
The alternative is: When the user scrolls to the bottom of a page, display a "load more" or "next page" button. This allow the user to see the footer and have better control.


