<?php

namespace susuka\controller;

/**
 * API controller
 * 
 * Can be used to create a RESTful or similar Web-API.
 * 
 * @todo Move method detection to router?
 * @todo Client key validation (in base controller?)
 * @todo header() & output format (XML)
 * @todo HTTP return code
 * @todo Multipart data.. files..
 */
class Api extends Controller {
    public function render() {
        $action = strtolower($_SERVER['REQUEST_METHOD']);
        
        switch($action) {
        case 'get':
            $args = $_GET;
            break;
        case 'post':
            $args = $_POST;
            break;
        default:
            $input = file_get_contents('php://input');
            parse_str($input, $args);
        }
        
        // TODO: We should not rely on get args here? (this is a Router issue, it should not set controller args as get params...)
        $args = array_merge($this->request->getParam('get'), $args);
        $result = $this->action($action, $args);
        
        if(is_array($result)) {
            return $this->renderArray($result);
        }
        
        return $result;
    }
    
    protected function renderArray(array $result) {
        $this->setContentType('application/json');
        return json_encode($result);
    }
}
