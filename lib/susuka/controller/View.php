<?php

namespace susuka\controller;

use \susuka\core\Registry;

/**
 * ViewController.
 * All View controllers should extend this class.
 * 
 * @todo Remove SU_VIEW_FORMAT constant and merge detectViewFormat with Controller::detectContentType
 */
class View extends Controller {
    static $defaultConfig = array(
        'layout' => 'layout/default',
        'content' => 'content', // Layout section where the view content will be placed.
        'format' => null,
    );
    
    public function __construct(array $config) {
        parent::__construct(array_merge($config, self::$defaultConfig));
    }
    
    /**
     * @todo Arguments & $_GET/$_POST ...
     * @todo Store action as a member?
     * @todo Allow array return by param?
     */
    public function render() {
        $args = $this->request->getParam('get');
        $action = $this->request->getGet('action');
        
        $result = $this->action($action, $args);
        
        if(is_array($result)) {
            $result = $this->renderView($action, $result);
        }
        
        return $result;
    }
    
    protected function detectContentType() {
        // FIXME: no define
        if($type = suConstant('SU_VIEW_FORMAT')) {
            if(isset($_SERVER['HTTP_ACCEPT']) && stristr($_SERVER['HTTP_ACCEPT'], $type)) {
                return $type;
            } else {
                if(SU_LOG) \suLog::w('Serving %s as text/html', $type);
            }
        } else {
            if(SU_LOG) \suLog::w('ContentType not set');
        }
        return 'text/html';
    }
    
    /**
     * @todo Xml, Json
     * @todo Paths & extensions
     */
    protected function renderView($action, array $values) {
        $view = $this->createView($values);
        if($view instanceof \susuka\view\Html) {
            $controller = get_class($this);
            $controller = explode('\\', $controller);
            $controller = strtolower($controller[count($controller) - 1]);
            $view->addPath('@'.$this->config['module'].'/view/');
            $view->addPath('@'.$this->config['module'].'/view/'.$controller);
            $view->addPath('@'.$this->config['module'].'/view/'.$controller.'/'.$action);
            $view->setContentScript($action);
        }
        $view->set('#action', $action);
        return $view->render();
    }
    
    protected function createView(array $values) {
        $app = $this->getApplication();
        $format = $this->getViewFormat();
        $config = array(
            'class' => '\\susuka\\view\\Text',
            'router' => $this->getRouter(),
            'translator' => $app->getTranslator(),
            'values' => $values,
        );
        switch($format) {
        case 'text/html':
        case 'application/xhtml+xml':
            $config['class'] = '\\susuka\\view\\Html';
            $config['options'] = array(
                'layout' => $this->config['layout'],
                'mimeType' => $format,
            );
            $view = $app->getView($config);
            $view->addPath($this->config['module'].'/view');
            break;
        case 'text/plain':
        default:
            $view = $app->getView($config);
        }
        if(!defined('SU_VIEW_FORMAT')) {
            define('SU_VIEW_FORMAT', $view->getMimeType());
        }
        $view->set('#controller', $this);
        Registry::set('view', $view);
        $this->initView($view);
        return $view;
    }
    
    protected function initView($view) {
    }
    
    function setViewFormat($format) {
        if(isset($this->config['format']) && !empty($this->config['format'])) {
            return false;
        }
        $this->config['format'] = $format;
        return !empty($format);
    }
    
    function getViewFormat() {
        if(isset($this->config['format'])) {
            return $this->config['format'];
        }
        return $this->detectViewFormat();
    }
    
    protected function detectViewFormat() {
        # FIXME: Console hack
        if(strpos($_SERVER['HTTP_USER_AGENT'], 'Wget') === 0) {
            return 'text/plain';
        }
        if(defined('SU_VIEW_FORMAT')) {
            return SU_VIEW_FORMAT;
        }
        //return 'application/xhtml+xml';
        return 'text/html';
    }
}
