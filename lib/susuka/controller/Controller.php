<?php

namespace susuka\controller;

use \susuka\exception\Argument as ArgumentException;

/**
 * @todo View format 
 * @todo Annotated routes, views and stuff using getDocComment?
 * @todo $response member?
 * @todo Move view stuff to ViewController
 */
abstract class Controller {
    protected $config;
    protected $router;
    protected $request;
    protected $response;
    protected $application;
    protected $session;
    protected $contentType;
    protected $appConfig;
    
    public function __construct(array $config) {
        $this->config = $config;
    }
    
    public function getApplication() {
        return $this->application;
    }
    
    public function setApplication($value) {
        $this->application = $value;
        return $this;
    }
    
    public function getConfig() {
        if($this->config === null) $this->config = $this->getApplication()->getConfig();
        return $this->config;
    }
    
    public function setConfig($value) {
        $this->config = $value;
        return $this;
    }
    
    public function getAppConfig() {
        if($this->appConfig === null) $this->appConfig = $this->getApplication()->getConfig();
        return $this->appConfig;
    }
    
    public function setAppConfig($value) {
        $this->appConfig = $value;
        return $this;
    }
    
    public function getSession() {
        if($this->session === null) $this->session = $this->getApplication()->getSession();
        return $this->session;
    }
    
    public function setSession($value) {
        $this->session = $value;
        return $this;
    }
    
    function getRouter() {
        if($this->router === null) $this->router = $this->getApplication()->getRouter();
        return $this->router;
    }
    
    function setRouter($value) {
        $this->router = $value;
        return $this;
    }
    
    public function getRequest() {
        return $this->request;
    }
    
    public function setRequest($value) {
        $this->request = $value;
        return $this;
    }
    
    public function getResponse() {
        return $this->response;
    }
    
    public function setResponse($value) {
        $this->response = $value;
        return $this;
    }
    
    public function getContentType() {
        if($this->contentType === null) return $this->detectContentType();
        return $this->contentType;
    }
    
    public function setContentType($value) {
        $this->contentType = $value;
        return $this;
    }
    
    protected function detectContentType() {
        return 'text/plain';
    }
    
    /**
     * @todo Create internal subrequest instead
     */
    public function redirect($route, array $args = array()) {
        $this->httpRedirect($route, $args);
    }
    
    public function httpRedirect($route, array $args = array()) {
        if(strpos($route, '://') === false) {
            if(is_string($args)) $args = array('action' => $args);
            $url = $this->getRouter()->build($route, $args);
        } else {
            $url = $route;
        }
        header('Location: '.$url);
        die;
    }
    
    public function run() {
        $result = $this->render();
        if(!headers_sent() && ($type = $this->getContentType())) {
            header('Content-Type: '.$type);
        } else {
            if(SU_LOG) \suLog::w('Cannot send Content-Type, headers_sent=%s', headers_sent());
        }
        echo $result;
    }
    
    /**
     * Runs the controller action specified by the Request and returns the result.
     */
    abstract public function render();
    
    /**
     * Invoke the controller action for the current request.
     */
    protected function action($action, array $args) {
        $name = $name = is_numeric($action) ? '_'.$action.'Action' : $action.'Action';
        $method = new \ReflectionMethod($this, $name); // We need reflection to know the order of the arguments
        if(!$method->isPublic()) {
            ArgumentException::raise('The action "%s::%s" is not public!', get_class($this), $method->getName());
        }
        if($method->getName() !== $name) {
            // NOTE: We don't allow this because it casues problems on case-sensitive filesystems (and it's ugly)
            ArgumentException::raise('The action "%s::%s" doesn\'t exactly match "%s"!', get_class($this), $method->getName(), $name);
        }
        
        $methodArgs = array();
        foreach($method->getParameters() as $param) {
            // TODO: Check isOptional & getDefaultValue? (or can the router give us incomplete args?)
            $name = $param->getName();
            $methodArgs[] = isset($args[$name]) ? $args[$name] : NULL; // TODO: Allow this? (needed for $suffix atm)
        }
        
        return $method->invokeArgs($this, $methodArgs);
    }
    
}

