<?php

namespace susuka\markup;

/**
 * Markdownlike markup parser.
 * 
 * @todo Code ``
 * @todo Whitespace/tab indention?
 * @todo Join blockquote paragraphs & lists "> abc\n> def" is one block, "> abc\n>\n>def" is two.
 * @todo Custom tags
 * @todo Simpler quoting: @user\n"blablabla", or "blablabla"\n-- derpymcderp
 *       ^ PROBLEM: How do we quote a quote? (and make it easy to parse...)
 */
class Simple2 implements Markup {
    protected $actions = array(
        'doBlock' => 1000,
        'doRemoveHtml' => 10000,
    );
    protected $masked;
    protected $toc;
    protected $id = 1;
    
    public function __construct() {
    }
    
    public function render($input, $options = array()) {
        $this->reset();
        $result = $input;
        foreach($this->actions as $action => $priority) {
            $result = $this->$action($result);
        }
        $result = htmlspecialchars($result);
        $result = $this->unmask($result);
        return $result;
    }
    
    protected function reset() {
        $this->toc = new Node;
        $this->masked = array();
        asort($this->actions);
    }
    
    protected function mask($tag, $text) {
        $text = $this->unmask($text);
        $id = count($this->masked);
        $this->masked[] = $text;
        return "\x1A$tag$id$tag\x1A";
    }
    
    protected function maskBlock($text) {
        return $this->mask('B', $text);
    }
    
    protected function maskInline($text) {
        return $this->mask('I', $text);
    }
    
    protected function removeHtml($text) {
        return htmlspecialchars($text);
    }
    
    protected function unmask($text) {
        return preg_replace_callback('/\x1A\w([\d]+)\w\x1A/', array($this, 'unmaskCallback'), $text); 
    }
    
    protected function unmaskCallback($match) {
        return $this->masked[$match[1]];
    }
    
    protected function createId($name) {
        $id = $this->id++;
        $name = $this->removeHtml($name);
        return str_replace('\\', '_', get_class($this)).'_'.$name.'_'.$id;
    }
    
    protected function doRemoveHtml($input) {
        $result = $this->removeHtml($input);
        return $this->maskBlock($result);
    }
    
    protected function doBlock($input) {
        $result = $this->doHeaders($input);
        $result = str_replace("\x1AB", "\n\n\n\x1AB", $result);
        $result = str_replace("B\x1A", "B\x1A\n\n\n", $result);
        $blocks = explode("\n\n\n", $result);
        foreach($blocks as &$block) {
            $trimmed = trim($block);
            if(strncmp($trimmed, "\x1AB", 2) === 0) {
                # ...
            } else if(strlen($trimmed)) {
                $block = $this->doInnerBlock($block);
                #$block = $this->maskBlock('<blockquote>'.$block.'</blockquote>');
                $block = $this->maskBlock($block);
            }
        }
        return $this->maskBlock(implode("\n", $blocks));
    }
    
    protected function doInnerBlock($text) {
        $text = $this->doSubBlocks($text);
        return $text;
    }
    
    protected function doSubBlocks($text) {
        $blocks = explode("\n\n\n\n", $text);
        foreach($blocks as &$block) {
            if(preg_match_all('/^(?<indent>( \>|\> )+)/m', $block, $match, PREG_OFFSET_CAPTURE)) {
                // Blockquotes
                # FIXME: First and last paragraph is skipped
                $node = new Node;
                $indentDivider = $this->detectIndent($match[0], array(' ' => 1, '>' => 1, '\t' => 4));
                for($i = 0; $i < count($match[0]); ++$i) {
                    $m = $match[0][$i];
                    $start = $m[1] + strlen($m[0]);
                    if($i + 1 < count($match[0])) {
                        $content = substr($block, $start, $match[0][$i + 1][1] - $start);
                    } else {
                        $content = substr($block, $start);
                    }
                    $level = $this->getIndent($indentDivider, $m[0], array(' ' => 1, '>' => 1, '\t' => 4));
                    $content = $this->doBlock($content);
                    $node = $node->getLevel($level);
                    $node->add($content);
                }
                $block = $this->buildQuote($node->getRoot());
            } else if(preg_match_all('/^(?<indent>[ \t]*)(?<item>\*[ ]+|\*\) |[\d]+\. )/m', $block, $match, PREG_OFFSET_CAPTURE)) {
                // Lists
                # FIXME: First and last paragraph is skipped
                $node = new Node;
                $indentDivider = $this->detectIndent($match[0]);
                for($i = 0; $i < count($match[0]); ++$i) {
                    $m = $match[0][$i];
                    $start = $m[1] + strlen($m[0]);
                    if($i + 1 < count($match[0])) {
                        $content = substr($block, $start, $match[0][$i + 1][1] - $start);
                    } else {
                        $content = substr($block, $start);
                    }
                    $level = $this->getIndent($indentDivider, $m[0]);
                    $content = $this->doInnerParagraph($content, 'div'); # TODO <div> for single <p> for multiple?
                    $node = $node->getLevel($level);
                    $node->type = is_numeric(trim($m[0])) ? 'ol' : 'ul';
                    $node->add(array(trim($m[0]), $content));
                }
                $block = $this->buildList($node->getRoot());
            } else {
                $block = $this->doInnerParagraph($block);
            }
        }
        $text = implode("\n", $blocks);
        return $this->maskInline($text);
    }
    
    protected function buildQuote($node) {
        $result = $node->level > 0 ? '<blockquote>' : '';
        foreach($node->nodes as $child) {
            if(is_string($child)) {
                $result .=  $child;
            } else {
                $content = $this->buildQuote($child);
                $result .= $content;
            }
        }
        if($node->level > 0) $result .= '</blockquote>';
        return $result;
    }
    
    protected function buildList($node) {
        $type = !empty($this->type) ? $this->type : 'ul';
        $result = '<'.$type.'>';
        if(!empty($node->type) && $node->type === 'ol') {
            usort($node->nodes, function($a, $b) {
               if(is_array($a) && is_array($b)) {
                   return (int)$a[0] < (int)$b[0] ? -1 : 1;
               }
               return 0; 
            });
        }
        $open = false;
        foreach($node->nodes as $child) {
            if(is_array($child)) {
                if($open) $result .= '</li>';
                $result .= '<li>'.$child[1];
                $open = true;
            } else {
                $content = $this->buildList($child);
                if(!$open) $result .= '<li>';
                $result .= $content;
                $open = true;
            }
        }
        if($open) $result .= '</li>';
        $result .= '</'.$type.'>';
        return $result;
    }
    
    protected function detectIndent($text, $chars = array(' ' => 1, '\t' => 4)) {
        $parts = is_array($text) ? $text : explode("\n", $text);
        $divider = 4;
        foreach($parts as $part) {
            if(is_array($part)) {
                $part = isset($part['indent']) ? $part['indent'] : $part[0];
            }
            if(isset($chars[$part[0]])) {
                $divider = 0;
                $i = 0;
                while($i < strlen($part) && isset($chars[$part[$i]])) {
                    $divider += $chars[$part[$i++]];
                }
                break;
            }
        }
        return $divider;
    }
    
    protected function getIndent($divider, $text, $chars = array(' ' => 1, '\t' => 4)) {
        $indent = 0;
        $i = 0;
        while($i < strlen($text) && isset($chars[$text[$i]])) {
            $indent += $chars[$text[$i++]];
        }
        return $indent / $divider;
    }
    
    protected function doInnerParagraph($text, $tag = 'p') {
        # TODO: Quotes, Code
        $parts = preg_split('/\n[ ]*\n/', $text);
        foreach($parts as &$part) {
            $part = $this->doLinks($part);
            $part = $this->doStyles($part);
            $part = $this->removeHtml($part);
            $part = str_replace(array("\n\n", "  \n"), '<br />', $part); // Create line-breaks
            $part = $this->maskBlock('<'.$tag.'>'.$part.'</'.$tag.'>');
        }
        $text = implode("\n", $parts);
        $text = $this->removeHtml($text); // Remote any remaining HTML
        return $this->maskBlock($text);
    }
    
    protected function doHeaders($text) {
        $text = preg_replace_callback('/^(?<level>[#]{1,6})(?<title>[^\n]+)$/m', array($this, 'doHeadersCallback'), $text);
        $text = preg_replace_callback('/^(?<title>^[^\n]+)\n(?<level>[=-]+)\n+$/m', array($this, 'doHeadersCallback'), $text);
        return $text;
    }
    
    protected function doHeadersCallback($match) {
        switch($match['level'][0]) {
        case '=':
            $level = 1;
            break;
        case '-':
            $level = 2;
            break;
        case '#':
            $level = strlen($match['level']);
            $match['title'] = rtrim($match['title'], '#');
            break;
        }
        if(preg_match('/^(.+?)[ ]*\{#(|.+?)\}/m', $match['title'], $m)) {
            $name = $m[2] === '' ? lcfirst(preg_replace('/[^\w]+/', '-', $m[1])) : $m[2];
            $name = $this->createId($name);
            $title = $this->removeHtml($m[1]);
            $this->toc->getLevel($level)->add(array($name, $title));
            $title = sprintf('<a name="%s">%s</a>', $name, $title);
        } else {
            $title = $this->removeHtml($match['title']);
        }
        $result = sprintf('<h%d>%s</h%d>', $level, $title, $level);
        return $this->maskBlock($result);
    }
    
    /**
     * @todo Match ending )) with opening (? like [xxx](blah blah(blabha))
     * 
     * Links:
     *   [text](link)
     *   [text](link "title")
     * 
     * Custom links:
     *   [text](@wiki:page=index&stuff=meh)
     * 
     * Embedded:
     *   ![text](link)
     *   TODO: Assests how?
     * 
     * References:
     *   [text][id]
     *   [id]: http://example.com "Blablablabal"
     * 
     * Quick links?:
     *   [link]
     */
    protected function doLinks($text) {
        # Links
        $pattern = '/(?<prefix>!|)\[(?<text>[^\]]+)\]\((?<link>[^\)]+)\)/m';
        $text = preg_replace_callback($pattern, array($this, 'doLinksCallback'), $text);
        # Simple embeds !(xxx), TODO: optional (not in strict markdown)
        $pattern = '/(?<prefix>!)\((?<link>[^\)]+)\)/m';
        $text = preg_replace_callback($pattern, array($this, 'doLinksCallback'), $text);
        # Reference defines, FIXME
        $pattern = '/\[(?<id>[^\]]+)\]:(?<link>[^"]+)"(?<title>[^\)]+)"/m';
        $text = preg_replace_callback($pattern, function($match) {
            return ''; # FIXME!!!!!
        }, $text);
        # References, FIXME
        $pattern = '/(?<prefix>!|)\[(?<text>[^\]]+)\]\[(?<link>[^\]]+)\]/m';
        $text = preg_replace_callback($pattern, array($this, 'doLinksCallback'), $text);
        # TODO: autolinks=
        return $text;
    }
    
    protected function cleanupLinkMatch(&$match) {
        $link = trim($match['link']);
        $parts = explode(' ', $link, 2); # FIXME!?
        $match['options'] = array();
        if(count($parts) === 2) {
            list($link, $title) = $parts;
            if($title[0] === '"') { # FIXME
                $title = trim($title, '" ');
            } else {
                parse_str($title, $match['options']);
                $title = false;
            }
        }
        $match['text'] = isset($match['text']) ? $match['text'] : $link;
        $match['title'] = isset($title) ? $title : false;
        if($match['prefix'] === '!') {
            $match['link'] = $link; # FIXME?!
        } else {
            $match['link'] = $this->safeUrl($link, $match['options']);
        }
    }
    
    protected function safeUrl($url, array $parameters) {
        return Shared::safeUrl($url, $parameters);
    }
    
    protected function doLinksCallback($match) {
        $this->cleanupLinkMatch($match);
        if($match['prefix'] === '!') {
            $result = Shared::embed($match['link'], $match); # TODO: Callback?
            if(empty($result)) {
                $link = $this->removeHtml($match['link']);
                $text = $this->removeHtml($match['text']);
                $title = $this->removeHtml($match['title']);
                $result = sprintf('<img src="%s" alt="%s" title="%s" />', $link, $text, $title);
            }
        } else {
            $link = $this->removeHtml($match['link']);
            $text = $this->removeHtml($match['text']);
            if($title = $this->removeHtml($match['title'])) {
                $result = sprintf('<a href="%s" title="%s">%s</a>', $link, $title, $text);
            } else {
                $result = sprintf('<a href="%s">%s</a>', $link, $text);
            }
        }
        return $this->maskInline($result);
    }
    
    protected function doStyles($text) {
        $chars = preg_quote('*-_/', '/');
        $pattern = sprintf('/(?<open>[%s]{1,2})(?<content>[^%s]+)(?<close>[%s]{1,2})/', $chars, $chars, $chars);
        return preg_replace_callback($pattern, array($this, 'doStylesCallback'), $text);
    }
    
    protected function doStylesCallback($match) {
        if($match['open'] === $match['close']) {
            $content = $this->removeHtml($match['content']);
            switch($match['open']) {
            case '*':
            case '_':
            case '//':
                return $this->maskInline('<i>'.$content.'</i>');
            case '**':
            case '__': // TODO: This is bold in markdown, ignore and use as underline?
                return $this->maskInline('<b>'.$content.'</b>');
            case '--':
                return $this->maskInline('<s>'.$content.'</s>');
            }
        }
        return $match[0];
    }
}
