<?php

namespace susuka\markup;

class Node {
    public $level = 0;
    public $parent = null;
    public $nodes = array();
    public $content = array();
    
    public function add($node = null) {
        if($node === null) $node = new Node();
        if($node instanceof Node) {
            $node->parent = &$this;
            $node->level = $this->level + 1;
        }
        $this->nodes[] = $node;
        return $node;
    }
    
    public function getRoot() {
        return $this->getLevel(0);
    }
    
    public function getLevel($level) {
        $n = &$this;
        $l = $this->level;
        if($l != $level) {
            while($l < $level) {
                $n = $n->add();
                $l++;
            }
            while($l > $level) {
                $n = &$n->parent;
                $l--;
            }
        }
        return $n;
    }
}
