<?php

namespace susuka\markup;

class Chain implements Markup {
    protected $instances;
    
    public function __construct(array $instances = array()) {
        $this->instances = $instances;
    }
    
    public function add($instance) {
        $this->instance[] = $instance;
    }
    
    public function set(array $instances) {
        $this->instances = $instances;
    }
    
    public function get($index) {
        return $this->instances[$index];
    }
    
    public function clear() {
        $this->instances = array();
    }
    
    public function render($input, $options = array()) {
        foreach($this->instances as $instance) {
            $input = $instance->render($input, $options);
        }
        return $input;
    }
}
