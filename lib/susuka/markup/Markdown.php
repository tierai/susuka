<?php

namespace susuka\markup;

use \susuka\exception\Core as CoreException;
        
/**
 * Markdown markup
 * 
 * @todo All markup implementations should deal with links and images the same way, and produce similar html for lists and stuff.
 * @todo Allow BBCode in Markdown?
 * 
 * @todo Markdown_Parser seems like a possible security risk (XSS), maybe implement our own that does the following:
 *       - Does not allow any html elements by default
 *       - Allows BBCode, it's simply easier to read and write in many cases.
 *       Something like:
 *       - htmlspecialchars on input
 *       - Do BBCode => HTML, mask result
 *       - Do Markdown => HTML, mask result
 *       - Unmask
 */
class Markdown implements Markup {

    /**
     * @todo Make sure our markdown class doesn't generate any bad html.
     */
    public function render($input, $options = array()) {
        try {
            $parser = MarkdownParser::getInstance();
            $parser->no_markup = isset($options['no_markup']) ? $options['no_markup'] : true;
            $result = $parser->transform($input);
            return Shared::sanitize($result);
        } catch(CoreException $ex) {
            if(SU_LOG) \suLog::w('%s', $ex);
            // TODO: Standardize error formatting
            // TODO: $noThrow ?
            return 'Parse error: '.$ex->getMessage();
        }
    }
}

