<?php

namespace susuka\markup;

interface Markup {
    function render($input, $options = array());
}

