<?php

namespace susuka\markup;

use \susuka\core\Registry as Registry;
use \susuka\core\Uri as Uri;

/**
 * Shared markup code
 */
class Shared {
    
    /**
     * @todo Document the @route:params link format.
     */
    public static function safeUrl($url, array $parameters = array()) {
        if(empty($url)) {
            return ''; // FIXME?
        }
        if($url[0] === '#') {
            return $url;
        } else if($url[0] === '@') { // Is this an internal link?
            if($router = Registry::get('router')) {
                $route = substr($url, 1);
                return $router->build($route, $parameters);
            }
        }
        // This will force all links to be in the format scheme://...
        // So javascript:alert() will be javascript://alert(), which is safe (TODO: is it?)
        // TODO: Whitelist for valid "non ://" links, like mailto: (or just blacklist javascript?)
        if($uri = new Uri($url)) {
            return (string)$uri;
        }
        if(SU_LOG) \suLog::w('Bad url "%s"', $url);
        return '#bad_url:'.htmlspecialchars($url);
    }
    
    /**
     * Sanitize a HTML string
     */
    public static function sanitize($input) {
        $tidy = new SafeHTML;
        return $tidy->render($input, array('return_element' => 'body'));
    }
    
    /**
     * Get HTML for embedded content based on its URI.
     * 
     * @todo Local assets, should share code with HtmlView::asset().
     */
    public static function embed($url, $options = array()) {
        if($id = self::getYouTubeId($url)) {
            return self::embedYouTube($id, $options);
        }
        return false;
    }
    
    /**
     * Extract the video id from a youtube link.
     * 
     * Known formats:
     *  http?://?.youtube.?/watch?v=ID
     *  http?://?.youtube.?/embed/ID
     * 
     * Custom formats:
     *  youtube:ID
     * 
     * @todo Improve this, allow short youtube:id links
     * @todo no-cookie
     */
    protected static function getYouTubeId($url) {
        if(strncasecmp($url, 'youtube:', 8) === 0) {
            return substr($url, 8);
        } else if(preg_match('/youtube\./i', $url, $match)) {
            $uri = parse_url($url);
            parse_str($uri['query'], $query);
            if(isset($query['v'])) {
                return $query['v'];
            } else if(preg_match('/embed\/(.*)/i', $url, $match)) {
                print_r($match);
                die;
            }
        }
    }
    
    /**
     * @todo Width in %?
     */
    protected static function embedYouTube($id, $options) {
        $w = isset($options['w']) ? (int)$options['w'] : 640;
        $h = isset($options['h']) ? (int)$options['h'] : 0;
        if(empty($h)) $h = $w * 9 / 16;
        $format = '<iframe class="media" width="%d" height="%d" src="http://www.youtube.com/embed/%s" frameborder="0"></iframe>';
        return sprintf($format, $w, $h, htmlspecialchars($id));
    }
    
}
