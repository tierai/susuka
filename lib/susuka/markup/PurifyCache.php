<?php

namespace susuka\markup;

require_once SU_LIB_PATH.'/ext/htmlpurifier/library/HTMLPurifier.auto.php';

class PurifyCache extends \HTMLPurifier_DefinitionCache {
    protected $cache;
    protected $ns;
    
    function __construct($type) {
        parent::__construct($type);
        $this->cache = \susuka\core\Registry::get('cache');
        $this->ns = get_class($this);
    }
    
    function add($def, $config) {
        \susuka\exception\NotImplemented::raise();
    }

    function set($def, $config) {
        \susuka\exception\NotImplemented::raise();
    }

    function replace($def, $config) {
        \susuka\exception\NotImplemented::raise();
    }

    function get($config) {
        \susuka\exception\NotImplemented::raise();
    }

    function remove($config) {
        \susuka\exception\NotImplemented::raise();
    }
    
    function flush($config) {
        \susuka\exception\NotImplemented::raise();
    }

    function cleanup($config) {
        \susuka\exception\NotImplemented::raise();
    }
}
