<?php

namespace susuka\markup;

require_once(SU_LIB_PATH.'/ext/markdown/markdown.php');

/**
 * Markdown parser
 * 
 * - Adds support for internal links using the Router
 * - Fixes javascript: links
 *   The default markdown parser allows javascript: hrefs in its links, this is bad.
 *   I don't know if there is some better Markdown parser out there of if we should continue patching this one.
 * 
 * Internal links:
 * - [title](@_route:params)
 *   [Click me][@_wiki:page=hello]
 * 
 * Media embedding (image extension):
 * - ![title](url to media)
 *   ![Youtube clip](http://www.youtube.com/watch?v=clipid)
 *   ![title](url "option=value&w=400&h=200")
 * 
 * Code highlighting:
 * ```LANGUAGE_NAME
 * code...
 * ```
 * 
 * @todo We probably want to override _doAnchors_reference_callback too, and any other function that deals with urls.
 * @todo Prefix header ids
 */
class MarkdownParser extends \MarkdownExtra_Parser {
    
    static function getInstance() {
        static $parser;
        if(!isset($parser)) {
            $parser = new self;
            $parser->no_markup = true;
            $parser->no_entities = true;
            $parser->document_gamut['doTOC'] = 55;
            // FIXME: This is set to script|math by default, allowing <script> tags...
            // We have to match something here (or rewrite markdown.php)
            $parser->clean_tags_re = 'math';
        }
        return $parser;
    }
    
    /**
     * [TOC] tag support taken from https://bitbucket.org/jtopjian/php-markdown-extra/
     */
    function doTOC($text) {
        #    
        # Adds TOC support by including the following on a single line:
        #    
        # [TOC]
        #    
        # TOC Requirements:
        #     * Only headings 2-6
        #     * Headings must have an ID
        #     * Builds TOC with headings _after_ the [TOC] tag
        if (preg_match ('/\[TOC\]/m', $text, $i, PREG_OFFSET_CAPTURE)) {
            preg_match_all ('/<h([2-6]) id="([0-9a-z_-]+)">(.*?)<\/h\1>/i', $text, $h, PREG_SET_ORDER, $i[0][1]);
            $toc = '';
            foreach ($h as &$m) $toc .= str_repeat ("\t", (int) $m[1]-2)."*\t [${m[3]}](#${m[2]})\n";
            $text = preg_replace ('/\[TOC\]/m', Markdown($toc), $text);
        }
        return trim ($text, "\n");
    }
    
    function _doAnchors_inline_callback($matches) {
        $whole_match    =  $matches[1];
        $link_text      =  $this->runSpanGamut($matches[2]);
        $url            =  $matches[3] == '' ? $matches[4] : $matches[3];
        $title          =& $matches[7];

        $url = Shared::safeUrl($url);
        $url = $this->encodeAttribute($url);

        $result = "<a href=\"$url\"";
        if (isset($title)) {
            $title = $this->encodeAttribute($title);
            $result .=  " title=\"$title\"";
        }
        
        $link_text = $this->runSpanGamut($link_text);
        $result .= ">$link_text</a>";

        return $this->hashPart($result);
    }
    
    function _doImages_inline_callback($matches) {
        $whole_match    = $matches[1];
        $alt_text       = $matches[2];
        $url            = $matches[3] == '' ? $matches[4] : $matches[3];
        $title          =& $matches[7];

        parse_str($matches[7], $options);
        if(empty($options['title'])) $options['title'] = $alt_text;
        $result = Shared::embed($url, $options);
        
        if(empty($result)) {
            $alt_text = $this->encodeAttribute($alt_text);
            $url = Shared::safeUrl($url);
            $url = $this->encodeAttribute($url);
            $result = "<img src=\"$url\" alt=\"$alt_text\"";
            if (isset($title)) {
                $title = $this->encodeAttribute($title);
                $result .=  " title=\"$title\""; # $title already quoted
            }
            $result .= $this->empty_element_suffix;
        }
        
        return $this->hashPart($result);
    }
    
    function detectCodeLanguage($code) {
        if(strncasecmp($code, '<?php', 5) === 0) return 'php';
        return false;
    }
    
    function makeCodeSpan($code) {
        $pos = strpos($code, PHP_EOL);
        if($pos === false) {
            return parent::makeCodeSpan($code);
        }
        $lang = substr($code, 0, $pos);
        $code = substr($code, $pos + 1);
        $code = trim($code);
        if(!$lang) $lang = $this->detectCodeLanguage($code);
        if(strcasecmp($lang, 'php') === 0) {
            $code = highlight_string($code, true);
        } else {
            $code = htmlspecialchars($code, ENT_NOQUOTES);
            $code = nl2br($code);
        }
        return $this->hashPart("<code>$code</code>");
    }
}
