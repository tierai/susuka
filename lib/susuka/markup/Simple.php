<?php

namespace susuka\markup;

/**
 * 
 * Differences from Markdown:
 * - Indentated paragraphs
 * - Automatic indentation using spaces or tabs, first line defines how many.
 * - Images containing media (youtube links, videos) will embed that media.
 * - Nicer lists using *) -) or 1), needs only one space after
 * - HTML tags are evil and will be removed
 * - BBCode awareness/support
 * 
 * @todo Standardized syntax for custom tags #[cmd](params) ?
 * @todo Nicer options for links [abc](url "options") => [abc](url a=1,b=2...)
 * @todo Stip HTML or mask it?
 * @todo Table of contents
 * @todo Strict markdown mode
 * @todo Cleanup, all regexp use named groups
 */
class Simple implements Markup {
    protected $actions = array();
    protected $toc;
    protected $masked;
    protected $paragraphMarginLeft = 0;
    
    public function __construct() {
        $this->actions += array(
            'doHeaders' => 200,
            #'doLinks' => 300,
            #'doStyles' => 400,
            'doParagraphs' => 500,
            'doRemoveHtml' => 100000,
        );
        asort($this->actions);
    }
    
    public function render($input, $options = array()) {
        $this->toc = new Node();
        $this->masked = array();
        return $this->process($input);
    }
    
    protected function process($input) {
        $result = $input;
        foreach($this->actions as $action => $priority) {
            $this->$action($result);
        }
        $result = $this->unmask($result);
        return $result;
    }
    
    protected function mask($tag, $text) {
        if(func_num_args() > 2) {
            $args = func_get_args();
            $args = array_slice($args, 2);
            $text = vsprintf($text, $args);
        }
        $text = $this->unmask($text);
        $id = count($this->masked);
        $this->masked[] = $text;
        return "\x1A$tag$id$tag\x1A";
    }
    
    protected function maskBlock($text) {
        return $this->mask('B', $text);
    }
    
    protected function maskInline($text) {
        return $this->mask('I', $text);
    }
    
    protected function removeHtml($text) {
        return htmlspecialchars($text);
    }
    
    protected function unmask($text) {
        return preg_replace_callback('/\x1A\w([\d]+)\w\x1A/', array($this, 'unmaskCallback'), $text); 
    }
    
    protected function unmaskCallback($match) {
        return $this->masked[$match[1]];
    }
    
    protected function doRemoveHtml(&$result) {
        $result = $this->removeHtml($result);
    }
    
    /**
     * Headers
     * 
     * Format:
     * 
     *   Header 1
     *   ========
     * 
     *   Header 2
     *   --------
     * 
     *   # Header 1
     *   ## Header 2
     * 
     * Links:
     *   
     *   # Header {#linkName}
     *   # Header {#} <-- Name is generated from the title
     * 
     * @todo Two-line headers will accept lines mixed with = and -, Text\n=-=------ will form a level 1 header
     */
    protected function doHeaders(&$result) {
        $result = preg_replace_callback('/^(?<level>[#]{1,6})(?<title>[^\n]+)$/m', array($this, 'doHeadersCallback'), $result);
        $result = preg_replace_callback('/^(?<title>^[^\n]+)\n(?<level>[=-]+)\n+$/m', array($this, 'doHeadersCallback'), $result);
    }
    
    protected function doHeadersCallback($match) {
        switch($match['level'][0]) {
        case '=':
            $level = 1;
            break;
        case '-':
            $level = 2;
            break;
        case '#':
            $level = strlen($match['level']);
            $match['title'] = rtrim($match['title'], '#');
            break;
        }
        if(preg_match('/^(.+?)[ ]*\{#(|.+?)\}/m', $match['title'], $m)) {
            $name = $match[2] === '' ? lcfirst(preg_replace('/[^\w]+/', '-', $m[1])) : $m[2];
            $name = $this->createId($name);
            $title = $this->removeHtml($m[1]);
            $this->toc->getLevel($level)->add(array($name, $title));
            $title = sprintf('<a name="%s">%s</a>', $name, $title);
        } else {
            $title = $this->removeHtml($match['title']);
        }
        $result = sprintf('<h%d>%s</h%d>', $level, $title, $level);
        return $this->maskBlock($result);
    }
    
    /**
     * Paragraphs
     * 
     * A new paragraph is formed by a new title or one blank line.
     * 
     * Example:
     *   # Title
     *   This is the first paragraph
     *   This is a new line for the first paragraph
     *   
     *   This is the second paragraph...
     * 
     * Markdown lists (note spaces!)
     *   *   Some text
     *       *   Second level
     *   1.  Some text
     *       1.   Second level
     * 
     * Simple lists:
     *   *) Some text
     *      *) Second level
     *   1. Some text
     *      1. Second level
     * 
     * Quotes:
     *   > Blah
     *   > > Bleh
     *
     * @note (unlike markdown) Any leading spaces are used to form blockquotes, not just >
     * @todo Headers & lists in blockquotes
     * 
     * NEW:
     *   doBlock(result)
     *     doHeaders(result)
     *     create paragraphs from blocks (headers)
     *     foreach(block as b):
     *       if b.isMasked:
     *         result.add(b)
     *       else
     *         doListsAndParagraphs(b)
     *         doStylesAndStuff(b)
     *         doBlock(b)
     *         result.add(b)
     */
    protected function doParagraphs(&$result) {
        $result = str_replace("\x1AB", "\n\n\n\x1AB", $result);
        $result = str_replace("B\x1A", "B\x1A\n\n\n", $result);
        #$result = preg_replace('/B\x1A[\n]+/', "B\x1A\n\n\n", $result);
        #$result = preg_replace('/[\n]+\x1AB/', "\n\n\n\x1AB", $result);
        $result = preg_replace_callback('/^([ >]+)/m', function($match) {
            return str_repeat('    ', substr_count($match[1], '>'));
        }, $result);
        $parts = explode("\n\n", $result);
        $node = new Node();
        $indentChar = ' ';
        $indentDivider = 4;
        foreach($parts as $part) {
            if($part[0] == '\t' || $part[0] == ' ') {
                $indentChar = $part[0];
                $indentDivider = 1;
                while($part[$indentDivider] === $indentChar) $indentDivider++;
                break;
            }
        }   
        for($i = 0; $i < count($parts); ++$i) {
            $part = $parts[$i];
            $p = trim($part);
            $next = $i + 1 < count($parts) ? $parts[$i] : false;
            if(strlen($p) && strncmp($p, "\x1AB", 2) !== 0) {
                $indent = 0;
                while($part[$indent] === $indentChar) $indent++;
                $indent /= $indentDivider;
                $node = &$node->getLevel($indent + 1);
                if(false && preg_match('/^([ ]{0,100})(\*   |\*\) |[\d]+\. )/m', $parts[$i])) {
                    while($i < count($parts)) {
                        $p = $parts[$i];
                        $p = trim($p);
                        $p = 'LISTITEM:'.$this->mask($p);
                        $node->add('<p>'.$p.'</p>');
                        $i++;
                    }
                    $i--;
                } else {
                    $this->doParagraph($p);
                    $node->add('<p>'.$p.'</p>');
                }
            } else {
                $node = &$node->getRoot();
                $node->add($p);
            }
        }
        $root = $node->getRoot();
        $result = $this->renderParagraphNode($root);
    }
    
    /**
     * Apply final formatting to the content of a paragraph
     * 
     * Line-breaks:
     *   \n\n or (two or more spaces) + \n
     */
    protected function doParagraph(&$result) {
        $this->doLinks($result);
        $this->doStyles($result);
        $result = $this->removeHtml($result); // Remote any remaining HTML
        $result = str_replace(array("\n\n", "  \n"), '<br />', $result); // Create line-breaks
        return $result;
    }
    
    protected function renderParagraphNode($node) {
        $result = '';
        foreach($node->nodes as $child) {
            if(!is_string($child)) {
                $content = $this->renderParagraphNode($child);
                if(strlen(trim($content))) {
                    if($node->level > 1) {
                        $result .= $this->maskBlock('<blockquote>'.$content.'</blockquote>');
                    } else {
                        $result .= $this->maskBlock($content);
                    }
                }
            } else {
                $result .= $this->maskBlock($child);
            }
        }
        return $result;
    }
    
    protected function doLinks(&$result) {
        $pattern = '/(?<prefix>!|)\[(?<text>[\w\s]+)\]\((?<link>[^\)]+)\)/m';
        $result = preg_replace_callback($pattern, array($this, 'doLinksCallback'), $result);
        $pattern = '/(?<prefix>!|)\((?<link>[^\)]+)\)/m';
        $result = preg_replace_callback($pattern, array($this, 'doLinksCallback'), $result);
    }
    
    protected function doLinksCallback($match) {
        if($match['prefix'] === '!') {
            $link = $match['link'];
            $text = isset($match['text']) ? $match['text'] : $link;
            $result = Shared::embed($link, array('title' => $text)); # TODO: Callback?
            if(empty($result)) {
                $link = $this->removeHtml($link);
                $text = $this->removeHtml($text);
                $result = sprintf('<img src="%s" alt="%s" />', $link, $text);
            }
        } else {
            $link = $this->removeHtml($match['link']);
            $text = $this->removeHtml(isset($match['text']) ? $match['text'] : $link);
            $result = sprintf('<a href="%s">%s</a>', $link, $text);
        }
        return $this->maskInline($result);
    }
    
    protected function doStyles(&$result) {
        $chars = preg_quote('*-_/', '/');
        $pattern = sprintf('/(?<open>[%s]{1,2})(?<content>[^%s]+)(?<close>[%s]{1,2})/', $chars, $chars, $chars);
        $result = preg_replace_callback($pattern, array($this, 'doStylesCallback'), $result);
    }
    
    protected function doStylesCallback($match) {
        if($match['open'] === $match['close']) {
            $content = $this->removeHtml($match['content']);
            switch($match['open']) {
            case '*':
            case '_':
            case '//':
                return $this->maskInline('<i>'.$content.'</i>');
            case '**':
            case '__': // TODO: This is bold in markdown, ignore and use as underline?
                return $this->maskInline('<b>'.$content.'</b>');
            case '--':
                return $this->maskInline('<s>'.$content.'</s>');
            }
        }
        return $match[0];
    }

    protected function createId($name) {
        $name = $this->removeHtml($name);
        return 'suSimpleMarkup_'.$name;
    }
}
