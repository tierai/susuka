<?php

namespace susuka\markup;

/**
 * @todo Use standalone htmlpurifier?
 */
class Purify implements Markup {
    static $init = false;
    
    function __construct() {
        if(!self::$init) {
            require_once 'PurifyCache.php';
            \HTMLPurifier_DefinitionCacheFactory::instance()->register('PurifyCache', '\\susuka\\markup\\PurifyCache');
            self::$init = true;
        }
    }
    
    function render($input, $options = array()) {
        $config = \HTMLPurifier_Config::createDefault();
        //$config->set('Cache.DefinitionImpl', 'PurifyCache');
        $config->set('Cache.DefinitionImpl', null);
        $purifier = new \HTMLPurifier($config);
        $clean_html = $purifier->purify($input);
        return $clean_html;
    }
}
