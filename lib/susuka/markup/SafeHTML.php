<?php

namespace susuka\markup;

/**
 * Allow tags & attributes using a whitelist
 */
class SafeHTML implements Markup {
    protected $elements = array(
        'html', 'body',
        'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
        'a', 'p',
        'u', 'b', 'i',
        'strong', 'em',
        'pre', 'code',
        'div', 'span',
        'ul', 'li', 'ol',
        'table', 'td', 'tr',
        'iframe',
        'img',
    );
    protected $attributes = array('class', 'src', 'alt', 'href');
    protected $filters = array(
        'iframe' => array(
            'frameborder' => '/^\d+$/',
            'width' => '/^\d+$/',
            'height' => '/^\d+$/',
        ),
        '*' => array(
            'alt' => '/^[\w ]+$/',
            'src' => '/^(http|https):\/\//',
            'href' => '/^(http|https):\/\//',
            'class' => '/^[\w ]+$/',
        ),
    );
    
    function render($input, $options = array()) {
        \libxml_use_internal_errors(true); // Dont trigger error for invalid html
        $input = trim($input);
        $doc = new \DOMDocument('1.0', 'UTF-8');
        if(strlen($input)) $doc->loadHTML('<?xml encoding="UTF-8">'.$input);
        foreach($doc->getElementsByTagName('*') as $node) {
            $this->filter($node);
        }
        if(isset($options['return_element'])) {
            $node = $doc->getElementsByTagName($options['return_element'])->item(0);
            $html = $doc->saveHTML($node);
            $html = str_replace(array('<body>', '</body>'), '', $html);
        } else {
            $html = $doc->saveHTML();
        }
        return $html;
    }
    
    protected function filter($node) {
        if($node->attributes) {
            foreach($node->attributes as $child) {
                $this->filterAttribute($node, $child);
            }
        }
        foreach($node->childNodes as $child) {
            switch($child->nodeType) {
            case XML_ELEMENT_NODE:
                if(in_array($child->tagName, $this->elements)) {
                    $this->filter($child);
                } else {
                    $child->parentNode->removeChild($child);
                }
                break;
            default:
                break;
            }
        }
    }
    
    protected function filterAttribute($element, $attribute) {
        $filter = false;
        if(isset($this->filters[$element->nodeName]) && isset($this->filters[$element->nodeName][$attribute->name])) {
            $filter = $this->filters[$element->nodeName][$attribute->name];
        } else if(isset($this->filters['*'][$attribute->name])) {
            $filter = $this->filters['*'][$attribute->name];
        }
        if($filter) {
            if(!preg_match($filter, $attribute->value)) {
                $attribute->value = $attribute->name == 'href' ? '#removed' : '0';
            }
        } else {
            $attribute->parentNode->removeAttributeNode($attribute);
        }
    }
}
