<?php

namespace susuka\markup;

class Tidy implements Markup {
    
    static function isAvailable() {
        return class_exists('\tidy');
    }
    
    /**
     * @todo HTML/XHTML based on view formats
     */
    function render($input, $options = array()) {
        if(isset($options['xhtml'])) {
            $xhtml = $options['xhtml'];
        } else {
            $xhtml = defined('SU_VIEW_FORMAT') && SU_VIEW_FORMAT == 'application/xhtml+xml';
        }
        $config = array(
           'indent' => true,
           'output-xhtml' => $xhtml,
           'wrap' => 200,
        );
        $tidy = new \tidy;
        $tidy->parseString($input, $config, 'utf8');
        $tidy->cleanRepair();
        if(!isset($options['tidy_full_document']) || !$options['tidy_full_document']) {
            $node = $tidy->body();
            $result = str_replace(array('<body>', '</body>'), '', (string)$node);
        } else {
            $result = (string)$tidy;
        }
        return $result;
    }
}
