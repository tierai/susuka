<?php

namespace susuka\markup;

/**
 * BBCode markup
 */
class BBCode implements Markup {

    /**
     * @todo Internal links
     */
    public function render($input, $options = array()) {
        static $parser;
        if(!isset($parser)) {
            require_once(SU_LIB_PATH.'/bbcode/BBCode.php');
            $parser = new \BBCode();
        }
        return $parser->BBCodeToHTML($input);
    }
}

