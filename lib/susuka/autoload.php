<?php

/**
 * Autoloader initialization
 * 
 * This is the only file that is needed to bootstrap the susuka library.
 * You might also have to set SU_APP_PATH and SU_PUBLIC_PATH, see public/app.php for an example.
 * 
 * @todo Cleanup...
 * @todo Error handling should play nice with the main views format (html, xml, text, ...) we don't want text for a binary view or html in a text view.
 */
 
// Required constants
if(!defined('SU_TITLE')) define('SU_TITLE', 'Susuka Framework');
if(!defined('SU_VERSION')) define('SU_VERSION', is_file(__DIR__.'/version.txt') ? trim(file_get_contents(__DIR__.'/version.txt')) : '1.0-git');

// We need the Autoloader
require_once 'core/Autoloader.php';

use \susuka\core\Autoloader as Autoloader;

/**
 * Error exception
 * @note This does not extend the exception\Exception class, it's intentional so PHP-errors won't get caught.
 */
class suErrorException extends Exception {
    
    public function __construct($file, $line, $message, $code = 0, $previous = NULL) {
        parent::__construct($message, $code, $previous);
        $this->file = $file;
        $this->line = $line;
        $this->trace = debug_backtrace();
    }
    
    /*public function __toString() {
        return print_r($this->trace, true).parent::__toString();
    }*/
}

/**
 * Get the value of a constant
 * @param $name Constant name
 * @return Constant value or false if not defined.
 */
function suConstant($name) {
    return defined($name) ? constant($name) : false;
}

/**
 * The error handler
 */
function suErrorHandler($errno, $errstr, $errfile, $errline) {
    if(suConstant('SU_LOG') && class_exists('\suLog')) try { \suLog::e('PHP Error: 0x%06x, %s, %s:%d', $errno, $errstr, $errfile, $errline); } catch(\Exception $null) {}
    throw new suErrorException($errfile, $errline, $errstr, $errno);
}

function suInternalError($ex) {
    if(!headers_sent()) {
        $protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
        header($protocol.' 500 Internal server error');
        header('Content-Type: text/plain');
    }
    if(defined('SU_LOG') && SU_LOG) try { \suLog::e($ex); } catch(\Exception $null) {} // FIXME: Handle log exceptions.. 
    if(SU_APP_ENVIRONMENT === 'dev') {
        echo $ex;
    } else {
        echo 'Internal server error: '.$ex->getMessage().' in ';
        echo substr($ex->getFile(), strlen(realpath(__DIR__.'/..'))).':'.$ex->getLine();
    }
    die;
}

/**
 * Shutdown callback
 */
function suShutdown() {
    try {
        if($error = error_get_last()) {
            // FIXME: PHP fails to load sqlite for some reason... ignore PHP startup errors for now
            if($error['line']) {
                switch($error['type']){
                    case E_ERROR:
                    case E_CORE_ERROR:
                    case E_COMPILE_ERROR:
                    case E_USER_ERROR:
                    default:
                        throw new suErrorException($error['file'], $error['line'], $error['message'], $error['type']);
                }
            }
        }
    } catch(\Exception $ex) {
        if(suConstant('SU_LOG') && class_exists('\suLog')) try { \suLog::e($ex); } catch(\Exception $null) {}
        if(suConstant('SU_APP_ENVIRONMENT') === 'dev') {
            \suInternalError($ex);
        }
    }
}

// Set up error handling
if(SU_APP_ENVIRONMENT === 'production') {
    // Don't display any errors in a production environment
    error_reporting(0);
    ini_set('display_errors', 0);
} else {
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors', 0);
    register_shutdown_function('suShutdown');
}

// Register our error handler
set_error_handler('suErrorHandler');

// Initialize out autoloader
Autoloader::getInstance()->addPath(defined('SU_LIB_PATH') ? SU_LIB_PATH : realpath(__DIR__.'/..'));

// Init log
require_once __DIR__.'/log/autoload.php';
