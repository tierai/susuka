<?php

namespace susuka\view;

class Xml extends View {
    
    public function escape($value) {
        return htmlspecialchars($value);
    }
    
    public function render() {
        $writer = new \XmlWriter();
        $writer->openMemory();
        if(SU_APP_ENVIRONMENT === 'development') {
            $writer->setIndent(true);
        }
        $writer->startDocument();
        $this->renderXml($writer);
        $writer->endDocument();
        return $writer->outputMemory();
    }
    
    public function getMimeType() {
        return 'text/xml';
    }
    
    protected function renderXml($writer) {
        $writer->startElement('view');
        foreach($this->values as $key => $value) {
            $writer->writeElement($key, $value);
        }
        $writer->endElement();
    }
}
