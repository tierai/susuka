<?php

namespace susuka\view;

use \susuka\core\FileSystem;
use \susuka\core\Registry;
use \susuka\core\Uri;
use \susuka\exception\Core as CoreException;
use \susuka\exception\NotFound as NotFoundException;
use \susuka\exception\NotSupported as NotSupportedException;
use \susuka\asset\Builder as AssetBuilder;

/**
 * @todo HTML support (no />), XhtmlView? (markup parsers also have to support this).
 * @todo Per-site global views (so we can define 1 content.phtml per site... not sure if we need it though)
 * @todo Easy inclusion of the parent template (so an overriding template in a module can include it's parent...)
 * @todo Cache here or in controller? (has to be per UserGroup if we're using users)
 * @todo We could/should optimize the template here?
 * @todo Export priorities?
 * 
 * @todo Variable documentation:
 *       May not start with _ (reserved) (TODO: _ => template vars, __ => view?)
 *       Cannot be $this or $self (self reserved for future use)
 *       All variables are automatically exported as globals in the template file
 */
class Html extends View {
    const DOCTYPE_HTML5 = '<!DOCTYPE HTML>';
    const DOCTYPE_HTML4_STRICT = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">';
    const DOCTYPE_HTML4_LOOSE = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
    const DOCTYPE_HTML4_FRAMESET = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">';
    const DOCTYPE_XHTML11 = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
    const DOCTYPE_XHTML1_STRICT = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
    const DOCTYPE_XHTML1_TRANSITIONAL = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
    const DOCTYPE_XHTML1_FRAMESET = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">';
    
    protected $template;
    protected $export = array();
    protected $paths = array(); // Include paths
    protected $files = array(); // For debugging
    protected $suffix = '.phtml';
    protected $headContent = array();
    protected $closeTag = '>';
    protected $helpers = array();
    protected $currentTemplate; // Used by renderTemplate
    protected $contentScript = 'content';
    
    function initComponent(array $config) {
        parent::initComponent($config);
        $this->template = isset($this->options['layout']) ? $this->options['layout'] : 'layout/default';
        $this->setDocType(isset($this->options['doctype']) ? $this->options['doctype'] : self::DOCTYPE_HTML5);
        $this->export('head', array($this, 'getHeadContent'));
        #$this->addPath('.');
    }
    
    function getDocType() {
        return $this->docType.PHP_EOL;
    }
    
    /**
     * @todo Doctype changes after addStype or addHeadContent...
     */
    function setDocType($value) {
        if(count($this->headContent)) NotSupportedException::raise('Cannot set DocType after header content has been added');
        $this->docType = $value;
        $this->closeTag = strpos($value, 'HTML 4.0') === false ? '/>' : '>';
    }
    
    function getHtmlOpenTag() {
        $lang = substr(\Locale::getDefault(), 0, 2); // TODO: Locale to ISO 639 how?
        $attr = strpos($this->getDocType(), 'XHTML') === false ? '' : ' xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"';
        $attr .= ' lang="'.htmlspecialchars($lang).'"';
        return '<html'.$attr.'>'.PHP_EOL;
    }
    
    function setContentScript($value) {
        $this->contentScript = $value;
    }
    
    function content() {
        return $this->import($this->contentScript);
    }
    
    function escape($value) {
        return htmlspecialchars($value);
    }
    
    function render() {
        //return $this->renderTemplate($this->template);
        return $this->renderTemplate(SU_APP_PATH.'/'.$this->template.$this->suffix, true, true, true);
    }
    
    function getMimeType() {
        return isset($this->options['mimeType']) ? $this->options['mimeType'] : 'text/html';
    }
    
    function addHeadContent($content, $first = false) {
        if($first) {
            array_unshift($this->headContent, $content);
        } else {
            $this->headContent[] = $content;
        }
    }
        
    function getHeadContent() {
        return implode(PHP_EOL, $this->headContent);
    }
    
    function addLink($rel, $type, $href, $first = false) {
        // TODO: Reslove later? (at getHeadContent?)
        if(strpos($href, '://') === false) {
            $href = $this->asset($href, false, false);
        }
        $this->addHeadContent(sprintf('<link rel="%s" type="%s" href="%s" '.$this->closeTag, htmlspecialchars($rel), htmlspecialchars($type), htmlspecialchars($href)), $first);
    }
    
    function addStyle($source, $first = false) {
        $this->addLink('stylesheet', 'text/css', $source, $first);
    }
    
    function addStyleContent($source, $first = false) {
        $this->addHeadContent(sprintf('<style type="text/css"><!-- %s --></style>', htmlspecialchars($source)), $first);
    }
    
    function addScript($source, $first = false, $type = 'text/javascript') {
        if(strpos($source, '://') === false && strpos($source, '//') !== 0) {
            $source = $this->asset($source, false, false);
        }
        $this->addHeadContent(sprintf('<script type="%s" src="%s"></script>', htmlspecialchars($type), htmlspecialchars($source)), $first);
    }
    
    function addScriptContent($source, $first = false, $type = 'text/javascript') {
        $this->addHeadContent(sprintf('<script type="%s"><!-- %s ---></script>', htmlspecialchars($type), htmlspecialchars($source)), $first);
    }
    
    /**
     * @todo Non-root server paths..
     * @todo Template support
     * @todo User router to resolve assets, with an _asset route?
     */
    function asset($path) {
        $result = $this->findAsset($path);
        return htmlspecialchars($result);
    }
    
    function _asset($path) {
        return $this->findAsset($path);
    }
        
    function link($title, $route = null, $params = array(), $translate = true) {
        if($route === null) $route = str_replace(' ', '_', strtolower($title));
        if($translate) $title = $this->_str($title);
        $href = $this->_url($route, $params);
        return sprintf('<a href="%s">%s</a>', htmlspecialchars($href), htmlspecialchars($title));
    }
    
    function _link($title, $route = null, $params = array()) {
        if($route === null) $route = str_replace(' ', '_', strtolower($title));
        $href = $this->_url($route, $params);
        return sprintf('<a href="%s">%s</a>', htmlspecialchars($href), $title);
    }
    
    /**
     * @param $value
     *  if string: $key is converted to $value
     *  if callable: $value is called as $value($key)
     */
    function export($key, $value) {
        $this->export[$key] = $value;
    }
    
    /**
     * Import a template section
     * @todo Doc
     */
    function import($key, $required = true, $echo = false) {
        $result = false;
        if(isset($this->export[$key])) {
            $import = $this->export[$key];
            if(is_string($import)) {
                $result = $this->renderTemplate($import, false, $required);
            } else if(is_callable($import)) {
                $result = call_user_func($import);
            } else if(is_array($import)) {
                foreach($import as $prefix) {
                    if(false !== ($result = $this->renderTemplate($prefix.'/'.$key, false, false))) {
                        break;
                    }
                }
            }
        }
        if($result === false) {
            $result = $this->renderTemplate($key, false, $required);
        }
        if($echo) {
            echo $result;
        }
        return $result;
    }
    
    function addPath($path) {
        array_unshift($this->paths, $path);
        if(SU_LOG) \suLog::d('Add path "%s"', $path);
    }
    
    /**
     * @todo Hmmm, controller helper?
     */
    function helper($name) {
        try {
            if(!isset($this->helpers[$name])) {
                $name = preg_replace('/[^a-zA-Z0-9_\-\/]+/', '', $name);
                $name = explode('/', $name);
                $name[count($name) - 1] = ucfirst($name[count($name) - 1]);
                $name = implode('/', $name);
                $loader = $this->getApplication()->getAutoloader();
                $class = $loader->getClassName('$helper/'.$name);
                if(!class_exists($class)) NotFoundException::raise($class);
                $this->helpers[$name] = new \ReflectionClass($class);
            }
            $args = func_get_args();
            array_shift($args);
            $helper = $this->helpers[$name]->newInstanceArgs($args);
            if(method_exists($helper, 'setView')) $helper->setView($this);
            return $helper;
        } catch(NotFoundException $ex) {
            if(SU_LOG) \suLog::e('Helper not found %s', $ex);
            return sprintf('Helper "%s" was not found', $name);
        }
    }
    
    /**
     * @todo See asset()
     * @todo SU_ASSET_PATH!?
     */
    protected function findAsset($name) {
        $builder = AssetBuilder::getInstance();
        if(SU_APP_ENVIRONMENT != 'production') {
            if(!$builder->isUpToDate($name)) {
                $builder->build($name);
            }
        }
        $path = $builder->getPath($name);
        # TODO: This should be optional
        $query = is_file(SU_PUBLIC_PATH.'/'.$path) ? substr(dechex(filemtime(SU_PUBLIC_PATH.'/'.$path)), -4) : '404';
        $result = new Uri();
        $result->setLocalPath($path);
        $result->setPart('query', $query);
        return $result;
    }
    
    /**
     * Render a template file
     * 
     * @todo Document this, it's confusing...
     */
    protected function renderTemplate($template, $throw = true, $required = true, $absolute = false) {
        try {
            $this->currentTemplate = $absolute ? $template : FileSystem::locateFileInPaths($template.$this->suffix, $this->paths);
            $this->paths[] = dirname($this->currentTemplate);
            $this->files[] = $this->currentTemplate;
            if(!ob_start()) {
                \susuka\exception\Exception::raise('ob_start failed!');
            }
            if(SU_LOG) \suLog::d('%s => %s (%s)', $template, $this->currentTemplate, $this->paths);
            extract($this->values, EXTR_REFS | EXTR_OVERWRITE | EXTR_PREFIX_ALL, '');
            require($this->currentTemplate);
            $result = ob_get_contents();
            ob_end_clean();
            array_pop($this->paths);
            array_pop($this->files);
            return $result;
        } catch (\Exception $ex) { // Gotta catch em all, for array_pop $paths and $files
            if(isset($file)) {
                array_pop($this->paths);
                array_pop($this->files);
            }
            if(SU_LOG) \suLog::d('%s', $ex->getMessage());
            if(SU_LOG) \suLog::d('Template "%s" was not found (%s), included from %s, paths=%s', $template, $required, $this->files, $this->paths);
            if($throw || !$ex instanceof NotFoundException) {
                throw $ex;
            }
            if($required) {
                return sprintf('<div class="error">Template not found: "%s" (included from %s)</div>', htmlspecialchars($template), htmlspecialchars(implode(',', $this->files)));
            }
            return false;
        }
    }
}
