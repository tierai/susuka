<?php

namespace susuka\view;

/**
 * RSS view
 * 
 */
class Rss extends Xml {
    static protected $limit = 50;
    static protected $rssVersion = '2.0';
    
    public function getMimeType() {
        return 'application/rss+xml';
    }
    
    /**
     * @todo Use default/first feed or merge all?
     */ 
    public function renderXml($writer) {
        $writer->startElement('rss');
        $writer->writeAttribute('version', self::$rssVersion);
        $writer->startElement('channel');
        if(isset($this->values['feeds']) && is_array($this->values['feeds'])) {
            $feed = $this->values['feeds'][0];
            $feed->order('created', 'desc');
            $feed->limit(0, self::$limit);
            $writer->writeElement('link', $feed->getUri());
            $writer->writeElement('title', $feed->getTitle());
            $writer->writeElement('description', $feed->getDescription());
            $writer->writeElement('pubDate', $feed->getCreationDate());
            $writer->writeElement('lastBuildDate', $feed->getModificationDate());
            foreach($feed->items() as $item) {
                $writer->startElement('item');
                $writer->writeElement('link', $item->getUri());
                $writer->writeElement('title', $item->getTitle());
                $writer->writeElement('guid', $item->getUniqueId());
                $writer->writeElement('description', $item->getDescription());
                $writer->writeElement('pubDate', $item->getCreationDate());
                $writer->endElement();
            }
        }
        $writer->endElement();
    }
}

