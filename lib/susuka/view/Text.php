<?php

namespace susuka\view;

class Text extends View {
    
    public function escape($value) {
        return $value;
    }
    
    /**
     * @todo #controller and other special values does not work with this...
     */
    public function render() {
        return implode(PHP_EOL, $this->values);
    }
    
    public function getMimeType() {
        return 'text/plain';
    }
}
