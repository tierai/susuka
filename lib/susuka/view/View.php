<?php

namespace susuka\view;

use \susuka\core\Registry;

/**
 * @todo Cache
 * @todo Debugging
 * @todo Output formats (xml, txt, rss, html..)
 * @todo Buffer, not echo
 */
abstract class View {
    protected $values;
    protected $options;
    protected $router;
    protected $translator;
    
    function initComponent(array $config) {
        $this->values = isset($config['values']) ? $config['values'] : array();
        $this->options = isset($config['options']) ? $config['options'] : array();
        $this->router = isset($config['router']) ? $config['router'] : null;
        $this->translator = isset($config['translator']) ? $config['translator'] : null;
    }
    
    public function getApplication() {
        return $this->application;
    }
    
    public function setApplication($value) {
        $this->application = $value;
    }
    
    function getRouter() {
        return $this->router;
    }
    
    function setRouter($value) {
        $this->router = $value;
    }
    
    function getTranslator() {
        return $this->translator;
    }
    
    function setTranslator($value) {
        $this->translator = $value;
    }
    
    function get($key, $default = null) {
        // Auto-translate keys starting with $ (TODO: This could be done in set() and in ctor)
        $loc = '$'.$key;
        if(isset($this->values[$loc])) {
            $value = $this->_str($this->values[$loc]);
        } else if(isset($this->values[$key])) {
            $value = $this->values[$key];
        } else {
            if(SU_LOG) \suLog::w('Undefined key: "%s"', $key);
            $value = false;
        }
        if(is_string($value)) {
            $value = htmlspecialchars($value);
        }
        return $value;
    }
    
    function _get($key, $default = null) {
        return isset($this->values[$key]) ? $this->values[$key] : $default;
    }
    
    function set($key, $value) {
        $this->values[$key] = $value;
    }
    
    function has($key) {
        return isset($this->values[$key]);
    }

    function value($key, &$value) {
        if(isset($this->values[$key])) {
            $value = $this->values[$key];
            return true;
        }
        return false;
    }
    
    /**
     * Translate a string, the returned value is escaped with htmlspecialchars.
     * 
     * @code
     *   echo $this->str('Welcome to %2, %1', $name, $title);
     *   echo $this->str(array('Welcome to %2, %1', $name, $title));
     * @endcode
     * 
     * @param $text Text key to translate
     * @param $arg1 First argument to pass to the translator.
     * @return Translated string.
     */
    function str($text, $arg1 = null) {
        $args = func_get_args();
        if(is_array($args[0])) {
            $key = array_shift($args[0]);
            $args = $args[0];
        } else {
            $key = array_shift($args);
        }
        $result = $this->translator->translate($key, $args);
        return htmlspecialchars($result);
    }
    
    /**
     * Translate a string and return the raw result.
     */
    function _str($text, $arg1 = null) {
        $args = func_get_args();
        if(is_array($args[0])) {
            $key = array_shift($args[0]);
            $args = $args[0];
        } else {
            $key = array_shift($args);
        }
        return $this->translator->translate($key, $args);
    }
    
    /**
     * @param $route Route name
     * @param $params Action or an array or parameters
     */
    function url($route, $params = array()) {
        if(is_string($params)) $params = array('action' => $params);
        return htmlspecialchars($this->router->build($route, $params));
    }
    
    function _url($route, $params = array()) {
        if(is_string($params)) $params = array('action' => $params);
        return $this->router->build($route, $params);
    }
    
    abstract function escape($value);
    abstract function render();
    abstract function getMimeType();
}
