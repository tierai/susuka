<?php

namespace susuka\cache;

/**
 * @note APC (or PHP?) segfaults when storing some objects, so we use serialize() for now...
 */
class Apc implements Cache {
    protected $ttl;
    protected $prefix;
    protected $serialize;
    
    public function __construct($config = null) {
        $this->ttl = !empty($config['ttl']) ? $config['ttl'] : SU_CACHE_LIFETIME;
        $this->prefix = !empty($config['prefix']) ? $config['prefix'] : 'default';
        $this->serialize = !empty($config['apc_serialize']) ? $config['apc_serialize'] : true;
    }
    
    public function get($namespace, $key, $default = null, $throw = false, $ttl = null) {
        try {
            $key = $this->getKey($namespace, $key);
            if($data = apc_fetch($key)) {
                if($this->serialize) $data = unserialize($data);
                if($ttl === false || $data['updated'] + ($ttl ? $ttl : $this->ttl) > time()) {
                    return $data['value'];
                }
            }
        } catch(\Exception $ex) {
            if(SU_LOG) \suLog::e($ex);
            if(SU_APP_ENVIRONMENT === 'dev') throw $ex;
        }
        if($throw) \susuka\exception\NotFound::raise('%s/%s is not set', $namespace, $key);
        return $default;
    }
    
    public function value($namespace, $key, &$value, $ttl = null) {
        try {
            $key = $this->getKey($namespace, $key);
            if($data = apc_fetch($key)) {
                if($this->serialize) $data = unserialize($data);
                $value = $data['value'];
                if($ttl === false || $data['updated'] + ($ttl ? $ttl : $this->ttl) > time()) {
                    return true;
                }
            }
        } catch(\Exception $ex) {
            if(SU_LOG) \suLog::e($ex);
            if(SU_APP_ENVIRONMENT === 'dev') throw $ex;
        }
        return false;
    }
    
    public function set($namespace, $key, $value) {
        $key = $this->getKey($namespace, $key);
        $data = array('value' => $value, 'updated' => time());
        if($this->serialize) $data = serialize($data);
        apc_store($key, $data);
    }
    
    public function delete($namespace, $key) {
        $key = self::getKey($namespace, $key);
        apc_delete($key);
    }
    
    public function flush() {
        apc_clear_cache();
    }
    
    protected function getKey($namespace, $key) {
        return $this->prefix.':'.$namespace.PHP_EOL.$key;
    }
}
