<?php

namespace susuka\cache;

/**
 * Development cache
 * 
 * @todo Do something with this
 */
class Dev extends File {
    
    public function __construct($config = null) {
        if(SU_APP_ENVIRONMENT !== 'dev') \susuka\exception\NotSupported::raise('Dev cache is only allowed in dev env');
        parent::__construct($config);
    }
}
