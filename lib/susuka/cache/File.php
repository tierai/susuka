<?php

namespace susuka\cache;

if(!defined('SU_CACHE_FILE_PATH')) define('SU_CACHE_FILE_PATH', sys_get_temp_dir().'/susuka-mvc-cache/'.basename(SU_APP_PATH).'-'.SU_APP_ENVIRONMENT);
if(!defined('SU_CACHE_FILE_LIFETIME')) define('SU_CACHE_FILE_LIFETIME', defined('SU_CACHE_LIFETIME') ? SU_CACHE_LIFETIME : 3600);

/**
 * 
 * Defines
 * SU_CACHE_FILE_PATH
 * SU_CACHE_FILE_LIFETIME
 */
class File implements Cache {
    protected $ttl;
    protected $path;
    protected $prefix;
    
    public function __construct($config = null) {
        $this->ttl = !empty($config['ttl']) ? $config['ttl'] : SU_CACHE_FILE_LIFETIME;
        $this->path = !empty($config['file_path']) ? $config['file_path'] : SU_CACHE_FILE_PATH;
        $this->prefix = !empty($config['prefix']) ? $config['prefix'] : 'default';
    }
    
    public function get($namespace, $key, $default = null, $throw = false, $ttl = null) {
        try {
            $file = $this->getFilename($namespace, $key);
            if(file_exists($file)) {
                $value = file_get_contents($file);
                if($ttl === false || filemtime($file) + ($ttl ? $ttl : $this->ttl) > time()) {
                    return unserialize($value);
                }
            }
        } catch(\suErrorException $ex) {
            if(SU_LOG) \suLog::e($ex);
            if(SU_APP_ENVIRONMENT === 'dev') throw $ex;
        }
        if($throw) \susuka\exception\NotFound::raise('%s/%s is not set', $namespace, $key);
        return $default;
    }
    
    public function value($namespace, $key, &$value, $ttl = null) {
        try {
            $file = $this->getFilename($namespace, $key);
            if(file_exists($file)) {
                $value = file_get_contents($file);
                $value = unserialize($value);
                if($ttl === false || filemtime($file) + ($ttl ? $ttl : $this->ttl) > time()) {
                    return true;
                }
            }
        } catch(\suErrorException $ex) {
            if(SU_LOG) \suLog::e($ex);
            if(SU_APP_ENVIRONMENT === 'dev') throw $ex;
        }
        return false;
    }
    
    public function set($namespace, $key, $value) {
        $file = $this->getFilename($namespace, $key, true);
        $value = serialize($value);
        file_put_contents($file, $value, LOCK_EX);
    }
    
    public function delete($namespace, $key) {
        $file = $this->getFilename($namespace, $key);
        if(file_exists($file)) {
            unlink($file);
        }
    }
    
    public function flush() {
        // FIXME!
    }
    
    protected function getFilename($namespace, $key, $create = false) {
        $hash = sha1($namespace.PHP_EOL.$key);
        $path = $this->path.'/'.$this->prefix.'/'.$hash[0].'/'.$hash[1].'/'.$hash[2];
        if($create && !is_dir($path)) {
            mkdir($path, 0777, true);
        }
        return $path.'/'.$hash.'.cache';
    }
}
