<?php

namespace susuka\cache;

class Memcache implements Cache {
    protected $maxKeyLength = 250;
    protected $memcache;
    protected $ttl;
    protected $prefix;
    
    public function __construct($config = null) {
        $this->memcache = new \Memcache();
        $this->ttl = !empty($config['ttl']) ? $config['ttl'] : SU_CACHE_LIFETIME;
        $this->prefix = !empty($config['prefix']) ? $config['prefix'] : 'default';
        $this->maxKeyLength = !empty($config['memcache_max_key_length']) ? $config['memcache_max_key_length'] : 250;
        if(isset($config['memcache_server'])) {
            foreach($config['memcache_server'] as $server) {
                if(isset($server['enable']) && $server['enable']) {
                    $host = isset($server['host']) ? $server['host'] : 'localhost';
                    $port = isset($server['port']) ? $server['port'] : 11211;
                    $persistent = isset($server['persistent']) ? $server['persistent'] : true;
                    $this->memcache->addServer($host, $port, $persistent);
                }
            }
        } else {
            $this->memcache->addServer('localhost');
        }
    }
 
    public function get($namespace, $key, $default = null, $throw = false, $ttl = null) {
        $key = $this->getKey($namespace, $key);
        if($result = $this->memcache->get($key)) {
            if($ttl === false || $result['updated'] + ($ttl ? $ttl : $this->ttl) > time()) {
                return $result['value'];
            }
        }
        if($throw) \susuka\exception\NotFound::raise('%s/%s is not set', $namespace, $key);
        return $default;
    }
    
    public function value($namespace, $key, &$value, $ttl = null) {
        $key = $this->getKey($namespace, $key);
        if($result = $this->memcache->get($key)) {
            $value = $result['value'];
            if($ttl === false || $result['updated'] + ($ttl ? $ttl : $this->ttl) > time()) {
                return true;
            }
        }
        return false;
    }

    public function set($namespace, $key, $value) {
        $key = $this->getKey($namespace, $key);
        $this->memcache->set($key, array(
            'updated' => time(),
            'value' => $value,
        ));
    }
    
    public function delete($namespace, $key) {
        $key = $this->getKey($namespace, $key);
        $this->memcache->delete($key);
    }
    
    public function flush() {
        $this->memcache->flush();
    }
    
    static protected function getKey($namespace, $key) {
        $key = $this->prefix.':'.$namespace.PHP_EOL.$key;
        if(strlen($key) >= $this->maxKeyLength) {
            // TODO: Warning?
            return substr($key, 0, $this->maxKeyLength - 40).sha1($key);
        }
        return $key;
    }
}
