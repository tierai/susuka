<?php

namespace susuka\cache;

/**
 * Array cache.
 * 
 * @note Cache lifetime is ignored
 */
class Memory implements Cache {
    protected $cache = array();
    
    public function get($namespace, $key, $default = null, $throw = false, $ttl = null) {
        if(isset($this->cache[$namespace][$key])) {
            return $this->cache[$namespace][$key];
        }
        if($throw) \susuka\exception\NotFound::raise('%s/%s is not set', $namespace, $key);
        return false;
    }
    
    public function value($namespace, $key, &$value, $ttl = null) {
        if(isset($this->cache[$namespace][$key])) {
            $value = $this->cache[$namespace][$key];
            return true;
        }
        return false;
    }
    
    public function set($namespace, $key, $value) {
        $this->cache[$namespace][$key] = $value;
        return true;
    }
    
    public function delete($namespace, $key) {
        unset($this->cache[$namespace][$key]);
    }
    
    public function flush() {
        $this->cache = $array();
    }
}
