<?php

namespace susuka\cache;

if(!defined('SU_CACHE_LIFETIME')) define('SU_CACHE_LIFETIME', 3600);

/**
 * @note Keys may contain any printable characters above ASCII 0x1F (space and above, lower characters are reserved for the implementation).
 * @note Cache implementations use the same storage, but keys can be automatically prefixed with the prefix configuration option.
 */
interface Cache {
    
    /**
     * Fetch a value and return it.
     */
    public function get($namespace, $key, $default = null, $throw = false, $ttl = null);
        
    /**
     * Fetch a value from the cache and store it in $value.
     * @note $value can be set even if the return value is false, if $ttl expired but the object was still in storage.
     * @param $namespace Namespace for the object.
     * @param $key The key used to find the object.
     * @param $ttl Time to live.
     * @return True on success.
     */
    public function value($namespace, $key, &$value, $ttl = null);
    
    /**
     * Store a value in the cache.
     * @param $namespace Namespace for the object.
     * @param $key The key used to store the object.
     * @return True on success.
     */
    public function set($namespace, $key, $value);
    
    /**
     * Delete an object from the cache.
     */
    public function delete($namespace, $key);
    
    /**
     * Delete all object in this cache.
     * @note This may flush more than this object, implementations like Apc share the same single instance.
     * @todo flush($namespace)?
     */
    public function flush();
}
