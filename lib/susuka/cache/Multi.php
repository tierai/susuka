<?php

namespace susuka\cache;

/**
 * Multilevel cache.
 * 
 * @todo Init from config.
 */
class Multi implements Cache {
    protected $instances = array();
    
    public function __construct($config, array $instances = array()) {
        $this->instances = $instances;
    }
    
    public function add($instance) {
        $this->instances[] = $instance;
    }
    
    public function get($namespace, $key, $default = null, $throw = false, $ttl = null) {
        foreach($this->instances as $cache) {
            if($cache->value($namespace, $key, $result, $ttl)) {
                return $result;
            }
        }
        if($throw) \susuka\exception\NotFound::raise('%s/%s is not set', $namespace, $key);
        return $default;
    }
    
    public function value($namespace, $key, &$value, $ttl = null) {
        foreach($this->instances as $cache) {
            if($cache->set($namespace, $key, $value, $ttl)) {
                return true;
            }
        }
        return false;
    }
    
    public function set($namespace, $key, $value) {
        foreach($this->instances as $cache) {
            $cache->set($namespace, $key, $value);
        }
    }
    
    public function delete($namespace, $key) {
        foreach($this->instances as $cache) {
            $cache->delete($namespace, $key);
        }
    }
    
    public function flush() {
        foreach($this->instances as $cache) {
            $cache->flush();
        }
    }
}
