<?php

namespace susuka\helper;

abstract class View implements Helper {
    protected $application;
    protected $view;
    
    public function getApplication() {
        if($this->application == null) {
            $this->application = \susuka\core\Registry::get('app');
        }
        return $this->application;
    }
    
    public function setApplication($value) {
        $this->application = $value;
        return $this;
    }
    
    public function getView() {
        if($this->view == null) {
            return \susuka\core\Registry::get('view');
        }
        return $this->view;
    }
    
    public function setView($view) {
        $this->view = $view;
    }
}
