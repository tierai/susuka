<?php

namespace susuka\helper\html;

class Debug extends \susuka\helper\View {
    public function __toString() {
        if(defined('SU_DEBUG_TIMER') && SU_DEBUG_TIMER) {
            $app = $this->getView()->getApplication();
            $time = (microtime(true) - SU_DEBUG_TIMER) * 1000;
            $items[] = sprintf('Env: %s ', SU_APP_ENVIRONMENT);
            $items[] = sprintf('Time: %.03fms ', $time);
            $items[] = sprintf('Mem: %dkB ', memory_get_usage() / 1000);
            $items[] = sprintf('Peak: %dKB ', memory_get_peak_usage() / 1000);
            $items[] = sprintf('Logging: %s ', SU_LOG ? SU_LOG : 'disabled');
            $items[] = sprintf('PHP: %s ', phpversion());
            $db = \susuka\data\adapter\Pdo::getInstance(false);
            $items[] = sprintf('DB: %s', $db ? ($db->isConnected() ? 'Connected' : 'Disconnected') : 'NULL');
            foreach($app->getComponents() as $name => $i) {
                $items[] = sprintf('%s: %s ', ucfirst($name), is_object($i) ? get_class($i) : ($i ? (string)$i : 'disabled'));
            }
            #$items[] = sprintf('Path: %s ', SU_APP_PATH);
            #$items[] = sprintf('Public: %s ', SU_PUBLIC_PATH);
            $result = sprintf('<li><div>%.03fms</div><ul>', $time);
            foreach($items as $item) {
                $result .= sprintf('<li><div>%s</div></li>'.PHP_EOL, htmlspecialchars($item));
            }
            $result .= '</ul></li>';
            if(extension_loaded('xdebug')) {
                $result .= sprintf('<li><a href="?XDEBUG_PROFILE">XDebug %s</a><ul>', xdebug_is_enabled() ? 'enabled' : 'disabled');
                ob_start();
                xdebug_print_function_stack();
                $stack = ob_get_contents();
                ob_end_clean();
                $stack = trim($stack);
                $stack = str_replace(realpath(SU_APP_PATH), 'app', $stack);
                $stack = str_replace(realpath(SU_LIB_PATH), 'lib', $stack);
                $stack = str_replace(realpath(SU_PUBLIC_PATH), 'public', $stack);
                $result .= sprintf('<li><div>File: %s</div></li>', htmlspecialchars(xdebug_get_profiler_filename()));
                $result .= sprintf('<li><pre>%s</pre></li>', htmlspecialchars($stack));
                $result .= '</ul></li>';
            }
            return $result;
        }
        return '';
    }
}
