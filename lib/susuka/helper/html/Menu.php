<?php

namespace susuka\helper\html;

class Menu extends \susuka\helper\View {
    protected $items;
    
    public function __construct(array $items) {
        $this->items = $items;
    }
    
    public function __toString() {
        $view = $this->getView();
        return $this->build($view, $this->items);
    }
    
    protected function build($view, array $items) {
        $result = '';
        foreach($items as $key => $item) {
            if(isset($item['guest']) && $item['guest'] != 'FIXME:User::isGuest()') {
                continue;
            }
            if(isset($item['callback'])) {
                $result .= call_user_func($item['callback'], $this, $view, $item);
                continue;
            }
            $escape = isset($item['escape']) ? $item['escape'] : true;
            $title = isset($item['title']) ? $item['title'] : 'Untitled#'.$key;
            if(isset($item['class'])) {
                $result .= '<li class="'.htmlspecialchars($item['class']).'">';
            } else {
                $result .= '<li>';
            }
            if(isset($item['html'])) {
                $result .= $item['html'];
            } else if(isset($item['href'])) {
                $result .= sprintf('<a href="%s">%s</a>', htmlspecialchars($item['href']), $escape ? htmlspecialchars($title) : $title);
            } else if(isset($item['route'])) {
                if(is_array($item['route'])) {
                    $params = $item['route'];
                    $route = array_shift($params);
                } else {
                    $route = $item['route'];
                    $params = array();
                }
                $result .= $escape ? $view->link($title, $route, $params) : $view->_link($title, $route, $params);
            } else {
                $result .= '<div>'.($escape ? htmlspecialchars($title) : $title).'</div>';
            }
            if(isset($item['items'])) {
                $result .= '<ul>'.$this->build($view, $item['items']).'</ul>';
            }
            $result .= '</li>';
        }
        return $result;
    }
}

