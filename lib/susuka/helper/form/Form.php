<?php

namespace susuka\helper\form;

class Form extends Element {
    const METHOD_GET = 'get';
    const METHOD_PUT = 'put';
    const METHOD_POST = 'post';
    const METHOD_DELETE = 'delete';
    const ENCTYPE_URLENCODED = 'application/x-www-form-urlencoded';
    const ENCTYPE_MULTIPART = 'multipart/form-data';
    const CSRF_DEFAULT_NAME = 'su_csrf_check';
    
    protected $csrfElement;
    protected $session;
    
    function __construct($request = null, $name = null, $action = null, $method = null, array $attrs = array(), array $rules = array()) {
        $this->filter = array_merge($this->filter, array(
            'name' => 'text',
            'method' => 'text',
            'action' => 'text',
            'accept' => 'text',
            'accept-charset' => 'text',
            'enctype' => 'text',
        ));
        if($request === null) $request = \susuka\core\Registry::get('request');
        if($name === null) $name = str_replace('\\', '_', get_class($this));
        if($action === null) $action = new \susuka\core\Uri;
        if($method === null) $method = self::METHOD_POST;
        parent::__construct('form', array_merge(array('request' => $request, 'name' => $name, 'action' => $action, 'method' => $method), $attrs), $rules);
    }
    
    function clear() {
        $this->request(null);
    }
    
    function userValues($filter = false, array $defaults = array()) {
        if($request = $this->attr('request')) {
            $method = $this->attr('method');
            if(empty($method)) $method = 'post';
            if(is_array($filter)) {
                return $request->getArray($method, $filter, $defaults, false);
            }
            return $request->getParam($method);
        }
        return null;
    }
    
    function defaults($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['defaults'] = $value;
            return $this;
        }
        return isset($this->attrs['defaults']) ? $this->attrs['defaults'] : null;
    }
    
    function request($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['request'] = $value;
            return $this;
        }
        return isset($this->attrs['request']) ? $this->attrs['request'] : null;
    }
    
    function method($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['method'] = $value;
            return $this;
        }
        return isset($this->attrs['method']) ? $this->attrs['method'] : null;
    }
    
    function action($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['action'] = $value;
            return $this;
        }
        return isset($this->attrs['action']) ? $this->attrs['action'] : null;
    }
    
    function csrf($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['csrf'] = $value;
            return $this;
        }
        return isset($this->attrs['csrf']) ? $this->attrs['csrf'] : null;
    }
    
    function submitted() {
        \susuka\exception\Core::raise('You should not call this method on the root form');
    }
    
    function validate() {
        if($element = $this->csrfElement()) {
            if(strcmp($element->val(), $this->csrfToken()) != 0) {
                $this->error('Cross-site request forgery protection enabled, please try again');
                if(SU_APP_ENVIRONMENT === 'dev') $this->error($element->val().'!='.$this->csrfToken());
                return false;
            }
        }
        return parent::validate();
    }
    
    function input($name, $type, $value = null, array $attrs = array(), array $rules = array()) {
        return $this->add(new Input($name, $type, $value, $attrs, $rules));
    }
    
    function text($name, $value = null, array $attrs = array(), array $rules = array()) {
        return $this->input($name, 'text', $value, $attrs, $rules);
    }
    
    function password($name, $value = null, array $attrs = array(), array $rules = array()) {
        return $this->input($name, 'password', $value, $attrs, $rules);
    }
    
    function hidden($name, $value = null, array $attrs = array(), array $rules = array()) {
        return $this->input($name, 'hidden', $value, $attrs, $rules);
    }
    
    function button($name, $value = null, array $attrs = array(), array $rules = array()) {
        return $this->input($name, 'button', $value, $attrs, $rules);
    }
    
    function submit($name, $value = null, array $attrs = array(), array $rules = array()) {
        return $this->input($name, 'submit', $value, $attrs, $rules);
    }
    
    function reset($name, $value = null, array $attrs = array(), array $rules = array()) {
        return $this->input($name, 'reset', $value, $attrs, $rules);
    }
    
    function select($name, array $options = array(), $value = null, array $attrs = array(), array $rules = array()) {
        return $this->add(new Select($name, $options, $value, $attrs, $rules));
    }

    function textarea($name, $value = null, array $attrs = array(), array $rules = array()) {
        return $this->add(new Textarea($name, $value, $attrs, $rules));
    }
    
    function link($name, $href, $value, array $attrs = array(), array $rules = array()) {
        return $this->add(new Link($name, $href, $value, $attrs, $rules));
    }
    
    function note($name, $value, array $attrs = array(), array $rules = array()) {
        return $this->add(new Note($name, $value, $attrs, $rules));
    }
    
    function group($name, array $elements, array $attrs = array(), array $rules = array()) {
        return $this->add(new Group($name, $elements, $attrs, $rules));
    }
    
    function render($view = null) {
        $this->csrfElement();
        return parent::render($view);
    }
    
    function getSession() {
        if($this->session == null) {
            $this->session = $this->getApplication()->getSession();
        }
        return $this->session;
    }
    
    function setSession($value) {
        $this->session = $value;
    }
    
    protected function csrfToken() {
        return $this->getSession()->get(__CLASS__, 'csrfFormToken_'.$this->name());
    }
    
    protected function csrfNewToken() {
        $token = \susuka\core\Random::string(16);
        $this->getSession()->set(__CLASS__, 'csrfFormToken_'.$this->name(), $token);
        return $token;
    }
    
    protected function csrfElement() {
        if($csrf = $this->csrf()) {
            if($this->csrfElement === null) {
                $this->csrfElement = $this->hidden(is_string($csrf) ? $csrf : static::CSRF_DEFAULT_NAME, $this->csrfNewToken());
            }
        }
        return $this->csrfElement;
    }
}
