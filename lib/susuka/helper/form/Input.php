<?php

namespace susuka\helper\form;

class Input extends Element {
    
    function __construct($name, $type, $value = null, array $attrs = array(), array $rules = array()) {
        $this->filter = array_merge($this->filter, array(
            'alt' => 'text',
            'accept' => 'text',
            'name' => 'text',
            'type' => 'text',
            'value' => 'text',
            'src' => 'text',
            'size' => 'number',
            'maxlength' => 'number',
            'checked' => 'bool',
            'disabled' => 'bool',
            'readonly' => 'bool',
        ));
        parent::__construct('input', array_merge(array('name' => $name, 'type' => $type, 'value' => $value), $attrs), $rules);
    }
}
