<?php

namespace susuka\helper\form;

class Link extends Element {
    
    function __construct($name, $href, $value = null, array $attrs = array(), array $rules = array()) {
        $this->filter = array_merge($this->filter, array('href' => 'text'));
        parent::__construct('a', array_merge(array('name' => $name, 'href' => $href, 'value' => $value), $attrs), $rules);
    }
    
    function href($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['href'] = $value;
            return $this;
        }
        return isset($this->attrs['href']) ? $this->attrs['href'] : null;
    }
    
    protected function renderElement($view) {
        $href = $this->href();
        if(is_array($href)) {
            $href = $view->_url(array_shift($href), $href);
        }
        $attrs = array_merge($this->attrs, array('value' => $this->formValue(), 'href' => $href));
        return $this->createElement($view, $this->tag, $attrs, $this->filter, $this->elements);
    }
}
