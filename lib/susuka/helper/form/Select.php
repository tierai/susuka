<?php

namespace susuka\helper\form;

class Select extends Element {
    function __construct($name, array $options = null, $value = null, array $attrs = array(), array $rules = array()) {
        $this->filter = array_merge($this->filter, array(
            'name' => 'text',
            'disabled' => 'bool',
            'readonly' => 'bool',
        ));
        parent::__construct('select', array_merge(array('name' => $name, 'options' => $options, 'value' => $value), $attrs), $rules);
    }
    
    function options($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['options'] = $value;
            return $this;
        }
        return isset($this->attrs['options']) ? $this->attrs['options'] : null;
    }
    
    // TODO: optgroup
    protected function renderElement($view = null) {
        if($options = $this->options()) {
            $value = '';
            $selected = $this->formValue();
            $translate = $this->translate();
            foreach($options as $k => $v) {
                if($translate) $v = $view->_str($v);
                $sel = $selected !== null && $selected === $k ? ' selected="selected"' : '';
                $value .= '<option value="'.$view->escape($k).'"'.$sel.'>'.$view->escape($v).'</option>';
            }
        } else {
            $value = '<option>NULL</option>';
        }
        $attrs = array_merge($this->attrs, array('value' => $value));
        return $this->createElement($view, $this->tag, $attrs, $this->filter, $this->elements).PHP_EOL;
    }
    
}
