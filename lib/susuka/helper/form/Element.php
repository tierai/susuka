<?php

namespace susuka\helper\form;

class Element extends \susuka\helper\View {
    protected $filter = array(
        'class' => 'text',
        'id' => 'text',
        'style' => 'text',
        'title' => 'text',
    );
    protected $tag;
    protected $attrs;
    protected $rules;
    protected $parent;
    protected $request;
    protected $elements = array();
    protected $messages = array();
    
    function __construct($tag, array $attrs = array(), array $rules = array()) {
        $this->tag = $tag;
        $this->attrs = $attrs;
        $this->rules = $rules;
        if(empty($this->attrs['translate'])) $this->attrs['translate'] = array('title', 'value');
    }
    
    function tag() {
        return $this->tag;
    }
    
    function attrs() {
        return $this->attrs;
    }
    
    /**
     * Returns this elements root element.
     * @note This is assumed to be a Form object.
     */
    function root() {
        $root = $this;
        while($root->parent) {
            $root = $root->parent;
        }
        return $root;
    }
    
    function parent() {
        return $this->parent;
    }
    
    function name() {
        return $this->attr('name');
    }
    
    function attr($key, $value = null) {
        if(func_num_args() == 2) {
            assert($key != 'name');
            $this->attrs[$key] = $value;
            return $this;
        }
        return isset($this->attrs[$key]) ? $this->attrs[$key] : null;
    }
    
    function id($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['id'] = $value;
            return $this;
        }
        return isset($this->attrs['id']) ? $this->attrs['id'] : null;
    }
    
    function cls($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['class'] = $value;
            return $this;
        }
        return isset($this->attrs['class']) ? $this->attrs['class'] : null;
    }
    
    function style($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['style'] = $value;
            return $this;
        }
        return isset($this->attrs['style']) ? $this->attrs['style'] : null;
    }
    
    function title($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['title'] = $value;
            return $this;
        }
        return isset($this->attrs['title']) ? $this->attrs['title'] : null;
    }
    
    /**
     * Get or set the value attribute for this element.
     * @todo value, val, userValue.. it's a bit confusing...
     */
    function value($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['value'] = $value;
            return $this;
        }
        return isset($this->attrs['value']) ? $this->attrs['value'] : null;
    }
    
    function fallback($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['fallback'] = $value;
            return $this;
        }
        return isset($this->attrs['fallback']) ? $this->attrs['fallback'] : null;
    }
    
    function label($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['label'] = $value;
            return $this;
        }
        return isset($this->attrs['label']) ? $this->attrs['label'] : null;
    }
    
    function visible($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['visible'] = $value;
            return $this;
        }
        return isset($this->attrs['visible']) ? $this->attrs['visible'] : true;
    }
    
    function translate($value = null) {
        if(func_num_args() == 1) {
            $this->attrs['translate'] = $value;
            return $this;
        }
        return isset($this->attrs['translate']) ? $this->attrs['translate'] : null;
    }
    
    function values($filter = false, array $default = array(), $throw = false) {
        $result = array();
        foreach($this->elements as $element) {
            $result[$element->name()] = $element->formValue();
            $result = array_merge($result, $element->values());
        }
        if($filter) {
            return \susuka\core\Request::filterValues($result, $filter, $default, $throw);
        }
        return $result;
    }
    
    /**
     * Alias for formValue()
     */
    function val() {
        return $this->formValue();
    }
    
    /**
     * Returns the value of this form element.
     * The value is retrieved in order from the following sources:
     * - (Local) Value attribute.
     * - (Root) User value from the root form request attribute (GET or POST).
     * - (Root) Default value for the root form defaults attribute.
     * - (Local) Element fallback attribute.
     */
    function formValue() {
        $value = $this->value();
        if($value !== null) return $value;
        $root = $this->root();
        $name = $this->name();
        if($request = $root->attr('request')) {
            $method = $root->attr('method');
            if(empty($method)) $method = 'post';
            $value = $request->get($method, $name, null, false);
            if($value !== null) return $value;
        }
        if($defaults = $root->attr('defaults')) {
            if(is_array($defaults)) {
                $value = isset($defaults[$name]) ? $defaults[$name] : null;
            } else if($defaults instanceof \susuka\data\model\Model) {
                $value = $defaults->get($name, null, false);
            } else {
                \susuka\Excpetion\NotSupported::raise('Unknown defaults value in form %s', $root->name());
            }
            if($value !== null) return $value;
        }
        return $this->fallback();
    }
    
    /**
     * Returns the user submitted value for this element.
     * The returned value is retrived from the root-elements Request object.
     */
    function userValue() {
        $root = $this->root();
        if($request = $root->attr('request')) {
            $method = $root->attr('method');
            if(empty($method)) $method = 'post';
            return $request->get($method, $this->name(), null, false);
        }
        return null;
    }
    
    function submitted() {
        return $this->userValue();
    }
    
    function add(Element $element) {
        $element->remove();
        $element->parent = $this;
        $this->elements[$element->name()] = $element;
        return $element;
    }
    
    function remove($element = null) {
        if(func_num_args() == 1) {
            $name = $element instanceof Element ? $element->name() : $element;
            if(isset($this->elements[$name])) {
                $this->elements[$name]->parent = null;
                unset($this->elements[$name]);
                return true;
            }
            return false;
        } else if($this->parent) {
            unset($this->parent->elements[$this->name()]);
            $this->parent = null;
        }
        return true;
    }
    
    function elements() {
        return $this->elements;
    }
    
    function rule($rule) {
        $this->rules[] = $rule;
        return $this;
    }
    
    function rules() {
        return $this->rules;
    }
    
    function message($type, $format) {
        $this->messages[$type][] = func_get_args();
        return $this;
    }
    
    function messages($type = null) {
        if(func_num_args() == 1) {
            return isset($this->messages[$type]) ? $this->messages[$type] : array();
        }
        return $this->messages;
    }
    
    function error($format) {
        $this->messages['error'][] = func_get_args();
        return $this;
    }
    
    function info($format) {
        $this->messages['info'][] = func_get_args();
        return $this;
    }
    
    /**
     * Validate the form
     * @throws ???
     * @return An array of valid form fields.
     */
    function validate() {
        $this->messages['error'] = array();
        foreach($this->rules as $rule) {
            if(!is_string($rule) && is_callable($rule)) {
                if(!call_user_func($rule, $this)) {
                    $this->error('Invalid value');
                }
            } else if(is_array($rule)) {
                $message = isset($rule['message']) ? $rule['message'] : null;
                $default = $message ? $message : 'Invalid value';
                if(isset($rule['callback'])) {
                    if(!call_user_func($rule['callback'], $this, $rule)) {
                        $this->error($default);
                    }
                } else {
                    $value = $this->formValue();
                    
                    // Length
                    if(isset($rule['length'][0]) && strlen($value) < $rule['length'][0]) {
                        $this->error($message ? $message : 'Value must be at least %d characters', $rule['length'][0]);
                    } else if(isset($rule['length'][1]) && strlen($value) > $rule['length'][1]) {
                        $this->error($message ? $message : 'Value cannot be longer than %d characters', $rule['length'][1]);
                    }
                    
                    // Equallity
                    if(isset($rule['equal'])) {
                        if($rule['equal'] instanceof Element) {
                            if($value !== $rule['equal']->val()) {
                                $other = $rule['equal']->label();
                                $this->error($message ? $message : 'This field must be equal to %s', $other ? $other : $rule['equal']->name());
                            }
                        } else {
                            if($value !== $rule['equal']) {
                                $this->error($message ? $message : 'This field must be equal to %s', $rule['equal']);
                            }
                        }
                    }
                }
            } else {
                \susuka\exception\Core::raise('Bad rule format "%s"', print_r($rule));
            }
        }
        $result = true;
        foreach($this->elements as $element) {
            $result &= $element->validate();
        }
        return $result && empty($this->messages['error']);
    }
    
    function render($view = null) {
        if(!$this->visible()) return null;
        if($view === null) $view = $this->getView();
        if(empty($this->attrs['name'])) $this->attrs['name'] = uniqid();
        if(empty($this->attrs['id'])) $this->attrs['id'] = $this->parent ? $this->parent->name().'_'.$this->name() : $this->name();
        return $this->renderPrefix($view).$this->renderElement($view).$this->renderSuffix($view);
    }
    
    function __toString() {
        $view = $this->getView();
        return $this->render($view);
    }
    
    protected function renderElement($view) {
        $attrs = array_merge($this->attrs, array('value' => $this->formValue()));
        return $this->createElement($view, $this->tag, $attrs, $this->filter, $this->elements);
    }
    
    protected function renderPrefix($view) {
        $result = '';
        if(!isset($this->filter['label']) && ($label = $this->label())) {
            $label = $this->createElement($view, 'label',
                array('for' => $this->id(), 'value' => $label, 'translate' => $this->translate()),
                array('for' => 'text', 'title' => 'text'),
                array()
            );
            $result .= $label;
        }
        if($this->messages) {
            // TODO: wrap in container?
            foreach($this->messages as $type => $messages) {
                foreach($messages as $args) {
                    $text = $view->_str($args);
                    $result .= '<div class="'.$type.'">'.$view->escape($text).'</div>';
                }
            }
        }
        return $result;
    }
    
    protected function renderSuffix($view) {
        return PHP_EOL;
    }
    
    protected function createElement($view, $tag, array $attrs, array $filter, array $elements) {
        $result = '<'.$tag;
        $translate = isset($attrs['translate']) ? $attrs['translate'] : false;
        foreach($filter as $key => $type) {
            if(!isset($attrs[$key])) continue;
            $value = $attrs[$key];
            switch($type) {
            case 'bool':
                $value = empty($value) ? false : $key;
                break;
            case 'number':
                $value = intval($value);
                break;
            case 'text':
                if($translate && in_array($key, $translate)) $value = $view->_str($value);
                break;
            case 'ignore':
                continue;
            default:
                \susuka\Excpetion\NotSupported::raise('%s is not a valid filter type', $key);
            }
            if($value !== false) {
                $result .= ' '.$key.'="'.$view->escape($value).'"';
            }
        }
        if($elements) {
            $content = '';
            foreach($elements as $element) {
                $elementContent = $element->render($view);
                if(strlen($elementContent)) $content .= '<li>'.$elementContent.'</li>';
            }
            if(strlen($content)) $content = '<ol>'.$content.'</ol>';
        } else if(isset($attrs['value']) && !isset($filter['value'])) {
            $content = $attrs['value'];
            if($translate && $tag !== 'input' && in_array('value', $translate)) $content = $view->_str($content);
        } else {
            $content = false;
        }
        if(strlen($content) < 1 && in_array($tag, array('input'))) {
            # TODO: $view->getOutputFormat()...
            $result .= ' />';
        } else {
            $result .= '>'.$content.'</'.$tag.'>';
        }
        return $result;
    }
}
