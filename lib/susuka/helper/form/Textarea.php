<?php

namespace susuka\helper\form;

class Textarea extends Element {
    
    function __construct($name, $value = null, array $attrs = array(), array $rules = array()) {
        $this->filter = array_merge($this->filter, array(
            'name' => 'text',
            'rows' => 'number',
            'cols' => 'number',
            'disabled' => 'bool',
            'readonly' => 'bool',
        ));
        if(!isset($attrs['rows'])) $attrs['rows'] = 10;
        if(!isset($attrs['cols'])) $attrs['cols'] = 20;
        parent::__construct('textarea', array_merge(array('name' => $name, 'value' => $value), $attrs), $rules);
    }
}
