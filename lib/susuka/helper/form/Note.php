<?php

namespace susuka\helper\form;

class Note extends Element {
    
    function __construct($name, $value, array $attrs = array(), array $rules = array()) {
        parent::__construct('span', array_merge(array('name' => $name, 'value' => $value), $attrs), $rules);
    }
}
