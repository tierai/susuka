<?php

namespace susuka\helper\form;

class Group extends Element {
    
    function __construct($name, array $elements, array $attrs = array(), array $rules = array()) {
        parent::__construct('div', array_merge(array('name' => $name), $attrs), $rules);
        foreach($elements as $element) {
            $this->add($element);
        }
    }
    
}
