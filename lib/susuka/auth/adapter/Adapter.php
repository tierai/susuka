<?php

namespace susuka\auth\adapter;

interface Adapter {
    function authenticate(array $args);
}
