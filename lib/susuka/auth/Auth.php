<?php

namespace susuka\auth;

use \susuka\exception\Security as SecurityException;

class Auth {
    const CODE_UNKNOWN = 0;
    const CODE_IDENTITY_NOT_FOUND = 1;
    const CODE_INVALID_PASSWORD = 2;
    const CODE_ALREADY_AUTHENTICATED = 3;
    
    protected $application;
    protected $identity;
    protected $session;
    protected $sessionKey;

    public function __construct($config = null) {
        $this->sessionKey = isset($config['sessionKey']) ? $config['sessionKey'] : '_su_auth_identity_';
    }
    
    public function getApplication() {
        return $this->application;
    }
    
    public function setApplication($value) {
        $this->application = $value;
    }
    
    public function getSession() {
        if($this->session === null) $this->session = $this->getApplication()->getSession();
        return $this->session;
    }
    
    public function setSession($value) {
        $this->session = $value;
    }

    public function authenticate($adapter, array $args) {
        if($this->hasIdentity()) {
            SecurityException::raise(self::CODE_ALREADY_AUTHENTICATED);
        } else if($this->identity = $adapter->authenticate($args)) {
            $this->setStoredIdentity($this->identity);
        } else {
            SecurityException::raise(self::CODE_UNKNOWN);
        }
    }
    
    public function hasIdentity() {
        return $this->getIdentity() ? true : false;
    }
    
    /**
     * Returns the Identity assosiated with this authentication object.
     * @todo Is identity an object or an ID?
     *       if object: does ->getId() return the ID, and what format is it in? (integer?)
     */
    public function getIdentity() {
        if(!$this->identity) $this->identity = $this->getStoredIdentity();
        return $this->identity;
    }
    
    public function clearIdentity() {
        $this->clearStoredIdentity();
        $this->identity = null;
    }
    
    protected function getStoredIdentity() {
        return $this->getSession()->get(null, $this->sessionKey);
    }
    
    protected function setStoredIdentity($identity) {
        $this->getSession()->set(null, $this->sessionKey, $identity);
    }
    
    protected function clearStoredIdentity() {
        $this->getSession()->set(null, $this->sessionKey, null);
    }
}
