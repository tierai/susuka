<?php

namespace susuka\session;

class Php implements Session {
    protected $config;
    protected $defaultNamespace;
    protected $privateNamespace;
    
    public function __construct($config) {
        $this->config = $config;
        $this->defaultNamespace = isset($config['default_namespace']) ? $config['default_namespace'] : '$su_global$';
        $this->privateNamespace = isset($config['private_namespace']) ? $config['private_namespace'] : '$su_private$';
        
        if($name = isset($config['php_name']) ? $config['php_name'] : 'suSID') {
            session_name($name);
        }
        
        if(!session_start()) {
            \susuka\exception\Core::raise('session_start() failed');
        }
        
        if(!$this->value($this->privateNamespace, 'client_id', $id) || strcmp($id, $this->createClientId()) !== 0) {
            if(!session_destroy()) {
                \susuka\exception\Core::raise('session_destroy() failed');
            }
            if(!session_start()) {
                \susuka\exception\Core::raise('session_start() failed');
            }
            if(!session_regenerate_id(true)) {
                \susuka\exception\Core::raise('session_regenerate_id() failed');
            }
            $this->set($this->privateNamespace, 'client_id', $this->createClientId());
        }
    }
    
    public function get($namespace, $key, $default = null, $throw = false) {
        if($namespace === null) $namespace = $this->defaultNamespace;
        if(!isset($_SESSION[$namespace][$key])) {
            if($throw) InvalidArgumentException::raise('The key "%s" does not exist!', $key);
            return $default;
        }
        return $_SESSION[$namespace][$key];
    }
    
    public function value($namespace, $key, &$value) {
        if($namespace === null) $namespace = $this->defaultNamespace;
        if(isset($_SESSION[$namespace][$key])) {
            $value = $_SESSION[$namespace][$key];
            return true;
        }
        return false;
    }
    
    public function set($namespace, $key, $value = null) {
        if($namespace === null) $namespace = $this->defaultNamespace;
        if(is_array($key)) {
            foreach($key as $name => $value) {
                $_SESSION[$namespace][$key] = $value;
            }
        } else {
            $_SESSION[$namespace][$key] = $value;
        }
        return $this;
    }
    
    public function delete($namespace, $key) {
        unset($_SESSION[$namespace][$key]);
        return $this;
    }
    
    protected function createClientId() {
        return $_SERVER['REMOTE_ADDR'].'/'.$_SERVER['HTTP_USER_AGENT'];
    }
}
