<?php

namespace susuka\session;

/**
 * @todo ttl?
 * @todo prefix?
 * @todo Storage base interface for all get, set classes? (Config, Cache..)
 * 
 * @note Session implementation currently share the same global $_SESSION variable.
 */
interface Session {
    
    /**
     * Fetch an object from this session.
     * @param $namespace Object namespace, use null for default.
     * @param $key Object key.
     * @param $default Default return value (if $throw is false)
     * @param $throw Throw a NotFound exception if the object doesn't exist.
     * @return True if the object was found.
     */
    public function get($namespace, $key, $default = NULL, $throw = false);
    
    /**
     * Fetch an object from this session.
     * @param $namespace Object namespace, use null for default.
     * @param $key Object key.
     * @param $value Reference to the fetched value.
     * @return True if the object was found.
     */
    public function value($namespace, $key, &$value);
    
    /**
     * Store a session object.
     * @param $namespace Object namespace, use null for default.
     * @param $key String key or an array of key-value pairs to set.
     * @param $value Value to set
     * @return $this
     */
    public function set($namespace, $key, $value = null);
    
    /**
     * Delete a session object.
     * @param $namespace Object namespace, use null for default.
     * @param $key Object key.
     * @return $this
     */
    public function delete($namespace, $key);
}
