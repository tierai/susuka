<?php

namespace susuka\session;

class Database implements Session {
    protected $cache;
    
    public function __construct($config) {
        \susuka\exception\NotImplemented::raise();
    }
    
    public function getCache() {
        return $this->cache;
    }
    
    public function setCache($value) {
        $this->cache = $value;
        return $this;
    }
    
    public function set($namespace, $key, $value = null) {
        \susuka\exception\NotImplemented::raise();
    }
    
    public function value($namespace, $key, &$value) {
        \susuka\exception\NotImplemented::raise();
    }
    
    public function get($namespace, $key, $default = NULL, $throw = false) {
        \susuka\exception\NotImplemented::raise();
    }
    
    public function delete($namespace, $key) {
        \susuka\exception\NotImplemented::raise();
    }
    
}
