<?php

namespace susuka\core;

use susuka\exception\NotFound as NotFoundException;

class FileSystem {
    
    static public function locateFile($filename, $throw = true, $callback = false) {
        $debug = SU_LOG && SU_APP_ENVIRONMENT === SU_APP_ENVIRONMENT_DEV ? array() : false;
        if($debug !== false) \suLog::d('Searching for "%s"', $filename);
        
        if($filename[0] == '/' && is_file($filename)) return $filename;
        
        $paths = array();
        $app = Registry::get('app');
        
        if($filename[0] == '@') {
            list($name, $filename) = explode('/', substr($filename, 1), 2);
            $module = Registry::get('app')->loadModule($name);
            if($app) $paths[] = $app->getPath().'/module/'.$name;
            $paths[] = $module['fullpath'];
        } else if($app) {
            $paths[] = $app->getPath();
        } else {
            $paths = array('.', '/');
        }
        
        foreach($paths as $path) {
            $file = $path.'/'.$filename;
            if($callback) $file = call_user_func($callback, $file);
            if(is_file($file)) {
                return $file;
            }
            if($debug !== false) $debug[] = $file;
        }
        
        /*echo '<pre>';
        echo "$filename = \n";
        print_r($paths);
        print_r($debug);
        echo '</pre>';*/
        
        $debug = is_array($debug) ? ' ('.implode(', ', $debug).')' : '';
        if($throw) {
            NotFoundException::raise('File "%s" does not exist!'.$debug, $filename);
        }
        return false;
    }
    
    static public function locateFileInPaths($filename, array $paths, $throw = true) {
        foreach($paths as $path) {
            if($file = self::locateFile($path.'/'.$filename, false)) return $file;
        }
        if($throw) {
            NotFoundException::raise('File "%s" does not exist!', $filename);
        }
        return false;
    }
}
