<?php

namespace susuka\core;

class Registry {
    static $values = array();

    static public function set($key, $value) {
        self::$values[$key] = $value;
    }
    
    static public function get($key, $default = null, $throw = false) {
        if(isset(self::$values[$key])) {
            return self::$values[$key];
        }
        if($throw) {
            throw new \Exception(sprintf('The key "%s" does not exist in the registry', $key));
        }
        return $default;
    }
}
