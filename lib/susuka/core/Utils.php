<?php

namespace susuka\core;

class Utils {

    static function getUserFile($file, $exist = false) {
        $pos = strrpos($file, '.');
        if($pos !== false) {
            $result = substr($file, 0, $pos).'.user'.substr($file, $pos);
        } else {
            $result = $file.'.user';
        }
        if($exist && !file_exists($result)) {
            return false;
        }
        return $result;
    }
    
    static function getClassName($object) {
        $class = is_string($object) ? $object : get_class($object);
        $parts = explode('\\', $class);
        return end($parts);
    }
    
    static function getNamespace($object) {
        $class = is_string($object) ? $object : get_class($object);
        $parts = explode('\\', $class, -1);
        return implode('\\', $parts);
    }
    
    static function arrayMerge(array $array1, array $array2) {
        $result = array();
        $arrays = func_get_args();
        foreach($arrays as $array) {
            foreach($array as $key => $value) {
                if(is_string($key)) {
                    if(is_array($value) && array_key_exists($key, $result) && is_array($result[$key])) {
                        $result[$key] = self::arrayMerge($result[$key], $value);
                    } else {
                        $result[$key] = $value;
                    }
                } else {
                    $result[] = $value;
                }
            }
        }
        return $result;
    }
}
