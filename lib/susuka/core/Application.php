<?php

namespace susuka\core;

use susuka\core\Request as Request;
use susuka\exception\Argument as ArgumentException;
use susuka\exception\Core as CoreException;
use susuka\exception\NotFound as NotFoundException;
use susuka\exception\NotImplemented as NotImplementedException;

// FIXME...
define('SU_APP_ENVIRONMENT_DEV', 'dev');
define('SU_APP_ENVIRONMENT_TESTING', 'testing');
define('SU_APP_ENVIRONMENT_STAGING', 'staging');
define('SU_APP_ENVIRONMENT_PRODUCTION', 'production');

// FIXME: Move these constants
if(!defined('SU_MODULE_DIR')) define('SU_MODULE_DIR', 'module');
if(!defined('SU_DEFAULT_CONFIG_FILE')) define('SU_DEFAULT_CONFIG_FILE', 'config/config.php');
if(!defined('SU_DEFAULT_ROUTES_FILE')) define('SU_DEFAULT_ROUTES_FILE', 'config/routes.php');

/**
 * @todo Static class?
 * @todo initXxx => __call?
 * 
 * Defines
 * SU_APP_PATH
 * SU_APP_ENVIRONMENT
 * SU_LIB_PATH
 */
class Application {
    protected $path;
    protected $libPath;
    protected $environment;
    protected $modules = array();
    protected $components = array();
    protected $init = array('autoloader', 'cache', 'config', 'locale', 'controller');
    protected static $instance;
    
    static function boot() {
        if(!defined('SU_APP_PATH')) throw new \Exception('SU_APP_PATH has not been defined!');
        if(!defined('SU_APP_ENVIRONMENT')) define('SU_APP_ENVIRONMENT', 'dev');
        if(!defined('SU_LIB_PATH')) define('SU_LIB_PATH', SU_APP_PATH.'/../../lib/');
        
        $file = SU_APP_PATH.'/App.php';

        if(file_exists($file)) {
            require($file);
            $class = defined('SU_APP_CLASS') ? SU_APP_CLASS : basename(SU_APP_PATH).'\\App';
        } else {
            if(SU_LOG) \suLog::w('Using default application class');
            $class = defined('SU_APP_CLASS') ? SU_APP_CLASS : __CLASS__;
        }

        return new $class();
    }
    
    static function instance() {
        return self::$instance;
    }

    function __construct() {
        $this->name = SU_APP_NAME;
        $this->path = realpath(SU_APP_PATH);
        $this->libPath = realpath(SU_LIB_PATH);
        $this->environment = SU_APP_ENVIRONMENT;
        Registry::set('app', $this);
        self::$instance = $this;
    }
    
    function getPath() {
        return $this->path;
    }
    
    function getModules() {
        return $this->modules;
    }
    
    function getComponents() {
        return $this->components;
    }

    function run() {
        try {
            if(SU_LOG) {
                \suLog::d('==== RUN ====');
                \suLog::d('Environment=%s, Path=%s, LibPath=%s', $this->environment, $this->path, $this->libPath);
            }
            $this->init();
            $this->components['controller']->run(); // outside try?
            if(SU_LOG && defined('SU_DEBUG_TIMER') && SU_DEBUG_TIMER) {
                $time = (microtime(true) - SU_DEBUG_TIMER) * 1000;
                \suLog::n('%.05fms', $time);
            }
        } catch(\Exception $ex) {
            // TODO: Route to ErrorController
            if($ex instanceof NotFoundException && $this->environment !== SU_APP_ENVIRONMENT_DEV) {
                header('HTTP/1.0 404 Not Found');
                echo '404 Not Found';
                die;
            }
            throw $ex;
        }
    }
    
    public function __call($name, array $args) {
        if(strncmp($name, 'get', 3) === 0) {
            if(count($args) > 1) ArgumentException::raise('Method %s only accepts 0 or 1 arguments', $name);
            $component = lcfirst(substr($name, 3));
            return $this->getComponent($component, empty($args) ? true : $args[0]);
        }
        ArgumentException::raise('Method %s does not exist', $name);
    }
    
    protected function init() {
        foreach($this->init as $component) {
            $this->getComponent($component, true);
        }
    }
    
    protected function initConfig($instance) {
        $instance->load($this->path.'/'.SU_DEFAULT_CONFIG_FILE);
    }
    
    protected function initRouter($instance) {
        $instance->load($this->path.'/'.SU_DEFAULT_ROUTES_FILE);
    }
    
    protected function initTranslator($instance) {
        foreach($this->modules as $module) {
            if(isset($module['fullpath'])) {
                $instance->loadPath($module['fullpath'].'/config');
            }
        }
        $instance->loadPath($this->path.'/config');
    }
    
    protected function initAutoloader($instance) {
        $instance->addPath($this->path, $this->name);
        $instance->addAlias('$helper', '\\susuka\\helper');
    }
    
    protected function createAutoloader($config) {
        return Autoloader::getInstance();
    }
    
    protected function createLocale() {
        $locale = $this->components['config']->get('app.locale');
        if(!$locale && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) $locale = \Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        if($locale) \Locale::setDefault($locale);
        return \Locale::getDefault();
    }
    
    protected function createConfig($config) {
        return new \susuka\config\Php;
    }
    
    protected function createCache($config) {
        if($this->environment === SU_APP_ENVIRONMENT_DEV) {
            return new \susuka\cache\Dev;
        }
        return extension_loaded('apc') ? new \susuka\cache\Apc : new \susuka\cache\File;
    }

    protected function createRouter($config) {
        return new \susuka\router\Router($config);
    }

    protected function createRequest($config) {
        return new Request;
    }
    
    protected function createAuth($config) {
        return new \susuka\auth\Auth($config);
    }
    
    protected function createSession($config) {
        return new \susuka\session\Php($config);
    }

    /**
     * @todo This is ugly...
     * @todo Deal with mod-error not enabled
     */
    protected function createController($config) {
        $router = $this->getRouter();
        $request = $this->getRequest();
        $request = $router->match($request);
        if(!$request) {
            if(!$this->getConfig()->find('app.module', array('error', 'enable'))) {
                NotFoundException::raise('Router could not match request "%s"', (string)$this->getRequest());
            }
            $request = new Request(NULL, array(
                'get' => array('action' => 404, 'request' => (string)$this->getRequest()),
                'controller' => 'error/controller/Index',
            )); 
        }
        if(SU_APP_ENVIRONMENT === 'dev') {
            $controller = $this->_initControllerFromRequest($request);
        } else {
            try {
                $controller = $this->_initControllerFromRequest($request);
            } catch(\susuka\exception\Exception $ex) {
                $request = new Request(NULL, array(
                    'get' => array('action' => 500, 'request' => (string)$request),
                    'controller' => 'error/controller/Index',
                )); 
                $controller = $this->_initControllerFromRequest($request);
            }
        }
        return $controller;
    }
    
    protected function createTranslator($config) {
        return new Translator;
    }
    
    /*** TODO: Rewrite code below to use Manager! ***/
    
    protected function _initControllerFromRequest($request) {
        $class = $request->getParam('controller');
        if(is_array($class)) {
            $params = $class;
            $class = array_shift($params);
        } else {
            $params = array();
        }
        // TODO: Maybe request should resolve the full classname & module?
        $moduleName = current(explode('/', $class, 2));
        $this->loadModule($moduleName); 
        $class = $this->getAutoloader()->getClassName($class);
        if(!class_exists($class)) {
            NotFoundException::raise('Controller class "%s" not found', $class);
        }
        $controller = new $class(array('module' => $moduleName));
        $controller->setRequest($request);
        $controller->setResponse(new Response);
        return $controller;
    }

    /**
     * @todo Multiple paths, so app/module/foo can override the config for lib/module-foo
     * @todo Module class?
     * @todo Allow libs to specify their own root NS?
     * @todo Cache
     * @todo Autoloader paths should be initialized by LibMgr??
     * @todo Add path to FileMGr (for assets and stuff)
     */
    public function loadModule($name) {
        if(isset($this->modules[$name])) {
            return $this->modules[$name];
        }
        
        $config = $this->components['config']->get('app.module');
        
        if(!isset($config[$name])) {
            //CoreException::raise('Cannot load module "%s", the module is not configured in the application configuration!', $name);
            // FIXME: Hmmm
            $module = array('enable' => true);
        } else {        
            $module = $config[$name];
        }
        
        // TODO: Hmm..
        if(isset($module['enable']) && !$module['enable']) {
            CoreException::raise('Cannot load module "%s", the module is not enabled!', $name);
        }
        
        $appModule = false;
        
        // Discover fullpath
        if(isset($module['fullpath'])) {
            if(!is_dir($module['fullpath'])) {
                NotFoundException::raise('The full path specified for module "%s" does not exist!', $name);
            }
        } else {
            // FIXME!!!
            // Try app/module/
            $path = isset($module['path']) ? $module['path'] : $this->path.'/'.SU_MODULE_DIR.'/'.$name;
            if(is_dir($path)) {
                $appModule = true;
                $fullpath = $path;
            } else if(is_dir($this->libPath.'/'.$path)) { // That failed, try lib/${module-path}/
                $fullpath = $this->libPath.'/'.$path;
            } else if(is_dir($this->libPath.'/'.$name)) { // Try lib/${module-name}/
                $fullpath = $this->libPath.'/'.$name;
            } else {
                NotFoundException::raise('Could not find the full path for module "%s" ("%s" is not a directory)!', $name, $path);
            }
            
            $module['path'] = $path;
            $module['fullpath'] = realpath($fullpath);
        }
        
        // Add autoloader path
        if($appModule) {
            $prefix = implode('/', array_slice(explode('/', $module['fullpath']), -3));
        } else {
            $prefix =  str_replace('-', '/', $module['path']);
        }
        
        // Hmm
        $autoload = $module['fullpath'].'/autoload.php';
        if(is_file($autoload)) {
            require_once $autoload;
        }
        
        $loader = $this->getAutoloader();
        
        // Add the autoloader prefix
        $loader->addPath($module['fullpath'], $prefix);
        
        // Create alias (so we don't have to specify the full class in routes and stuff)
        $loader->addAlias($name.'/', $prefix);
        
        // Load module config
        $module = $this->loadModuleConfig($module);
        
        // Load strings
        if($translator = $this->getTranslator(false)) {
            $translator->loadPath($module['fullpath'].'/config');
        }
        
        // TODO: Performance?
        $this->components['config']->set('app.module', array($name => $module), true);
        
        $this->modules[$name] = $module;
        
        // Initialize dependencies (fixme recursion...)
        if(isset($module['dependencies'])) {
            foreach($module['dependencies'] as $k => $v) {
                if(is_string($v)) {
                    $this->loadModule($v);
                } else {
                    // FIXME: Other format
                    NotImplementedException::raise();
                }
            }
        }
        
        return $this->modules[$name];
    }
    
    /**
     * @todo .user config?
     */
    protected function loadModuleConfig($module) {
        // FIXME: Performance?
        $moduleConfigFile = $module['fullpath'].'/'.SU_DEFAULT_CONFIG_FILE;
        if(is_file($moduleConfigFile)) {
            $config = new \susuka\config\Php(); // FIXME...
            $config->load($moduleConfigFile);
            $config->merge($module, true);
            $module = $config->values();
        }
        return $module;
    }
    
    protected function getComponent($name, $instance) {
        if(isset($this->components[$name])) {
            return $this->components[$name];
        }
        if($instance === false) return false;
        return $this->initComponent($name, $instance);
    }
    
    protected function initComponent($name, $instance) {
        if(!is_object($instance)) {
            if($config = $this->getConfig(false)) {
                $config = $config->find('app.component', array(strtolower($name)), array());
            } else {
                $config = array();
            }
            if(is_array($instance)) {
                $config = \susuka\core\Utils::arrayMerge($config, $instance);
            }
            $instance = $this->createComponent($name, $config);
        }
        if(!isset($this->components[$name])) {
            Registry::set($name, $instance);
            $this->components[$name] = $instance;
        }
        $method = array($this, 'init'.$name);
        if(method_exists($method[0], $method[1])) {
            call_user_func($method, $instance);
        }
        return $instance;
    }
    
    protected function createComponent($name, $config) {
        if(isset($config['class'])) {
            $instance = new $config['class'];
        } else {
            $method = array($this, 'create'.$name);
            if(method_exists($method[0], $method[1])) {
                $instance = call_user_func($method, $config);
            } else {
                CoreException::raise('Cannot create component "%s", no classname or create method', $name);
            }
        }
        // TODO: if($instance instanceof Component...)?
        $method = array($instance, 'setApplication');
        if(method_exists($method[0], $method[1])) {
            call_user_func($method, $this);
        }
        // TODO: Maybe standardize the ctors for "components" instead?
        $method = array($instance, 'initComponent');
        if(method_exists($method[0], $method[1])) {
            call_user_func($method, $config);
        }
        return $instance;
    }
}

