<?php

namespace susuka\core;

use \susuka\core\Uri as Uri;
use \susuka\exception\NotFound as NotFoundException;

/**
 * @todo Move to request\Http?
 */
class Request {
    protected $uri;
    
    /**
     * 'controller' => 'classname'
     * 'get' => array() - get args
     * 'post' => post args
     */
    protected $params;
    
    function __construct($uri = NULL, $params = NULL) {
        $this->uri = $uri ? $uri : new Uri();
        $this->params = $params ? $params : array();
        if(empty($this->params['post'])) $this->params['post'] = $_POST;
    }
    
    function getUri() {
        return $this->uri;
    }
    
    function get($type, $key, $default = null, $throw = false) {
        if(isset($this->params[$type][$key])) {
            return $this->params[$type][$key];
        }
        return $throw ? NotFoundException::raise('The %s variable "%s" is not set!', $type, $key) : $default;
    }
    
    function getArray($type, array $keys, array $default = array(), $throw = false) {
        if(isset($this->params[$type])) {
            return static::filterValues($this->params[$type], $keys, $default, $throw);
        }
        if($throw) NotFoundException::raise('No %s variables found', $type);
        return false;
    }
    
    function getGet($key, $default = null, $throw = false) {
        return $this->get('get', $key, $default, $throw);
    }
    
    function getPost($key, $default = null, $throw = false) {
        return $this->get('post', $key, $default, $throw);
    }
    
    public function getGets(array $keys, array $default = array(), $throw = false) {
        return $this->getArray('get', $keys, $default, $throw);
    }
    
    public function getPosts(array $keys, array $default = array(), $throw = false) {
        return $this->getArray('post', $keys, $default, $throw);
    }
    
    public function getParams() {
        return $this->params;
    }
    
    public function getParam($key, $default = null) {
        return isset($this->params[$key]) ? $this->params[$key] : $default;
    }
    
    public function setParam($key, $value) {
        $this->params[$key] = $value;
    }
    
    public function __toString() {
        return (string)$this->uri;
    }
    
    public function getId() {
        return (string)$this->uri.print_r($this->params, true);
    }
    
    /**
     * 
     * @param $keys Array of required keys, a key with the value NULL will not be included in the result.
     * @example $request->getPosts(array('submit', 'user', 'pass')) # Require all 3 fields
     * @example $request->getPosts(array('submit' => false, 'user', 'pass')) # Require submit, user, pass but don't return submit
     * @example $request->getPosts(array('submit' => false, 'user', 'pass')) # Require submit, user, pass but don't return submit
     * @todo Regex matching?
     * 
     * Callback($input) return value:
     *   true: input ok
     *   false: return false
     *   null: filter input
     *   other value: replace input ????
     */
    public static function filterValues(array $source, array $keys, array $default, $throw) {
        $result = array();
        foreach($keys as $key => $value) {
            if(is_numeric($key)) {
                $key = $value;
                $value = true;
            }
            if(isset($source[$key])) {
                $input = $source[$key];
                if(!is_string($value) && is_callable($value)) {
                    $value = call_user_func($value, $input);
                    if(!is_bool($value) && !is_null($value)) {
                        $input = $value; # FIXME! document
                    }
                }
                if($value !== true) {
                    if($value === false) {
                        if($throw) NotFoundException::raise('The value "%s" (%s) is not valid "%s"!', $key, $input, $value);
                        return false;
                    } else if($value === null) {
                        continue;
                    } else if(is_string($value) && strcmp($input, $value) !== 0) {
                        if($throw) NotFoundException::raise('The value "%s" (%s) does not equal "%s"!', $key, $input, $value);
                        return false;
                    }
                }
                $result[$key] = $input;
            } else if(isset($default[$key])) {
                $result[$key] = $default[$key];
            } else if($throw) {
                NotFoundException::raise('The value "%s" is not set!', $key);
            } else {
                return false;
            }
        }
        return $result;
    }
}
