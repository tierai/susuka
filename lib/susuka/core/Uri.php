<?php

namespace susuka\core;

class Uri {
    protected $uri;
    static protected $defaultPorts = array(
        'http' => 80,
        'https' => 443,
    );
    
    public function __construct($uri = NULL) {
        if($uri === NULL) {
            $https = self::isServerHttps();
            $scheme = $https ? 'https' : 'http';
            $path = explode('?', $_SERVER['REQUEST_URI'], 2);
            $this->uri = array(
                'scheme' => $scheme,
                'host' => $_SERVER['HTTP_HOST'],
                'port' => $_SERVER['SERVER_PORT'] == self::$defaultPorts[$scheme] ? NULL : $_SERVER['SERVER_PORT'], 
                'path' => urldecode($path[0]), 
                'query' => isset($path[1]) ? $path[1] : $_SERVER['QUERY_STRING'],
            );
        } else if(is_array($uri)) {
            $this->uri = $uri;
        } else {
            $this->uri = parse_url($uri);
            if($this->uri === FALSE) {
                \susuka\exception\Argument::raise('Invalid URI "%s"', $uri);
            }
        }
    }
    
    public function getPart($part) {
        return isset($this->uri[$part]) ? $this->uri[$part] : false;
    }
    
    public function setPart($part, $value) {
        $this->uri[$part] = $value;
    }
    
    public function getScheme() {
        return isset($this->uri['scheme']) ? $this->uri['scheme'] : false;
    }
    
    public function getHost() {
        return isset($this->uri['host']) ? $this->uri['host'] : false;
    }
    
    public function getPort() {
        return isset($this->uri['port']) ? $this->uri['port'] : false;
    }
    
    public function getUser() {
        return isset($this->uri['user']) ? $this->uri['user'] : false;
    }
    
    public function getPass() {
        return isset($this->uri['pass']) ? $this->uri['pass'] : false;
    }
    
    public function getPath() {
        return isset($this->uri['path']) ? $this->uri['path'] : false;
    }
    
    public function setPath($value) {
        $this->uri['path'] = $value;
    }
    
    public function getQuery() {
        return isset($this->uri['query']) ? $this->uri['query'] : false;
    }
    
    public function getFragment() {
        return isset($this->uri['fragment']) ? $this->uri['fragment'] : false;
    }
    
    public function getUriArray() {
        return $this->uri;
    }
    
    /**
     * @todo Implement
     */
    public function getRootPath() {
        \susuka\exception\NotImplemented::raise(__METHOD__);
    }
    
    /**
     * @todo Implement
     */
    public function setRootPath($value) {
        \susuka\exception\NotImplemented::raise(__METHOD__);
    }
    
    /**
     * Returns the "local" path for the current site.
     * Example:
     *  An application is running with under: example.com/~user/site
     *      anything after that is the local path.
     * @todo Implement this
     */
    public function getLocalPath() {
        if(SU_LOG) \suLog::w('Stub');
        return $this->getPath();
    }
    
    /**
     * Sets the "local" path.
     * @note Same as $uri->setPath($uri->getRootPath().'/new/local/path');
     * @todo Implement this 
     */
    public function setLocalPath($value) {
        if(SU_LOG) \suLog::w('Stub');
        $this->setPath($value);
    }
    
    public function getBaseHost() {
        $config = Registry::get('config');
        if($config && $config->value('app.hostname', $host)) {
            return $host;
        }
        return $_SERVER['HTTP_HOST'];
    }
    
    /**
     * Temp hack to set the host specified in the app config
     */
    public function setAppHost() {
        $this->uri['host'] = $this->getBaseHost();
    }
    
    public function __toString() {
        $scheme = $this->getScheme();
        if(empty($scheme)) $scheme = 'http';
        $result = $scheme.'://';
        
        if($part = $this->getHost()) {
            $result .= $this->getHost();
        } else {
            $result .= 'localhost';
        }
        
        if($part = $this->getPort()) {
            if(!isset(self::$defaultPorts[$scheme]) || $port != self::$defaultPorts[$scheme]) {
                $result .= ':'.$part;
            }
        }
        
        if($part = $this->getPath()) {
            $result .= $part[0] === '/' ? $part : '/'.$part;
        }
        
        if($part = $this->getQuery()) {
            $result .= '?'.$part;
        }
        
        if($part = $this->getFragment()) {
            $result .= '#'.$part;
        }
        
        return $result;
    }
    
    static protected function isServerHttps() {
        if(isset($_SERVER['HTTPS'])) {
            return !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off';
        }
        return false;
    }
}
