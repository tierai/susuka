<?php

namespace susuka\core;

/**
 * Autoloader class
 * 
 * @todo Explain namespace separator usage (/ vs \\)
 */
class Autoloader {
    static protected $instance;
    protected $paths = array();
    protected $aliases = array();
    protected $cache;
    
    public static function getInstance() {
        if(static::$instance === null) self::$instance = new static;
        return static::$instance;
    }
    
    public function __construct() {
        \spl_autoload_register(array($this, 'load'));
    }
    
    public function setCache($cache) {
        $this->cache = $cache;
    }
    
    public function addPath($path, $prefix = false) {
        array_unshift($this->paths, array($path, $prefix));
    }
    
    public function getPaths() {
        return $this->paths;
    }
    
    public function addAlias($prefix, $replace) {
        $prefix = str_replace('\\', '/', $prefix);
        $prefix = rtrim($prefix, '/').'/';
        $replace = str_replace('\\', '/', $replace);
        $replace = rtrim($replace, '/').'/';
        if(isset($this->aliases[$prefix])) {
            array_unshift($this->aliases[$prefix], $replace);
        } else {
            $this->aliases[$prefix][] = $replace;
        }
    }
    
    public function getClassName($class) {
        $name = str_replace('\\', '/', $class);
        foreach($this->aliases as $prefix => $replacements) {
            if(strncmp($name, $prefix, strlen($prefix)) === 0) {
                foreach($replacements as $replace) {
                    $test = str_replace('/', '\\', $replace.substr($name, strlen($prefix)));
                    if(class_exists($test, false) || $this->load($test)) return $test;
                }
            }
        }
        return $class;
    }

    public function load($class) {
        if(!$this->cache || !$this->cache->value(__CLASS__, $class, $file)) {
            $file = $this->getInclude($class);
            if(!$file) return false;
            if($this->cache) $this->cache->set(__CLASS__, $class, $file);
        }
        $dir = dirname($file);
        $init = $dir.'/autoload.php';
        if(is_file($init)) require_once $init;
        require_once $file;
        return true;
    }
    
    protected function getInclude($class) {
        $name = ltrim(str_replace('\\', '/', $class), '/');
        foreach($this->paths as $path) {
            if($path[1]) {
                if(strncmp($name, $path[1], strlen($path[1])) === 0) {
                    $file = substr($name, strlen($path[1]));
                } else {
                    continue;
                }
            } else {
                $file = $name;
            }
            $file = $path[0].'/'.$file.'.php';
            if(is_file($file)) return $file;
        }
        return false;
    }
}
