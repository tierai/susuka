<?php

namespace susuka\core;

class Random {
    
    static function string($length) {
        $result = '';
        do {
            #$result = base_convert(str_replace('.', '', uniqid(mt_rand(), true)), 16, 36);
            #$result = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
            #$result = sha1(str_replace('.', '', uniqid(mt_rand(), true)), true);
            $data = sha1(uniqid(mt_rand(), true), true);
            $result .= str_replace(array('+', '/', '='), '', base64_encode($data));
        } while(strlen($result) < $length);
        return substr($result, 0, $length);
    }
}
