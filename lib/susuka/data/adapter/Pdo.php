<?php

namespace susuka\data\adapter;

use \susuka\data\query\Pdo as PdoQuery;
use \susuka\exception\Core as CoreException;
use \susuka\core\Registry as Registry;

/**
 * Adapter implementation using PHP Data Objects.
 * @todo Move statics to a Base class?
 * @todo Sort out exception and return values... (applies to susuka\data\*)
 * @todo Convert PDOException to NotFound etc... ?
 */
class Pdo implements Adapter {
    protected static $instance;
    protected $pdo;
    protected $config;
    
    public static function create($config = null) {
        return new Pdo($config);
    }
    
    /// TODO: Remove this, use app components
    public static function getInstance($instance = true) {
        if(null === self::$instance) {
            if(false === $instance) return null;
            if(is_object($instance)) self::$instance = $instance;
            else self::$instance = self::create();
        }
        return self::$instance;
    }
    
    public static function setInstance($instance) {
        self::$instance = $instance;
    }
    
    public static function getDefaultConfig() {
        return self::$defaultConfig;
    }
    
    /**
     * Adapter::setDefaultConfig(array('dsn' => 'mysql:dbname=testdb;host=127.0.0.1', 'user' => 'dbuser', 'password' => 'dbpass'));
     */
    public static function setDefaultConfig($config) {
        self::$defaultConfig = $config;
    }
    
    public function __construct($config = null) {
        $this->config = $config;
    }
    
    public function prepare($query, array $options = array()) {
        if(SU_LOG) \suLog::d('%s', $query);
        if($statement = $this->getPdo()->prepare($query, $options)) {
            return new PdoQuery($this, $statement);
        }
        CoreException::raise(); // We should not get here
    }
    
    /*public function exec($query, array $parameters = array()) {
        $result = $this->prepare($query);
        return $result->exec($parameters);
    }*/
    
    public function quote($string) {
        if($string instanceof \DateTime) {
            $string = $string->format('Y-m-d H:i:s'); // FIXME!
        }
        return $this->getPdo()->quote($string);
    }
    
    public function escape($string) {
        $result = $this->quote($string);
        # FIXME: this is wrong, example: escape("''asdfasdf'''");
        return trim($result, '\'');
    }
    
    public function getInsertId() {
        return $this->getPdo()->lastInsertId();
    }
    
    public function getPdo() {
        if(null == $this->pdo) {
            $this->pdo = $this->connect();
        }
        return $this->pdo;
    }
    
    public function isConnected() {
        return $this->pdo != NULL;
    }
    
    protected function connect() {
        $config = $this->config;
        if(null === $config) {
            if($config = Registry::get('config')) {
                $config = $config->get('db.default', array());
            }
        }
        if(!is_array($config)) {
            $config = $config->values();
        }
        if(isset($config['dsn'])) {
            $dsn = $config['dsn'];
        } else {
            if(SU_APP_ENVIRONMENT === SU_APP_ENVIRONMENT_PRODUCTION) {
                CoreException::raise('Will not use a temporary database in production, specify it in config if this really is what you want');
            }
            $dsn = 'sqlite:'.sys_get_temp_dir().'/'.sha1(__FILE__).'.sqlite';
            if(SU_LOG) \suLog::w('Using temp database %s!', $dsn);
        }
        $username = isset($config['username']) ? $config['username'] : false;
        $password = isset($config['password']) ? $config['password'] : false;
        $options = isset($config['options']) ? $config['options'] : array();
        $pdo = new \PDO($dsn, $username, $password, $options);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }
}
