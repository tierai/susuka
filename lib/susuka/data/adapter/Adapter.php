<?php

namespace susuka\data\adapter;

/**
 * Database adapter interface
 */
interface Adapter {
    public function prepare($query, array $options = array());
    public function escape($string);
    public function getInsertId();
}
