<?php

namespace susuka\data\model;

use susuka\data\mapper\Dynamic as DynamicMapper;

class Dynamic extends Base {

    /**
     * Gets the cached instance of the mapper for a Base model.
     * This method should be called on a class that's a subclass of Base, example: $mapper = \mystuff\model\User::getMapper($this);
     * @todo BaseMapper::getMapper?
     * @todo check $className != self? (must be a subclass of DynamicModel)
     * 
     * @param $context Database adapter interface or an object that has the getDataAdapter()  method.
     */
    public static function getMapper($context, $class = false) {
        return DynamicMapper::getMapper($context, $class ? $class : get_called_class());
    }
}
