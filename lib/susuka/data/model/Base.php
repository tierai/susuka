<?php

namespace susuka\data\model;

abstract class Base implements Model {
    protected $values;
    protected $modified = array();
    
    public function __construct(array $values = array()) {
        $this->values = $values;
    }
    
    public function get($key, $default = null, $throw = false) {
        if(isset($this->values[$key])) {
            return $this->values[$key];
        } else if($throw) {
            NotFoundException::raise($key);
        }
        return $default;
    }
    
    public function set($key, $value) {
        if(!isset($this->values[$key]) || $this->values[$key] != $value) {
            $this->values[$key] = $value;
            $this->modified[$key] = true;
        }
    }
    
    // FIXME?
    public function getId() {
        return $this->get('id');
    }
    
    public function getValue($key, &$value, $default = null) {
        if(isset($this->values[$key])) {
            $value = $this->values[$key];
            return true;
        }
        $value = $default;
        return false;
    }
    
    public function getValues() {
        return $this->values;
    }
    
    public function setValues($values, $append = false) {
        if(!$append) {
            $this->values = array();
            $this->modified = array();
        }
        foreach($values as $key => $value) {
            $this->set($key, $value);
        }
    }
    
    public function getModifiedKeys() {
        return array_keys($this->modified, true);
    }
    
    public function clearModifiedKeys() {
        $this->modified = array();
    }
    
    public function onCreated($mapper) {}
    public function onSaved($mapper) {}
    public function onDeleted($mapper) {}
    public function onSaving($mapper) {}
    public function onDeleting($mapper) {}
    
    protected function getOrSet($key, $value = null, $set = false) {
        if($set) {
            $this->set($key, $value);
            return $this;
        }
        return $this->get($key);
    }
}
