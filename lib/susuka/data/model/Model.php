<?php

namespace susuka\data\model;

/**
 * @note Models in Susuka are light-weight data containers and do not interface with the database directly.
 *       Loading, Saving & Updating a Model is done with a Mapper.
 */
interface Model {
    public function get($key, $default = null, $throw = false);
    public function set($key, $value);
    public function getId();
    public function getValue($key, &$value, $default = null);
    public function getValues();
    public function setValues($values, $append = false);
    public function getModifiedKeys();
    public function clearModifiedKeys();
    public function onCreated($mapper);
    public function onSaved($mapper);
    public function onDeleted($mapper);
    public function onSaving($mapper);
    public function onDeleting($mapper);
}
