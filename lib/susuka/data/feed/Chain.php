<?php

/**
 * @todo Sorting and merging of feed entries
 */
class Chain implements Feed {
    protected $feeds;
    protected $index;
    
    function __construct(array $feeds = array()) {
        $this->feeds = $feeds;
        $this->index = 0;
    }
    
    function add($feed) {
        $this->feeds[] = $feed;
    }
    
    function count() {
        $count = 0;
        foreach($this->feeds as $feed) {
            $count += $feed->count();
        }
        return $count;
    }
    
    function limit($limit) {
        NotImplementedException::raise();
    }
    
    function getUri() {
        return false;
    }
    
    // \Iterator impl
    function rewind() {
        $this->index = 0;
        foreach($this->feeds as $feed) {
            $feed->rewind();
        }
    }

    function current() {
        return $this->feeds[$this->index]->current();
    }

    function key() {
        return $this->index . '.' . $this->feeds[$this->index]->key();
    }

    function next() {
        while($this->index < count($this->feeds)) {
            $this->feeds[$this->index]->next();
            if($this->feeds[$this->index]->valid()) {
                break;
            } else {
                $this->index++;
            }
        }
    }

    function valid() {
        return $this->index < count($this->feeds) && $this->feeds[$this->index]->valid();
    }
}
