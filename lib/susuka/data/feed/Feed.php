<?php

/** 
 * @todo How is this generated? Should we require the feed value to be an array or maybe a Feed object?
 * 
 * Hmm, like an ORM query, but not quite
 * interface Feed:
 *   getUri()
 *   reset()
 *   limit(...)
 *   next()
 *   count()
 * 
 * controller::someAction():
 *   $feeds = array($allForumsFeed, $currentForumFeed);
 *   return array('title' => 'meh', 'feeds' => $feeds) -- NOTE: Rss feeds should always start on page 1...
 * 
 * in html view:
 *   <?php if(!$this->feed('forum')) : ?>NO ITEMS IN FEED "forum"<?php endif; ?>
 *   <?php while($this->next('item_')) : ?>   -- fetches the next feed item and sets it's values prefixed with item_
 *     <li><?php echo $this->get('item_title')?></li>
 *   <?php endwhile; ?>
 * 
 * V2:
 * controller::someAction():
 */
interface Feed extends \Iterator {
    function count();
    function limit($limit);
    function getUri();
    // Iterator
    function rewind();
    function current();
    function key();
    function next();
    function valid();
}

