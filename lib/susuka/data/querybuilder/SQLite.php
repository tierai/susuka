<?php

namespace susuka\data\querybuilder;

class SQLite implements QueryBuilder {

    function insert($mapper, $model, $keys, $values, $pk) {
        $adapter = $mapper->getAdapter();
        // Format keys & values
        $k = '';
        $v = '';
        foreach($keys as $key) {
            if($key !== $pk) {
                $k .= $adapter->escape($key).',';
                $v .= $adapter->quote($values[$key]).',';
            }
        }
        $k = substr($k, 0, -1); // Remove the trailing ,
        $v = substr($v, 0, -1); // Remove the trailing ,
        $query = sprintf('INSERT INTO # (%s) VALUES (%s)', $k, $v);
        $mapper->exec($query, array(), false);
    }
    
    function update($mapper, $model, $keys, $values, $pk, $id) {
        $adapter = $mapper->getAdapter();
        $items = '';
        foreach($keys as $key) {
            if($key !== $pk) {
                $items .= $adapter->escape($key).'='.$adapter->quote($values[$key]).',';
            }
        }
        $items = substr($items, 0, -1); // Remove the trailing ,
        $query = sprintf('UPDATE # SET %s WHERE '.$adapter->escape($pk).'=?', $items);
        $mapper->exec($query, array($id), false);
    }
    
    function updateTable($mapper, $model, $keys, $values, $pk) {
        try {
            $adapter = $mapper->getAdapter();
            $result = $mapper->exec('PRAGMA table_info(#)', array(), false);
            if($table = $result->fetch()) {
                //if(SU_LOG) \suLog::d('table=%s (%s)', $table, gettype($table));
                do {
                    $columns[$table['name']] = $table;
                } while($table = $result->fetch());
                // TODO: Check column type..
                $diff = array_diff($keys, array_keys($columns));
                if(SU_LOG) \suLog::d('cols=%s, diff=%s', array_keys($columns), $diff);
                foreach($diff as $key) {
                    $mapper->exec('ALTER TABLE # ADD COLUMN '.$adapter->escape($key), array(), false);
                }
            } else {
                // Table does not exist, we need all keys
                $keys = array_keys($values);
                // Format keys
                $k = '';
                foreach($keys as $key) {
                    if($key === $pk) {
                        $k .=  $adapter->escape($key).' INTEGER PRIMARY KEY,';
                    } else {
                        $k .= $adapter->escape($key).',';
                    }
                }
                $k = substr($k, 0, -1); // Remove the trailing ,
                // TODO: Create schema based on key values & meta-data
                $query = sprintf('CREATE TABLE # (%s)', $k);
                $mapper->exec($query, array(), false);
            }
        } catch(\PDOException $ex) {
            if(SU_LOG) \suLog::d('Could not fetch table info: '.$ex->getMessage());
            throw $ex;
        }
    }
}
