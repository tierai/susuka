<?php

namespace susuka\data\querybuilder;

class MySQL implements QueryBuilder {

    function insert($mapper, $model, $keys, $values, $pk) {
        $adapter = $mapper->getAdapter();
        // Format keys & values
        $k = '';
        $v = '';
        foreach($keys as $key) {
            if($key !== $pk) {
                $k .= $adapter->escape($key).',';
                $v .= $adapter->quote($values[$key]).',';
            }
        }
        $k = substr($k, 0, -1); // Remove the trailing ,
        $v = substr($v, 0, -1); // Remove the trailing ,
        $query = sprintf('INSERT INTO # (%s) VALUES (%s)', $k, $v);
        $mapper->exec($query, array(), false);
    }
    
    function update($mapper, $model, $keys, $values, $pk, $id) {
        $adapter = $mapper->getAdapter();
        $items = '';
        foreach($keys as $key) {
            if($key !== $pk) {
                $items .= $adapter->escape($key).'='.$adapter->quote($values[$key]).',';
            }
        }
        $items = substr($items, 0, -1); // Remove the trailing ,
        $query = sprintf('UPDATE # SET %s WHERE '.$adapter->escape($pk).'=?', $items);
        $mapper->exec($query, array($id), false);
    }
    
    // 'SELECT * FROM information_schema.tables WHERE TABLE_NAME="#"'
    function updateTable($mapper, $model, $keys, $values, $pk) {
        try {
            $adapter = $mapper->getAdapter();
            $result = $mapper->exec('DESCRIBE #', array(), false);
            //if(SU_LOG) \suLog::d('table=%s (%s)', $table, gettype($table));
            while($table = $result->fetch()) {
                $columns[$table['Field']] = $table;
            }
            // TODO: Check column type..
            $diff = array_diff($keys, array_keys($columns));
            if(SU_LOG) \suLog::d('cols=%s, diff=%s', array_keys($columns), $diff);
            foreach($diff as $key) {
                $mapper->exec('ALTER TABLE # ADD COLUMN '.$adapter->escape($key).' TEXT', array(), false);
            }
        } catch(\PDOException $ex) {
            if($ex->getCode() === '42S02') {
                // Table does not exist, we need all keys
                $keys = array_keys($values);
                // Format keys
                $k = '';
                foreach($keys as $key) {
                    if($key === $pk) {
                        $k .=  $adapter->escape($key).' INTEGER PRIMARY KEY AUTO_INCREMENT,';
                    } else {
                        $k .= $adapter->escape($key).' TEXT,';
                    }
                }
                $k = substr($k, 0, -1); // Remove the trailing ,
                // TODO: Create schema based on key values & meta-data
                $query = sprintf('CREATE TABLE # (%s)', $k);
                $mapper->exec($query, array(), false);
            } else {
                if(SU_LOG) \suLog::d('Could not fetch table info: '.$ex->getMessage());
                throw $ex;
            }
        }
    }
}
