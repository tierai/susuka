<?php

namespace susuka\data\query;

class None implements Query {

    function __construct() {
    }
    
    function exec(array $parameters = array()) {
        return false;
    }
    
    function count() {
        return 0;
    }
    
    function fetch() {
        return false;
    }
    
    function setHydrator($hydrator) {
    }
    
    public function getHydrator() {
        return false;
    }
    
    public function rewind() {
    }

    public function current() {
        return null;
    }

    public function key() {
        return null;
    }

    public function next() {
    }

    public function valid() {
        return false;
    }
}
