<?php

namespace susuka\data\query;

interface Query extends \Iterator {
    public function exec(array $parameters = array());
}
