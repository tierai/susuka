<?php

namespace susuka\data\query;

class Pdo implements Query {
    protected $adapter;
    protected $statement;
    protected $hydrator;
    protected $cache;
    protected $index;
    protected $complete;
    
    /**
     * @todo Store $mapper so we can hydrate results?
     */
    public function __construct($adapter, $statement) {
        $this->adapter = $adapter;
        $this->statement = $statement;
        $this->hydrator = false;
        $this->cache = array();
        $this->index = 0;
        $this->complete = false;
    }
    
    public function exec(array $parameters = array()) {
        if(SU_LOG) \suLog::d('%s -> %s', $parameters, $this->statement->queryString);
        if($this->statement->execute($parameters)) {
            //$this->statement->debugDumpParams();
            return $this;
        }
        return false;
    }
    
    // FIXME: SELECT COUNT(*)..
    public function count() {
        if(SU_LOG) \suLog::w('FIXME');
        return $this->statement->rowCount();
    }
    
    public function fetch() {
        $result = $this->statement->fetch(\PDO::FETCH_ASSOC);
        if($result) {
            if($this->hydrator) {
                if(is_callable($this->hydrator)) {
                    return call_user_func($this->hydrator, $result);
                }
                $result = $this->hydrator->create($result); ## FIXME!!!
            }
            $this->cache[] = $result;
        }
        return $result;
    }
    
    public function setHydrator($hydrator) {
        $this->hydrator = $hydrator;
    }
    
    public function getHydrator() {
        return $this->hydrator;
    }
    
    // \Iterator impl
    public function rewind() {
        $this->index = 0;
    }

    public function current() {
        return $this->cache[$this->index];
    }

    public function key() {
        return $this->index;
    }

    public function next() {
        $this->index++;
    }

    public function valid() {
        if(!$this->complete) {
            if($this->index >= count($this->cache)) {
                $this->complete = $this->fetch() ? false : true;
            }
        }
        return $this->index < count($this->cache);
    }
}
