<?php

namespace susuka\data\mapper;

/**
 * Mapper meta-data class.
 * 
 * Mapper meta-data is used to describe or give hints to the Mapper implementation, stuff like the table name, primary & secondary keys.
 * Some Mappers & Adapter implementations may not support and simply ignore a feature, like the column-type.
 * 
 * @todo setPrimaryKey, addSecondaryKey...
 */
class Meta {
    protected $values;
    
    public function __construct(array $values = array()) {
        $this->values = $values;
    }
    
    public function get($key, $default = null, $throw = false) {
        if(isset($this->values[$key])) {
            return $this->values[$key];
        } else if($throw) {
            NotFoundException::raise($key);
        }
        return $default;
    }
    
    public function set($key, $value) {
        $this->values[$key] = $value;
    }
    
    public function add(array $values) {
        $this->values = $values + $this->values;
    }
    
    public function getValue($key, &$value, $default = null) {
        if(isset($this->values[$key])) {
            $value = $this->values[$key];
            return true;
        }
        $value = $default;
        return false;
    }
    
    public function getValues() {
        return $this->values;
    }
    
    public function getTableName() {
        return $this->get('tableName');
    }
    
    public function setTableName($value) {
        $this->set('tableName', $value);
    }
    
    public function getPrimaryKey() {
        return $this->get('primaryKey');
    }
    
    public function setColumnType($name, $type) {
        $this->values['columns'][$name]['type'] = $type;
    }
}
