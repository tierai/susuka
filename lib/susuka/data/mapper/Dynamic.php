<?php

namespace susuka\data\mapper;

use \susuka\data\adapter\Pdo as PdoAdapter;
use \susuka\exception\Core as CoreException;
use \susuka\exception\NotFound as NotFoundException;
use \susuka\exception\NotImplemented as NotImplementedException;
use \susuka\exception\NotSupported as NotSupportedException;
use \susuka\exception\Argument as ArgumentException;

if(!defined('SU_MAPPER_CLASS_NAME')) define('SU_MAPPER_CLASS_NAME', '\susuka\data\model\Model');
if(!defined('SU_MAPPER_BASE_CLASS')) define('SU_MAPPER_BASE_CLASS', '\susuka\data\model\Model');
if(!defined('SU_MAPPER_DEFAULT_CLASS')) define('SU_MAPPER_DEFAULT_CLASS', '\susuka\data\model\Dynamic');

/**
 * @todo Table prefix, how? (Use #?)
 * @todo Table aliases for easy JOINS, something like: "SELECT ... FROM # LEFT JOIN #tableAlias ON ..."
 * @todo Option to generate & modify Model.php files (add & remove properties). Development/NonFreeze only.
 * @todo Option to freeze individual Mappers?
 * @todo Mapper resolver...
 */
class Dynamic extends Base {
    static protected $mappers = array();
    protected $tableName;
    protected $className; ///< Model classname
    protected $defaultClass;
    protected $baseClass;
    protected $frozen;
    protected $builder;
    
    /**
     * Returns the instance for a mapper.
     * Models of the same type can share mappers (they only carry the scheme for the Model).
     * @todo Move to Base?
     * @todo Resolver
     * @todo Don't rely on PdoAdapter..
     */
    public static function getMapper($context, $class = false) {
        if($context instanceof \susuka\data\adapter\Adapter) {
            $adapter = $context;
        } else if(method_exists($context, 'getDataAdapter')) {
            $adapter = $context->getDataAdapter();
        } else {
            // TODO: Do note use getInstance
            $adapter = PdoAdapter::getInstance();
        }
        if($adapter === null) ArgumentException::raise('$context does not provide a valid Adapter instance');
        $id = spl_object_hash($adapter).'?'.$class;
        if(isset(self::$mappers[$id])) {
            $mapper = self::$mappers[$id];
        } else {
            /// @todo get_called_class?
            /// @todo Resolver...
            $mapper = new self($class, $context, $adapter);
            self::$mappers[$id] = $mapper;
        }
        return $mapper;
    }
    
    /**
     * @todo Move to static, work with model\Dynamic::getDynamicMapper()
     * @todo Merge meta with $modelClass::$meta?
     */
    public function __construct($className = false, $context = false, $adapter = null, $meta = null) {
        parent::__construct($adapter, $meta);
        $this->frozen = SU_APP_ENVIRONMENT == SU_APP_ENVIRONMENT_PRODUCTION;
        if($className) {
            $this->className = $className;
        } else if($context) {
            $this->className = self::getModelClassFromContext($context, $className);
        } else {
            $this->className =  $this->meta->get('className', SU_MAPPER_CLASS_NAME);
        }
        if(!$this->className || !class_exists($this->className)) {
            NotFoundException::raise('The class "%s" does not exist!', $this->className);
        }
        $this->baseClass = $this->meta->get('baseClass', SU_MAPPER_BASE_CLASS);
        $this->defaultClass = $this->meta->get('defaultClass', SU_MAPPER_DEFAULT_CLASS);
        if(!$this->className) {
            $this->className = $this->defaultClass;
        }
        if(!is_subclass_of($this->className, $this->baseClass)) {
            CoreException::raise('The class "%s" is not a subclass of the base "%s"', $this->className, $this->baseClass);
        }
        $this->tableName = $this->meta->get('tableName');
        if(!$this->tableName) {
            $this->tableName = $this->getTableName($this->className);
        }
        // Notify the Model
        if(method_exists($this->className, 'onMapperCreated')) {
            call_user_func(array($this->className, 'onMapperCreated'), $this);
        }
    }
    
    // TODO: Move to base
    public function create(array $values = array()) {
        $class = $this->className;
        $model = new $class($values);
        $model->onCreated($this);
        return $model;
    }
    
    // TODO: Move to base, & interface?
    public function load($value, $default = false, $hydrator = true) {
        $result = $this->find($value, array(0, 1), false, $hydrator);
        if($data = $result->fetch()) {
            return $data;
        }
        if(!is_string($default) && is_callable($default)) {
            $default = call_user_func($default);
        }
        if($default === false) {
            return false;
        }
        return $this->create($default);
    }
    
    public function find($value = null, $limit = array(0, 1), $order = true, $hydrator = true) {
        if(is_array($value)) {
            $where = '';
            foreach($value as $key => $val) {
                $where .= $this->adapter->escape($key).'=? AND ';
            }
            $where = substr($where, 0, -5); // Remove last ' AND ';
            $values = array_values($value);
        } else {
            if(!$key = $this->getMeta()->getPrimaryKey()) {
                CoreException::raise('This model has no primary key');
            }
            $where = $this->adapter->escape($key).'=?';
            $values = array($value);
        }
        return $this->findHelper('SELECT * FROM # WHERE '.$where, $values, $limit, $order, $hydrator);
    }
    
    /**
     * @param $limit array(start, count), false = no limit
     * @param $order array(key, mode), true = PK!?
     */
    public function all($limit = false, $order = true, $hydrator = true) {
        $limit = $this->fixLimit($limit);
        return $this->findHelper('SELECT * FROM #', array(), $limit, $order, $hydrator);
    }
    
    /**
     * Execute a "raw" SQL-query. Much like a PDO query but you can use # to refer to the Mappers database table.
     * 
     * Example:
     * @code
     *   $result = $mapper->exec('SELECT title,content FROM # WHERE name = :name AND published = true', array('name' => $page));
     * @endcode
     * 
     * @note SQL-syntax depends on implementation.
     * 
     * @param $query Query string to execute
     * @param $parameters TODO
     * @param $hydrator TODO
     * @return Query result
     */
    public function exec($query, array $parameters = array(), $hydrator = true) {
        $result = $this->prepare($query);
        $result = $result->exec($parameters);
        if($result && $hydrator) {
            $result->setHydrator($hydrator === true ? $this : $hydrator);
        }
        return $result;
    }
    
    /**
     * Prepare an SQL query.
     * 
     * @todo Fix table name... (#)
     * @todo Table aliases for joins...
     * 
     * @param $query Query string
     * @return Prepared Query.
     */
    public function prepare($query) {
        $tablePrefix = $this->getMeta()->get('tablePrefix', ''); // TODO: Should be shared... try adapter options if not set in meta?
        $tableName = $this->tableName;
        # FIXME: Hack used to avoid # inside VALUES() used by save()
        # save() should use prepared statements
        $ugly = explode('\'', $query, 2);
        if(count($ugly) == 2) {
            $query = $ugly[0];
            $fixme = '\''.$ugly[1];
        } else {
            $fixme = '';
        }
        $query = preg_replace_callback('/(\#[a-z0-9]*)/i', function($matches) use($tablePrefix, $tableName) {
            if($matches[0] === '#') {
                return $tablePrefix.$tableName;
            }
            return $tablePrefix.substr($matches[0], 1);
        }, $query);
        return $this->adapter->prepare($query.$fixme);
    }
    
    /**
     * @todo save(&$model)? so save($myObj) sets $myObj to a Model instance if it isn't?
     * @todo QueryWriter
     * @todo Check & double-check for SQL-injections...
     * @note This is one of the few places where we use raw SQL queries, make sure that this code cannot be used for SQL-injections.
     */
    public function save($model) {
        $pk = $this->getMeta()->getPrimaryKey();
        $values = $this->getModelValues($model);
        
        if($pk && $model instanceof $this->baseClass && !empty($values[$pk])) {
            $id = $values[$pk];
            $keys = $model->getModifiedKeys(); // TODO: Handle PK change how?
            if(SU_LOG) \suLog::d('PK(%s)=%s [%s, %s]', $pk, $id, $keys, $values);
        } else {
            $id = false;
            $values[$pk] = $id;
            $keys = array_keys($values);
            if(SU_LOG) \suLog::d('No PK(%s) found in [%s, %s]', $pk, $keys, $values);
        }
        
        if(empty($keys)) {
            if(SU_LOG) \suLog::d('No modified keys');
            return;
        }
        
        if($model instanceof $this->baseClass) {
            $model->onSaving($this);
        }
        
        if(!$this->frozen) {
            // TODO: Allow dynamic tables in production? (it's slow, but could be useful for some).
            if(SU_APP_ENVIRONMENT === SU_APP_ENVIRONMENT_PRODUCTION) {
                CoreException::raise('Dynamic mappers are not allowed in a production environment');
            }
            $this->getBuilder()->updateTable($this, $model, $keys, $values, $pk);
        }
        
        if($id) {
            if(SU_LOG) \suLog::d('Updating #%s', $id);
            $this->getBuilder()->update($this, $model, $keys, $values, $pk, $id);
        } else {
            if(SU_LOG) \suLog::d('Creating new entry: PK=%s, %s', $pk, $values);
            $this->getBuilder()->insert($this, $model, $keys, $values, $pk);
            $id = $this->adapter->getInsertId(); // TODO: getInsertId($pk) ?
            if(SU_LOG) \suLog::d('New id #%s', $id);
            if(empty($id)) {
                CoreException::raise('Could not fetch new PK');
            }
            if($model instanceof $this->baseClass) {
                $model->set($pk, $id);
            }
        }
        
        if($model instanceof $this->baseClass) {
            $model->clearModifiedKeys();
            $model->onSaved($this);
        }
        
        // TODO ?return $model;
    }
    
    public function copy($model) {
        $values = $this->getModelValues($model);
        if($key = $this->getMeta()->getPrimaryKey()) {
            unset($values[$key]); // TODO: unset or null?
        }
        return $this->create($values);
    }
    
    public function delete($model) {
        NotImplementedException::raise();
        
        // TODO: Stuff, unset pk
        
        $model->onDeleting($this);
        $model->onDeleted($this);
    }
    
    /**
     * @todo Should we really catch ALL PDOExceptions here?
     *       We're currently doing this to avoid non-existing table exceptions...
     */
    protected function findHelper($query, $values, $limit, $order, $hydrator) {
        try {
            if($order = $this->fixOrder($order)) {
                #$query .= ' ORDER BY ?';
                #$values = array_merge($values, $order);
                $query .= ' ORDER BY '.$this->adapter->escape($order[0]).' '.$this->adapter->escape($order[1]);
            }
            if($limit = $this->fixLimit($limit)) {
                # FIXME: This does not work with MySQL (we have to bind LIMIT params..)
                #$query .= ' LIMIT ?,?';
                #$values = array_merge($values, $limit);
                $query .= ' LIMIT '.intval($limit[0]).','.intval($limit[1]);
            }
            return $this->exec($query, $values, $hydrator);
        } catch(\PDOException $ex) {
            if(SU_LOG) \suLog::d(''.$ex);
            return new \susuka\data\query\None;
        }
    }
    
    /**
     * @param $order ...
     */
    protected function fixOrder($order) {
        if($order === false) {
            return false;
        } else if($order === true) {
            $key = $this->getMeta()->getPrimaryKey();
            return array($key, 'ASC');
        } else if(is_string($order)) {
            return array($order, 'ASC');
        } else if(is_array($order) && count($order)) {
            // TODO: Check!?
            return $order;
        }
        ArgumentException::raise('$order should be an array(a, b), a string or boolean');
    }
    
    protected function fixLimit($limit) {
        if($limit === false) {
            return false;
        } else if(is_numeric($limit)) {
            return array(0, $limit);
        } else if(is_array($limit) && count($limit) == 2) {
            if(!is_numeric($limit[0]) || !is_numeric($limit[1])) ArgumentException::raise('$limit should only contain numeric values');
            return $limit;
        }
        ArgumentException::raise('$limit should be an array(a, b), a numeric value or false');
    }

    protected function getBuilder() {
        if(null === $this->builder) {
            $driver = $this->adapter->getPdo()->getAttribute(\PDO::ATTR_DRIVER_NAME);
            switch($driver) {
            case 'sqlite':
                $this->builder = new \susuka\data\querybuilder\SQLite;
                break;
            case 'mysql':
                $this->builder = new \susuka\data\querybuilder\MySQL;
                break;
            default:
                CoreException::raise('Cannot create QueryBuilder: Unknown driver "%s"', $driver);
            }
        }
        return $this->builder;
    }

    /**
     * Gets the field values from a Model as an array.
     */
    protected function getModelValues($model) {
        if($model instanceof $this->baseClass) {
            return $model->getValues();
        } else if($model instanceof stdclass) {
            $values = array();
            $reflect = new \ReflectionObject($model);
            foreach($reflect->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
                $values[$property->getName()] = $property->getValue($model);
            }
            return $values;
        } else if(is_array($model)) {
            return $model;
        } else {
            NotSupportedException::raise('Cannot get values from type "%s"', get_class($model));
        }
    }
    
    /**
     * @todo $this->getMeta('modelPath')?
     * @todo Depricate \\model\\?
     */
    protected static function getModelClassFromContext($context, $name) {
        if(is_object($context)) {
            $context = get_class($context);
            $context = substr($context, 0, strrpos($context, '\\', 0));
        } else if(!is_string($context)) {
            ArgumentException::raise('$context should be a string or an object (type: %s)', gettype($context));
        }
        $className = substr($context, 0, strrpos($context, '\\', 0));
        if(class_exists($className.'\\data\\model\\'.$name)) {
            return $className.'\\data\\model\\'.$name;
        } else if(class_exists($className.'\\model\\'.$name)) {
            return $className.'\\model\\'.$name;
        }
        return false;
    }
}
