<?php

namespace susuka\data\mapper;

abstract class Base implements Mapper {
    protected $adapter;
    protected $meta;
    
    function __construct($adapter, $meta = null) {
        $this->adapter = $adapter;
        $this->meta = $meta ? $meta : new Meta(array('primaryKey' => 'id')); // TODO: Default PK, how?
    }
    
    /*public function one($key); // find($key, 1)->next()
    public function find($key = null, $limit = 1);
    public function exec($query, array $parameters = array());
    public function prepare($query);
    public function save($entity);
    public function copy($entity);
    public function delete($entity) {
    }*/
    
    function getId() {
        if($key = $this->meta->get('primaryKey')) {
            return $this->get($key);
        }
        CoreException::raise('This model has no primary key');
    }
    
    function getMeta() {
        return $this->meta;
    }
    
    function getAdapter() {
        return $this->adapter;
    }
    
    protected function getTableName($className) {
        return str_replace('\\', '_', $className);
    }
    
    protected function getClassName($tableName) {
        return str_replace('_', '\\', $tableName);
    }
}
