<?php

namespace susuka\data\mapper;

/**
 * @todo Hydration, how & where
 * @todo move load() here?
 */
interface Mapper {
    public function load($value, $default = false, $hydrator = true);
    
    /**
     * Perform a search.
     * @todo ALWAYS returns a Query!? (if affected rows == 0?)
     * @param $value PrimaryKey value or an array with key => value pairs.
     * @param $limit Maximum number of results to return.
     */
    public function find($value = null, $limit = 1);
    
    /**
     * Execute a raw query.
     */
    public function exec($query, array $parameters = array());
    public function prepare($query);
    public function save($model);
    public function copy($model);
    public function delete($model);
    public function getMeta();   
}
