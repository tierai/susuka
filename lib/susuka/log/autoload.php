<?php

define('SU_LOG_NONE', 0); ///< No logging
define('SU_LOG_ERROR', 40); ///< Log errors
define('SU_LOG_WARNING', 60); ///< Log warnings
define('SU_LOG_NOTICE', 80); ///< Log notices.
define('SU_LOG_DEBUG', 100); ///< Debug logging. Generates at least 5k of logs per website hit, use in development environments only!

if(!defined('SU_LOG')) define('SU_LOG', \suConstant('SU_APP_ENVIRONMENT') === 'dev' ? SU_LOG_DEBUG : SU_LOG_NONE);
if(!defined('SU_ACCESS_LOG')) define('SU_ACCESS_LOG', SU_LOG_NONE);
if(!defined('SU_SECURITY_LOG')) define('SU_SECURITY_LOG', SU_LOG_NOTICE);

/**
 * Log wrappers
 * 
 * The following constants are always defined and their respective logger is always available if the constant is true:
 *  SU_LOG => \suLog
 *  SU_ACCESS_LOG => \suAccessLog
 *  SU_SECURITY_LOG => \suSecurityLog
 * 
 * Example:
 *  if(SU_LOG) \suLog::w('Ooops!');
 *  if(SU_SECURITY_LOG) \suSecurityLog::d('Omg!');
 */
if(SU_LOG) {
    class suLog extends \susuka\log\Log {}
}

if(SU_ACCESS_LOG) {
    class suAccessLog extends \susuka\log\Access {}
    suAccessLog::n('%s %s %s', $_SERVER['REMOTE_ADDR'], $_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
}

if(SU_SECURITY_LOG) {
    class suSecurityLog extends \susuka\log\Security {}
}
