<?php

namespace susuka\log;

class Access extends Log {
    public function __construct($file = null, $threshold = null) {
        if($file === null) $file = defined('SU_LOGFILE_ACCESS') ? SU_LOGFILE_ACCESS : SU_LOG_PATH.'/access.log';
        if($threshold === null) $threshold = SU_ACCESS_LOG;
        parent::__construct($file, $threshold);
    }    
}
