<?php

namespace susuka\log;

class Security extends Log {
    public function __construct($file = null, $threshold = null) {
        if($file === null) $file = defined('SU_LOGFILE_SECURITY') ? SU_LOGFILE_SECURITY : SU_LOG_PATH.'/security.log';
        if($threshold === null) $threshold = SU_SECURITY_LOG;
        parent::__construct($file, $threshold);
    }
}
