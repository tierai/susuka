<?php

namespace susuka\asset;

use susuka\core\FileSystem as FileSystem;
use susuka\exception\NotFound as NotFoundException;

/**
 * @todo Don't use defines
 * @todo Hardcoded paths
 * @todo Remove yui-compressor dependency (use some PHP minifier?)
 */
class Builder {
    static $instance;
    
    public static function getInstance() {
        if(self::$instance == NULL) self::$instance = new self;
        return self::$instance;
    }
    
    /**
     * @todo Cleanup $name
     */
    public function getPath($name) {
        return '/asset/'.SU_APP_NAME.'/'.$name;
    }
    
    public function getPrivatePath($name) {
        $path = SU_APP_PATH.'/asset/'.$name;
        if(is_file($path)) return $path;
        if($temp = glob($path.'.*')) return $temp[0];
        return FileSystem::locateFile($name, false, function($filename){
            if($temp = glob($filename.'.*')) return $temp[0];
            return $filename;
        });
    }
    
    public function getPublicPath($name) {
        return SU_PUBLIC_PATH.$this->getPath($name);
    }

    public function isUpToDate($name) {
        $src = $this->getPrivatePath($name);
        $dst = $this->getPublicPath($name);
        return is_file($dst) && filemtime($src) <= filemtime($dst);
    }

    public function build($name, $optimize = false) {
        $src = $this->getPrivatePath($name);
        $dst = $this->getPublicPath($name);
        if(!is_file($src)) NotFoundException::raise('Cannot find asset "%s"', $src);
        $this->compile($src, $dst, $optimize);
    }

    public function buildAll() {
        $result = $this->buildAllIn(SU_APP_PATH.'/asset');
        if($config = \susuka\core\Registry::get('config')) {
            // TODO app/module?
            foreach($config->get('app.module', array()) as $k => $module) {
                if(isset($module['fullpath']))
                    $path = $module['fullpath'];
                else if(isset($module['path']))
                    $path = SU_LIB_PATH.'/'.$module['path'];
                else
                    continue;
                $result = array_merge($result, $this->buildAllIn($path.'/asset', $k));
            }
        }
        return $result;
    }
    
    protected function compile($src, $dst, $optimize = false) {
        $ext = pathinfo($src, PATHINFO_EXTENSION);
        if(!$ext) NotSupportedException::raise('File "%s" has no file extension', $src);
        $ext = '.'.$ext;
        if(in_array($ext, array('.php', '.scss', '.sass', '.less'))) {
            if(strrpos($dst, $ext) === strlen($dst) - strlen($ext)) $dst = substr($dst, 0, -strlen($ext));
        }
        $dir = pathinfo($dst, PATHINFO_DIRNAME);
        if(!is_dir($dir)) mkdir($dir, 0777, true);
        if(SU_LOG) \suLog::d('Compiling asset "%s" => "%s"...', $src, $dst);
        switch($ext) {
        case '.php':
            ob_start();
            require($src);
            $result = ob_get_contents();
            ob_end_clean();
            if(pathinfo($dst, PATHINFO_EXTENSION)) {
                file_put_contents($dst, $result);
            }
            break;
        case '.scss':
        case '.sass':
            exec(sprintf('sass "%s" "%s"', $src, $dst));
            break;
        case '.less':
            exec(sprintf('lessc "%s" "%s"', $src, $dst));
            break;
        default:
            copy($src, $dst);
        }
        if($optimize) {
            $this->optimize($dst);
        }
        if(SU_LOG) \suLog::d('Asset "%s" OK', $src);
    }
    
    protected function optimize($file) {
        switch(pathinfo($file, PATHINFO_EXTENSION)) {
        case 'css':
        case 'js':
            $cmd = sprintf('yui-compressor -o "%s" "%s"', $file, $file);
            exec($cmd);
            break;
        case 'jpg':
        case 'jpeg':
            exec(sprintf('jpegtran -copy none -outfile "%s" "%s"', $file, $file));
            break;
        case 'png':
            exec(sprintf('convert "%s" -strip "%s"', $file, $file));
            break;
        }
    }
    
    protected function buildAllIn($path, $module = false) {
        $result = array();
        if(is_dir($path)) {
            $path = realpath($path);
            $it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, \RecursiveIteratorIterator::SELF_FIRST));
            foreach($it as $file) {
                if($file->isFile()) {
                    $src = (string)$file;
                    $name = substr($src, strlen($path));
                    if($module) $name = '@'.$module.'/asset/'.$name;
                    $dst = $this->getPublicPath($name);
                    $this->compile($src, $dst, true);
                    $result[] = $dst;
                }
            }
        }
        return $result;
    }
}
