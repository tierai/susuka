<?php

namespace susuka\router;

use \susuka\core\Utils as Utils;
use \susuka\core\Registry as Registry;
use \susuka\core\FileSystem as FileSystem;
use \susuka\core\Uri as Uri;

/**
 * @todo Route class
 */
class Router {
    protected $cache;
    protected $application;
    protected $routes = array();
    
    public function __construct($config = null) {
    }
    
    public function getApplication() {
        if($this->application === null) $this->application = Registry::get('app');
        return $this->application;
    }
    
    public function setApplication($value) {
        $this->application = $value;
    }
    
    public function getCache() {
        if($this->cache === null) $this->cache = $this->getApplication()->getCache(false);
        return $this->cache;
    }
    
    public function setCache($cache) {
        $this->cache = $cache;
    }
    
    public function getRoutes() {
        return $this->routes;
    }
    
    public function setRoutes($routes) {
        $this->routes = $routes;
    }
    
    public function load($filename) {
        $cache = $this->getCache();
        if($cache && $cache->value(__CLASS__, $filename, $this->routes)) {
            return true;
        }
        if(SU_LOG) \suLog::d('Load %s', $filename);
        $this->routes = array();
        $this->import($filename);
        if($cache) $cache->set(__CLASS__, $filename, $this->routes);
    }
    
    public function import($filename) {
        $file = FileSystem::locateFile($filename);
        require($file);
        if($file = Utils::getUserFile($file, true)) {
            require($file);
        }
    }
    
    public function clear() {
        $this->routes = array();
    }
    
    public function route($name, array $options) {
        // TODO: if($options[class])
        $this->routes[] = new Route($name, $options);
    }
    
    /**
     * @todo Cache invalid requests or not?
     */
    public function match($request) {
        $result = false;
        if($cache = $this->getCache()) {
            $id = $request->getId().\Locale::getDefault();
            if($cache->value(__CLASS__, $id, $result)) {
                return $result;
            }
        }
        foreach($this->routes as $route) {
            if($route = $route->match($request)) {
                // FIXME: Request::isFinal? 
                if($route->getParam('controller')) {
                    $result = $route;
                }
            }
        }
        if($cache) $cache->set(__CLASS__, $id, $result);
        return $result;
    }
    
    /**
     * @todo $parent with cache?
     */
    public function build($name, array $parameters = array(), $parent = null) {
        if($cache = $this->getCache()) {
            $id = $name.print_r($parameters, true);
            if($cache->value(__CLASS__, $id, $result)) {
                return $result;
            }
        }
        foreach($this->routes as $route) {
            if($result = $route->build($name, $parameters, $parent)) {
                if($cache) $cache->set(__CLASS__, $id, $result);
                return $result;
            }
        }
        // FIXME!
        if(SU_LOG) \suLog::w('No route entry for "%s"', $name);
        $result = new Uri();
        $result->setAppHost();
        $result->setPart('path', $name);
        $result->setPart('query', http_build_query($parameters, '', '&'));
        $result = (string)$result;
        if($cache) $cache->set(__CLASS__, $id, $result);
        return $result;
    }
}
