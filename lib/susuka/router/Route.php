<?php

namespace susuka\router;

use \susuka\core\Uri as Uri;
use \susuka\core\Request as Request;
use \susuka\exception\Core as CoreException;
use \susuka\exception\NotFound as NotFoundException;

/**
 * Route
 * 
 * routes.php format:
 * $this->add(string $routeName, array $parameters);
 * 
 * Matching requests:
 * 'format' Matches the format
 * 'prefix' Like format but will only match the beginning of the request
 * 'regexp' Full regexp matching
 * 
 * URI elements (in testing order):
 * 'host' Matches the request HOST ($_SERVER['HTTP_HOST']) using regex. Use $host to access the current hostname.
 * 'path' Matches the beginning of the request PATH. This is the default URI element.
 * 
 * Examples:
 * 'format' => '/user/:name' - Would match example.com/user/myUserName
 * 'host:format' => 'admin.$host' - Would match admin.example.com ($host is replaced with the current hostname)
 * 
 * All match params can be an array to match any of the specified params.
 * 
 * other parameters:
 * 
 * 'target'
 * Targets a controller (prefix: @) or a new request path.
 * Default arguments are specified as an assoc array like array('@demo/controller/mine', 'arg1' => 'hello')
 * You can use multiple targets to match the index of the prefix or pattern match like: array('/rewrite1', '/rewrite2', ...)
 * Targets that target a controller count as a final route and will not trigger any further route processing.
 * 
 * 'import'
 * Imports routes from anothe file.
 * These routes will be processed if the route is a match, any remaining routes in the default (parent) routing table will not be processed.
 * 
 * @todo Importing?, we have to import to existing router so it can use the imported routes to build URIs (or do we)?
 * ---------------------------------------------------------------------
 * @todo Disallow some params (like action) from the query, only allow them if they are set by the route (so action or format can't be set by using $_GET)
 * @todo Trailing /...
 * @todo Trailing /: rewritten routes (prefix => target/$suffix with trailing /) does not match... example.com/test != example.com/test/
 * @todo Final route explanation (target = @controller ...)
 * @todo Remove duplicate / (suffix)
 * --------------------
 * @todo THIS IS CONFUSING ME! WHO WROTE THIS!? >:( - We (I) have to document this and the routes.php format!
 */
class Route {
    static protected $elementRegexp = '/(\:[\-_\w]+)/i';
    protected $name;
    protected $params; ///< Unmodified params from the route config.
    protected $patterns;
    protected $targets; ///< Array of targets
    protected $defaultArguments;
    protected $import;
    protected $formats; ///< Used for building URIs
    protected $locale;

    public function __construct($name, array $params) {
        $this->name = $name;
        $this->params = $params;
        $this->initialize();
    }
    
    public function match($request, $parent = null) {
        $target = false;
        $controller = false;
        $arguments = array();
        $uri = $request->getUri();
        $index = 0;

        if(SU_LOG) \suLog::d('Route "%s" Matching URI %s', $this->name, (string)$uri);
        
        if($this->locale && !in_array(\Locale::getDefault(), $this->locale)) {
            if(SU_LOG) \suLog::d(' Locale %s did not match %s', \Locale::getDefault(), $this->locale);
            return false;
        }
                    
        foreach($this->patterns as $uriKey => $patterns) {
            $match = false;
            $count = count($patterns);
            $subject = $uri->getPart($uriKey);
            
            // FIXME: / & ending paths..
            if($uriKey == 'path') {
                $subject = rtrim($subject, '/').'/';
            }
            
            if(SU_LOG) \suLog::d(' Matching "%s" in URI %s component', $subject, $uriKey);
                
            for($i = 0; $i < $count; $i++) {
                //$pattern = str_replace('$host', $_SERVER['HTTP_HOST'], $patterns[$i]); // FIXME: This could be cased if we only use 1 hostname
                $pattern = $patterns[$i];
                if($match = self::matchPattern($pattern, $subject, $arguments)) {
                    if($uriKey == 'path') {
                        $index = $i;
                    }
                    // Only a single subpattern has to match, break out of loop
                    if(SU_LOG) \suLog::d('  Pattern "%s" matched "%s" (index=%d)', $pattern, $subject, $index);
                    break;
                } else {
                    if(SU_LOG) \suLog::d('  Pattern "%s" did NOT match "%s"', $pattern, $subject);
                }
            }
            
            // All patterns have to match
            if(!$match) {
                if(SU_LOG) \suLog::d(' No match found');
                return false;
            }
        }
        
        if(!isset($this->targets[$index])) {
            if(count($this->targets) == 1) {
                // Allow multiple patterns if we have a single target
                $index = 0;
            } else {
                CoreException::raise('No target specified for pattern %d', $index);
            }
        }
         
        if($this->targets[$index]) {
            $target = $this->targets[$index];
            $arguments += $this->defaultArguments;
            // TODO: Use names for path & suffix that are less likely to be used in a query...
            $temp = array('path' => ltrim($request->getUri()->getPath(), '/')) + $arguments;
            if(isset($this->params['prefix'])) {
                $prefix = is_array($this->params['prefix']) ? $this->params['prefix'][$index] : $this->params['prefix'];
                $suffix = substr($uri->getPath(), strlen($prefix));
                $temp['suffix'] = $suffix;
            }
            $target = self::rewritePath($target, $temp);
            $request->getUri()->setPath($target);
            if($target[0] == '@') {
                $controller = substr($target, 1);
            }
            if(SU_LOG) \suLog::d(' Target "%s" (args=%s, defaults=%s, targets=%s, request=%s, controller=%s)', $target, $arguments, $this->defaultArguments, $this->targets, $request, $controller);
        }
        
        // TODO: Have to reuse parent router so it can be used to build a new uri...
        if($this->import) {
            if(SU_LOG) \suLog::d(' Importing "%s"', $this->import);
            $router = new Router;
            $router->load($this->import);
            if($result = $router->match($request, $this)) {
                if(SU_LOG) \suLog::d(' Matching route found from import');
                return $result;
            }
            // TODO: FAil here?
            if(SU_LOG) \suLog::d(' Import did not match');
            #return false;
        }
        
        if(SU_LOG) \suLog::d('Matching route found');

        $uri = $request->getUri();
        parse_str($uri->getPart('query'), $get);
        $get = array_merge($arguments, $get); // TODO: Rewrite args should overwrite $_GET or not?
                
        $result = new Request($uri, array(
            'routeParams' => $this->params,
            'target' => $target,
            'controller' => $controller,
            'post' => $_POST,
            'get' => $get,
        ));
        
        return $result;
    }
    
    /**
     * @todo Cleanup...
     * @todo Match other URI parts (host)
     * @todo Temp locale override
     */
    public function build($name, array $parameters = array(), $parent = null) {
        if(strcmp($name, $this->name) !== 0) {
            return false;
        }
        if($this->locale && !in_array(\Locale::getDefault(), $this->locale)) {
            return false;
        }
        if(empty($this->formats['path'])) {
            # FIXME: This could be slow, even if routes are cached!
            if($this->import) {
                try {
                    if(SU_LOG) \suLog::d(' Importing "%s"', $this->import);
                    $router = new Router;
                    $router->load($this->import);
                    if($result = $router->build($name, $parameters, $this)) {
                        if(SU_LOG) \suLog::d(' Matching output route found from import "%s"', $result);
                        return $result;
                    }
                    // TODO: FAil here?
                    if(SU_LOG) \suLog::d(' No match in imported file');
                    return false;
                } catch(NotFoundException $ex) {
                    if(SU_LOG) \suLog::d(' File not found');
                }
            }
            if(SU_LOG) \suLog::d('Route "%s" has no path format!', $this->name);
            return false;
        }
        $defaults = $this->defaultArguments;
        $parameters += $defaults;
        foreach($this->formats['path'] as $format) {
            $missing = array_diff($format['keys'], array_keys($parameters));
            #\suLog::d("%s is missing (%s) (required %s)", $format['format'], implode(', ', $missing), implode(', ', $format['keys']));
            if(count($missing) === 0) {
                if(SU_LOG) \suLog::d('Route "%s", found matching output route "%s", parent=%s', $this->name, $format['format'], $parent ? $parent->name : 'null');
                break;
            }
        }
        if(count($missing) !== 0) {
            if(SU_LOG) \suLog::d('Route "%s" requires more parameters (%s)', $this->name, implode(', ', $missing));
            return false;
        }
        // Remove trailing defaults..
        foreach(array_reverse($defaults) as $k => $v) {
            if(isset($parameters[$k]) && $parameters[$k] === $v) {
                if(SU_LOG) \suLog::d('UNSET %s', $k);
                unset($parameters[$k]);
                unset($defaults[$k]);
            } else {
                break;
            }
        }
        // Replace prefix with parent prefix
        if($parent && isset($parent->params['prefix'])) {
            // Search each parent target for a matching output format, and replace it with the parent prefix
            $parentPrefix = is_array($parent->params['prefix']) ? $parent->params['prefix'] : array($parent->params['prefix']);
            for($i = 0; $i < count($parent->targets); ++$i) {
                $parentTarget = self::rewritePath($parent->targets[$i], array('suffix' => ''));
                if(strpos($format['format'], $parentTarget) === 0) {
                    $new = $parentPrefix[$i].ltrim(substr($format['format'], strlen($parentTarget)), '/');
                    if(SU_LOG) \suLog::d('Fixing prefix %s => %s', $format['format'], $new);
                    $format['format'] = $new;
                    break;
                }
            }
        }
        // Build path from format
        $callback = function($input) use(&$parameters, &$defaults) {
            $name = substr($input[1], 1);
            if(isset($parameters[$name])) {
                $result = htmlspecialchars($parameters[$name]); // TODO: ???
                unset($parameters[$name]);
                unset($defaults[$name]);
                return $result;
            } else {
                return ''; // Was removed as default..
            }
        };
        $path = preg_replace_callback(self::$elementRegexp, $callback, $format['format']);
        $path = rtrim($path, '/'); // FIXME?
        
        // Build query
        $parameters = array_diff($parameters, $defaults);
        if(count($parameters)) {
            $query = http_build_query($parameters, '', '&');
        } else {
            $query = false;
        }
        
        // Format it
        $result = new Uri();
        $result->setAppHost();
        $result->setPart('path', $path);
        // FIXME: Temp host hack..., see function @todo
        if(isset($this->formats['host'])) {
            $result->setPart('host', str_replace(':host', $result->getHost(), $this->formats['host'][0]['format']));
        }
        $result->setPart('query', $query);
        $result = (string)$result;
        if(SU_LOG) \suLog::d('Route "%s" => "%s"', $this->name, $result);
        return $result;
    }
    
    public function getParam($key, $default = null) {
        if(isset($this->params[$key])) {
            return $this->params[$key];
        }
        return $default;
    }
    
    protected function initialize() {
        $this->patterns = array();
        $this->formats = array();
        
        $parts = array(
            'host:' => 'host',
            'path:' => 'path',
            '' => 'path',
        );
        
        $types = array(
            'format' => 'format',
            'prefix' => 'prefix',
            'pattern' => 'regexp',
            'regexp' => 'regexp',
        );
        
        foreach($types as $typeKey => $type) {
            foreach($parts as $partKey => $part) {
                $key = $partKey.$typeKey;
                if(isset($this->params[$key])) {
                    if(!isset($this->patterns[$part])) $this->patterns[$part] = array();
                    if(!isset($this->formats[$part])) $this->formats[$part] = array();
                    switch($type) {
                    case 'format':
                        $this->patterns[$part] += self::formatToRegexArray($this->params[$key], false, $this->params, $this->formats[$part]);
                        break;
                    case 'prefix':
                        $dummy = array(); // FIXME! (Do not use 'prefix' for uri building)
                        $this->patterns[$part] += self::formatToRegexArray($this->params[$key], true, $this->params, /*$this->formats[$part]*/ $dummy);
                        break;
                    case 'regexp':
                        $this->patterns[$part] += self::patternToRegexArray($this->params[$key]);
                        break;
                    default:
                        CoreException::raise('fixme');
                    }
                }
            }
        }
        
        if(empty($this->patterns)) {
            throw new \Exception(sprintf('Route "%s" has no patterns', $name));
        }
        
        // TODO: Support multiple pattern/target for absolute routes?
        if(isset($this->params['target'])) {
            $param = $this->params['target'];
            if(is_array($param)) {
                // Format array(0 => target, 1 => target2, defaultArgs...)
                $index = 0;
                $this->targets = array();
                $this->defaultArguments = $param;
                
                while(isset($this->defaultArguments[0])) {
                    $target = array_shift($this->defaultArguments);
                    if($target === true) {
                        // Special case: if target is TRUE, use the prefix/pattern as target
                        //$target = $this->patterns['path'][$index];
                        // FIXME: Broken $this->patterns is a regexp here!
                        CoreException::raise('fixme');
                    }
                    $this->targets[] = $target;
                    $index++;
                }
            } else {
                $this->targets = array($param);
                $this->defaultArguments = array();
            }
        }
        
        if(isset($this->params['import'])) {
            $this->import = $this->params['import'];
        }
        
        if(isset($this->params['locale'])) {
            $this->locale = is_array($this->params['locale']) ? $this->params['locale'] : array($this->params['locale']);
        }
    }
    
    static protected function matchPattern($pattern, $subject, array &$arguments) {
        try {
            if(preg_match($pattern, $subject, $matches)) {
                // TODO: Arguments!!!
                if(SU_LOG) \suLog::d('Match %s', $matches);
                foreach($matches as $k => $v) {
                    if(!is_numeric($k) && strlen($v) > 0) {
                        $arguments[$k] = $v;
                    }
                }
                return true;
            }
        } catch(\Exception $ex) {
            if(SU_LOG) \suLog::d('Invalid regex: "%s" %s', $pattern, $ex->getMessage());
        }
        return false;
    }
    
    static protected function rewritePath($path, $args) {
        $result = preg_replace_callback('/\$(\w+)/m', function($input) use($args) {
            $key = strtolower($input[1]);
            $result = isset($args[$key]) ? $args[$key] : 'null';
            if(ucfirst($key) === $input[1]) $result = ucfirst($result);
            return $result;
        }, $path);
        if(SU_LOG) \suLog::d('Rewrote "%s" => "%s" (args=%s)', $path, $result, $args);
        return $result;
    }
    
    static protected function formatToRegexArray($format, $prefix, $params, &$formats) {
        if(!is_array($format)) $format = array($format);
        $result = array();
        foreach($format as $f) {
            $result[] = self::formatToRegex($f, $prefix, $params, $formats);
        }
        return $result;
    }
    
    static protected function patternToRegexArray($pattern) {
        if(!is_array($pattern)) $pattern = array($pattern);
        $result = array();
        foreach($pattern as $p) {
            $result[] = self::lazyRegex($p);
        }
        return $result;
    }
    
    // FIXME: This is a mess...
    static protected function formatToRegex($format, $prefix, $params, &$formats) {
        $names = array();
        $result = preg_quote($format, '/');
        $callback = function($input) use($params, &$names) {
            // FIXME: Callback escapes first char?????
            $name = substr($input[1], 1);
            if(isset($params['rules'][$name])) {
                $rule = $params['rules'][$name];
            } else {
                $rule = '[^/]+|';
            }
            if(SU_LOG) \suLog::d('Using rule "%s" for "%s"', $rule, $name);
            $result = sprintf('(?<%s>%s)', $name, $rule);
            $result = addslashes($result);
            $names[] = $name;
            return $result;
        };
        $result = preg_replace_callback(self::$elementRegexp, $callback, $result);
        $result = stripslashes($result); // FIXME: callback adds / to result..
        $result = addcslashes($result, '/');
        #$result = str_replace('\\'.PHP_EOL, '', $result); // FIXME: See $callback
        $result = '/^'.$result;
        if($prefix) {
            $result .= '(?<suffix>[^$]+|)'; // TODO: Use this or not?
            #$result .= '([\/]+|)';
            $result .= '/';
        } else {
            $result .= '([\/]+|)';
            $result .= '$/';
        }
        $formats[] = array(
            'keys' => $names,
            'format' => $format,
        );
        if(SU_LOG) \suLog::d('Converted "%s" to "%s"', $format, $result);
        return $result;
    }

    static protected function lazyRegex($pattern, $suffix = '') {
        if($pattern[0] == '/') {
            return $pattern;
        }
        $result = '/'.addcslashes($pattern, '/').'/'.$suffix;
        if(SU_LOG) \suLog::d('Converted "%s" to "%s"', $pattern, $result);
        return $result;
    }
}
