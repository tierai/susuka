<?php

namespace susuka\config;

use \susuka\core\Utils as Utils;
use \susuka\exception\Argument as ArgumentException;

/**
 * PHP Configuration
 * 
 * @todo Cache/serialization
 */
class Php extends Config {    
    protected $cache;
    
    public function __construct($values = array()) {
        parent::__construct($values);
        $this->cache = \susuka\core\Registry::get('cache');
    }

    public function load($config) {
        if(is_string($config)) {
            if(!file_exists($config)) {
                ArgumentException::raise('File "%s" does not exist!', $config);
            }
            if(!$this->cache || !$this->cache->value(__CLASS__, $config, $this->values)) {
                require($config);
                if($file = Utils::getUserFile($config, true)) {
                    require($file);
                }
                if($this->cache) {
                    $this->cache->set(__CLASS__, $config, $this->values);
                }
            }
        } else {
            parent::load($config);
        }
    }
    
    public function save($filename) {
        $data = '<?php '.PHP_EOL;
        $data .= '$config = ';
        $data .= $this->toPhpConfig();
        if(FALSE === @file_put_contents($filename, $data, LOCK_EX)) {
            ArgumentException::raise('Could not write config to "%s"!', $filename);
        }
    }
    
    protected function toPhpConfig() {
        $result = '';
        foreach($this->values as $key => $value) {
            $result .= '$this->set(\''.addslashes($key).'\', ';
            $result .= self::toPhpValue($value);
            $result .= ', true);'.PHP_EOL;
        }
        return $result;
    }
    
    protected function fromPhpConfig($php) {
        eval($php);
    }

    static protected function toPhpValue($value, $level = 0) {
        if(is_array($value)) {
            $result = 'array('.PHP_EOL;
            foreach($value as $k => $v) {
                $result .= str_repeat('    ', $level + 1);
                if(is_numeric($k)) {
                    $result .= $k.' => ';
                } else {
                    $result .= '\''.addslashes($k).'\' => ';
                }
                $result .= self::toPhpValue($v, $level + 1);
                $result .= ','.PHP_EOL;
            }
            $result .= str_repeat('    ', $level).')';
        } else {
            $result = '\''.(string)$value.'\'';
        }
        return $result;
    }
}

