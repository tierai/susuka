<?php

namespace susuka\config;

use \susuka\exception\InvalidArgument as InvalidArgumentException;
use \susuka\core\Utils;

/**
 * Config
 * 
 * @todo Importing & loading of other Configurations..
 */
class Config {
    protected $values;
    
    public function __construct($values = array()) {
        $this->values = $values;
    }
    
    public function get($key, $default = NULL, $throw = false) {
        if(!isset($this->values[$key])) {
            if($throw) {
                InvalidArgumentException::raise('The key "%s" does not exist!', $key);
            }
            return $default;
        }
        return $this->values[$key];
    }
    
    public function find($base, array $keys, $default = false) {
        $root = $this->get($base);
        foreach($keys as $key) {
            if(!isset($root[$key])) {
                return $default;
            }
            $root = $root[$key];
        }
        return $root;
    }
    
    public function value($key, &$value) {
        if(isset($this->values[$key])) {
            $value = $this->values[$key];
            return true;
        }
        return false;
    }

    public function set($key, $value, $merge = false, $overwrite = false) {
        if($merge && is_array($value) && isset($this->values[$key]) && is_array($this->values[$key])) {
            $this->values[$key] = $overwrite ? Utils::arrayMerge($this->values[$key], $value) : Utils::arrayMerge($value, $this->values[$key]);
        } else {
            $this->values[$key] = $value;
        }
    }
    
    public function has($key) {
        return isset($this->values[$key]);
    }

    public function clear() {
        $this->values = array();
    }
    
    public function values() {
        return $this->values;
    }
    
    public function load($config) {
        if($config instanceof Config) {
            $this->values = $config->values();
        } else if(is_array($config)) {
            $this->values = $config;
        } else {
            InvalidArgumentException::raise('Unsupported config type "%s"', gettype($config));
        }
    }
    
    public function merge($config, $overwrite = false) {
        if($config instanceof Config) {
            $this->values = $overwrite ? Utils::arrayMerge($this->values, $config->values()) : Utils::arrayMerge($config->values(), $this->values);
        } else if(is_array($config)) {
            $this->values = $overwrite ? Utils::arrayMerge($this->values, $config) : Utils::arrayMerge($config, $this->values);
        } else {
            InvalidArgumentException::raise('Unsupported config type "%s"', gettype($config));
        }
    }

}
