<?php

namespace susuka\module\admin\controller;

class Index extends Base {
    
    function indexAction() {
        return array(
            '$title' => 'Administration',
        );
    }
}
