<?php

namespace susuka\module\admin\controller;

class Config extends Base {
    
    function indexAction() {
        $config = $this->getApplication()->getConfig();
        return array(
            '$title' => 'Configuration',
            'config' => $config,
        );
    }
}
