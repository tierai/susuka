<?php

namespace susuka\module\admin\controller;

class Base extends \susuka\controller\View {
    
    function __construct(array $config) {
        if(SU_APP_ENVIRONMENT === SU_APP_ENVIRONMENT_PRODUCTION) {
            SecurityException::raise();
        }
        parent::__construct($config);
    }
    
    protected function initView($view) {
        if($view instanceof \susuka\view\Html) {
            $view->set('content-class', 'su-admin');
            $view->addStyle('@admin/asset/css/main.css');
        }
        parent::initView($view);
    }
}
