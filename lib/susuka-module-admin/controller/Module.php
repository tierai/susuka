<?php

namespace susuka\module\admin\controller;

class Module extends Base {
    
    function indexAction() {
        $config = $this->getApplication()->getConfig();
        $modules = $config->get('app.module', array());
        $adminMenu = array();
        foreach($modules as $m) {
            $title = isset($m['path']) ? $m['path'] : '???';
            $adminMenu[] = array($title, '#');
            $adminMenu[] = array('aaaa', '#');
            $adminMenu[] = array('aaaa', '#');
            $adminMenu[] = array('aaaa', '#');
        }
        return array(
            '$title' => 'Modules',
            'modules' => $modules,
            'adminMenu' => $adminMenu,
        );
    }
}
