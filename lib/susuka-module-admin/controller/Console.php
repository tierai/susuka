<?php

namespace susuka\module\admin\controller;

use \susuka\exception\Security as SecurityException;

class Console extends \susuka\controller\Controller {
    
    public function __construct() {
        if(SU_APP_ENVIRONMENT === SU_APP_ENVIRONMENT_PRODUCTION) {
            SecurityException::raise();
        }
    }
    
    public function indexAction() {
        return array('title' => 'Console', 'content' => 'Nothing to see here...');
    }
    
    public function buildAction() {
        $files = \susuka\asset\Builder::getInstance()->buildAll();
        return array(
            'files' => $files,
        );
    }
    
    /**
     * @todo console/log/$logfile
     */
    public function logAction() {
        $this->setViewFormat('text/plain');
        if(defined('SU_LOGFILE') && is_file(SU_LOGFILE)) {
            return array(file_get_contents(SU_LOGFILE));
        } else {
            return array('Logging disabled');
        }
    }
}
