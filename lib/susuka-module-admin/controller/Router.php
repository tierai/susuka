<?php

namespace susuka\module\admin\controller;

class Router extends Base {
    
    function indexAction() {
        $router = $this->getApplication()->getRouter();
        return array(
            '$title' => 'Router',
            'router' => $router,
        );
    }
}
