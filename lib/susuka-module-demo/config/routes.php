<?php

$this->route('_demo', array(
    'format' => array('/_demo/:action', '/_demo/:action/:message'),
    'target' => array('@demo/controller/Index', 'action' => 'index', 'message' => false),
));
