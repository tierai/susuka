<?php

namespace susuka\module\demo\controller;

/**
 * Index
 */
class Index extends \susuka\controller\View {

    function indexAction() {
        return array(
            'title' => 'DemoModule/Welcome!',
            'message' => 'Hello from '.__METHOD__,
        );
    }
    
    ## TODO: Fix this?
    /*function testAction() {
        $this->view->export('header', 'Derpity derp');
        $this->view->export('header-extra', 'view/header-extra.phtml', true);
        return array('message' => 'hmmm...');
    }*/
    
    function route1Action($message) {
        return array('title' => 'DemoModule/RouteOne', 'message' => $message);
    }
    
    function route2Action() {
        return array('title' => 'DemoModule/RouteTwo', 'message' => 'Hello from '.__METHOD__);
    }
}
