<?php

namespace susuka\module\wiki\model;

use \susuka\data\model\Dynamic as Model;

class Page extends Model {

    /**
     * Mapper creation callback, useful for adding metadata...
     * @todo Document this...
     */
    static function onMapperCreated($mapper) {
        $meta = array('stuff');
        // TODO: Set index and column types
        #$this->bean->setMeta('buildcommand.unique', array(array('name', 'current')));
        #$this->bean->setMeta('buildcommand.indexes', array('current' => 'idx_current'));
        $mapper->getMeta()->add($meta);
    }
}
