<?php

$this->route('_wiki', array(
    'format' => '/wiki/:page',
    'target' => array('@wiki/controller/Index', 'action' => 'view', 'page' => 'index'),
    'rules' => array(
        'page' => '[^$]+|',
    ),
));

$this->route('_wiki', array(
    'format' => '/_wiki/:instance/:action/:page',
    'target' => array('@wiki/controller/Index', 'action' => 'view', 'page' => 'index'),
    'rules' => array(
        'page' => '[^$]+|',
    ),
));
