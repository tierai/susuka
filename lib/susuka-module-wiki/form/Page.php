<?php

namespace susuka\module\wiki\form;

class Page extends \susuka\helper\form\Form {
    public $title;
    public $status;
    public $format;
    public $content;
    public $preview;
    public $reset;
    public $save;
    
    function __construct($request = null, $defaults, array $formats = array()) {
        parent::__construct($request);
        $this->defaults($defaults);
        $this->title = $this->text('title')->label('Title')->title('Page title')->rule(array(
            'length' => array(1, 255),
        ));
        $this->status = $this->select('status', array(0 => 'Draft', 1 => 'Published'))->label('Status')->title('Page status');
        $this->format = $this->select('format', $formats)->label('Format')->title('Content format')->visible(!empty($formats))->rule(function($self) use($formats) {
            return isset($formats[$self->formValue()]);
        });
        $this->content = $this->textarea('content')->title('Type your content here')->rule(array('length' => array(1)));
        $this->preview = $this->submit('preview', 'Preview');
        $this->reset = $this->reset('reset', 'Reset');
        $this->save = $this->submit('submit', 'Save');
        $this->group('buttons', array($this->preview, $this->reset, $this->save));
        $this->csrf(true);
    }
}
