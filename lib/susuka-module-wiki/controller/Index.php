<?php

namespace susuka\module\wiki\controller;

use \susuka\core\Registry;

use \susuka\data\adapter\Pdo as PdoAdapter;
use \susuka\data\mapper\Dynamic as DynamicMapper;
use \susuka\data\model\Dynamic as DynamicModel;

use \susuka\module\wiki\model\Page as Page;
use \susuka\module\wiki\form\Page as PageForm;


class Index extends \susuka\controller\View {
    protected $cache;
    protected $formats = array('html' => 'HTML', 'markdown' => 'Markdown', 'bbcode' => 'BBCode'); ///< TODO: Get from wiki.config and user permissions
    
    public function __construct(array $config) {
        parent::__construct($config);
        $this->cache = Registry::get('cache');
        $this->mapper = Page::getMapper($this);
    }
    
    public function viewAction($page, $id = null) {
        if(!$this->cache || !$this->cache->value(__CLASS__, $page, $data)) {
            $query = $id ? array('name' => $page, 'id' => $id) : array('name' => $page, 'status' => 1);
            $data = $this->mapper->load($query, function() use($page) {
                return array('name' => $page, 'title' => $page);
            });
            $data->set('html', $this->renderPage($data));
            if($this->cache) $this->cache->set(__CLASS__, $page, $data);
        }
        
        // TODO: check user permissions
        if("canedit") {
            // TODO: Not sure how we want to display the editor menu...
            $this->getApplication()->getConfig()->set('app.menu-footer', array(
                array(
                    'title' => 'Edit',
                    'route' => array('_wiki', 'page' => $page, 'action' => 'edit', 'id' => $data->getId()),
                ),
                array(
                    'title' => 'History',
                    'route' => array('_wiki', 'page' => $page, 'action' => 'history', 'id' => $data->getId()),
                ),
            ), true);
        }
        
        return array(
            'title' => $data->get('title'),
            'data' => $data,
        );
    }
    
    /**
     * @todo Find conflicting drafts..
     * @todo Only save if modified
     */
    public function editAction($page, $id = null) {
        $data = $this->mapper->find(array('name' => $page, 'id' => $id), array(0, 1), array('id', 'DESC'))->fetch();
        if(!$data) $data = $this->mapper->create(array('name' => $page, 'title' => $page));
        
        $form = new PageForm($this->request, $data, $this->formats);
        
        if($form->save->submitted() && $form->validate()) {
                // TODO: auto-filter buttons and crap?
                $values = $form->values(array('title', 'content', 'status', 'format'));
                $publish = $values['status'] != 0;
                $oldId = $data->getId();
                if($data->get('status') != 0) {
                    $data = $this->mapper->copy($data);
                }
                $data->setValues($values, true);
                $data->set('updated', new \DateTime());
                $this->mapper->save($data);
                if($publish) {
                    $this->mapper->exec('UPDATE # SET status=0 WHERE name=? AND id!=?', array($page, $data->getId()));
                }
                if($this->cache) $this->cache->delete(__CLASS__, $page);
        } else {
            $data->setValues($form->userValues(), true);
        }
        
        return array(
            '$title' => array('Editing %1$s (#%2$d)', $data->get('title'), $data->get('id')),
            'form' => $form,
            'data' => $data,
            'html' => $this->renderPage($data),
        );
    }
    
    public function historyAction($page) {
        $result = $this->mapper->find(array('name' => $page), 100, array('id', 'DESC'));
        return array(
            '$title' => array('History for %1$s', $page),
            'page' => $page,
            'result' => $result,
        );
    }
    
    public function deleteAction($page) {
        NotImplemented::raise();
        // ....
        $this->cache->delete($this, $page);
    }

    /**
     * @todo Cache HTML
     */    
    protected function renderPage($data) {
        $options = array();
        switch($data->get('format')) {
        case 'html':
            $markup = new \susuka\markup\SafeHTML;
            $options = array('return_element' => 'body');
            break;
        case 'markdown':
            $markup = new \susuka\markup\Markdown;
            $options = array('no_markup' => false);
            break;
        default:
            $markup = new \susuka\markup\Simple2;
            break;
        }
        return $markup->render($data->get('content'), $options);
    }
    
    /**
     * Remove trailing / from page
     * @todo Should this be possible to do in Route?
     */
    protected function action($action, array $args) {
        if(isset($args['page'])) {
            $args['page'] = rtrim($args['page'], '/');
        }
        return parent::action($action, $args);
    }
    
    protected function initView($view) {
        if($view instanceof \susuka\view\Html) {
            $view->set('content-class', 'su-wiki');
            $view->addStyle('@wiki/asset/css/wiki.css');
        }
        parent::initView($view);
    }
}
