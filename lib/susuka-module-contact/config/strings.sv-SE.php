<?php

$this->add('sv-SE', array(
    'Contact us' => 'Kontakta oss',
    'Enter your email address here' => 'Skriv din epost-adress här',
    'This is not a valid email address' => 'Det här är inte en korrent epost-adress',
    'Type your message here' => 'Skriv ditt meddelande här',
    'Send message' => 'Skicka meddelande',
));
