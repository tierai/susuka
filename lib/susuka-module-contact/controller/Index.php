<?php

namespace susuka\module\contact\controller;

class Index extends \susuka\controller\View {
    
    function indexAction() {
        $form = new \susuka\module\contact\form\Contact;
        if($form->submit->submitted() && $form->validate()) {
            $config = $this->getAppConfig();
            $email = $config->find('app.module', array('contact', 'email'));
            if(empty($email)) \susuka\exception\Core::raise('No contact email configured');
            $subject = $config->find('app.module', array('contact', 'subject'), 'Website message from: '.$email);
            $message = htmlspecialchars($form->message->val());
            $message .= $this->footer();
            if($this->mail($email, $subject, $message)) {
                $form->info('Your message was sent, we will reply to you shortly');
                $form->clear();
            } else {
                $form->error('Could not send mail, please try again or contact the site administrator');
            }
        }
        return array(
            '$title' => 'Contact us',
            'form' => $form
        );
    }
    
    protected function mail($to, $subject, $message) {
        return mail($to, $subject, $message);
    }
    
    protected function footer() {
        return PHP_EOL.PHP_EOL.sprintf('--------'.PHP_EOL.'sent from: %s - %s', $_SERVER['REMOTE_ADDR'], gmdate('c'));
    }
}
