<?php

namespace susuka\module\contact\form;

class Contact extends \susuka\helper\form\Form  {
    public $submit;
    public $email;
    public $message;
    
    function __construct($request = null) {
        parent::__construct($request);
        $this->email = $this->text('email')->label('Your email address')->title('Enter your email address here')->rule(array('length' => array(1)))->rule(function($e) {
            if(!filter_var($e->val(), FILTER_VALIDATE_EMAIL)) {
                $e->error('This is not a valid email address');
            }
            return true;
        });
        $this->message = $this->textarea('message')->title('Type your message here')->rule(array('length' => array(10)));
        $this->submit = $this->submit('submit', 'Send message');
        $this->group('buttons', array($this->submit));
        $this->csrf(true);
    }
}
