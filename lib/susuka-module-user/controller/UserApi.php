<?php

namespace susuka\module\user\controller;

/**
 * 
 * 
 * @todo Client auth/verification? (in a RESTful way..)
 * @todo For ajax forms: How do the User form generate a CSRF token that this class can use?
 */
class UserApi extends \susuka\controller\Api {
    
    public function __construct($config = array()) {
        parent::__construct($config);
        #$this->setCsrfCheck(true, __NAMESPACE__, array('post', 'put', 'delete'));
        # and same in User.php
    }
    
    public function getAction($id = null) {
        return array('fixme' => __METHOD__, 'id' => $id);
    }
    
    public function postAction($data) {
        return array('fixme' => __METHOD__, 'data' => $data);
    }
    
    public function putAction($data) {
        return array('fixme' => __METHOD__, 'data' => $data);
    }
    
    public function deleteAction($id) {
        return array('fixme' => __METHOD__, 'id' => $id);
    }
    
    protected function action($action, array $args) {
        if(empty($args['csrf_key']) || $this->getSession()->get(array(__CLASS__, 'csrf_key')) !== $args['csrf_key']) {
            $code = 403;
            $this->response->code($code);
            return array(
                'message' => $this->response->_getHttpMessage($code),
            );
        }
        return parent::action($action, $args);
    }
    
    // in controller? setCsrfCheck(false|session|cookie|callback, action(s)|WRITE, method?) createCsrfKey checkCsrfKey
    // global? per-controller key? per action?
}
