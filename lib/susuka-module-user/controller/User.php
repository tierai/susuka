<?php

namespace susuka\module\user\controller;

use \susuka\module\user\model\User as UserModel;

/**
 * @todo CRUD & REST
 */
class User extends \susuka\controller\View {
    protected $userMapper;
    
    public function __construct(array $config) {
        parent::__construct($config);
        $this->userMapper = UserModel::getMapper($this);
    }
    
    public function viewAction($id) {
        $user = $this->userMapper->load($id);
        return array(
            '$title' => array('View user %1$s', $user ? $user->name() : 'NULL'),
            'user' => $user
        );
    }
    
    /**
     * @note This action should only be accessible by the User, if the ACL is working.
     */
    public function editAction($id) {
        $user = $this->userMapper->load($id);
        return array(
            '$title' => array('Edit user %1$s (#%2$d)', $user ? $user->name() : 'NULL', $id),
            'user' => $user
        );
    }
    
    /**
     * @todo ajax:
     *      extend RestCtrlr or new class
     *      Session key
     * @todo
     *  modes:
     *      disabled
     *      admin_activation
     *      email_validation
     *  quick-signup:
     *      only enter email address, rest of the registration is done from a link sent to that email
     *  captcha...
     */
    public function registerAction() {
        $form = new \susuka\module\user\form\Register($this->userMapper, $this->request);
        if($form->submit->submitted() && $form->validate()) {
            $user = $this->userMapper->create($form->values(array('name', 'email')));
            $user->password($form->pass->val());
            $mapper->save($user);
            return $this->redirect('_auth');
        }
        
        return array(
            '$title' => 'Register',
            'form' => $form,
        );
    }
    
    public function recoverAction() {
        \susuka\exception\NotImplemented::raise();
    }
    
    public function listAction() {
        $list = $this->userMapper->all();
        # TODO: Wrap result in a Feed, so HTML is properly escaped
        return array(
            '$title' => 'List users',
            'list' => $list
        );
    }
}
