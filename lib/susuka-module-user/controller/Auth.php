<?php

namespace susuka\module\user\controller;

use \susuka\l10n;

use \susuka\core\Registry as Registry;
use \susuka\exception\Security as SecurityException;
use \susuka\module\user\model\User as User;
use \susuka\module\user\form\Login as LoginForm;

/**
 * @todo Split actions to separate controllers?
 * @todo User FormBuilder:
 *       su\html\form\Form:
 *       :: how do we create it? (config, defines, should be same as Markup..)
 */
class Auth extends \susuka\controller\View {
    
    public function loginAction() {
        $auth = $this->getApplication()->getAuth();
        if($auth->hasIdentity()) {
            return $this->redirectUser();
        }
        $form = new LoginForm($this->request);
        if($form->login->submitted() && $form->validate()) {
            try {
                $adapter = $this->createAdapter($form->adapter->val());
                $auth->authenticate($adapter, array(
                    'username' => $form->user->val(),
                    'password' => $form->pass->val(),
                ));
                if(!$auth->hasIdentity()) {
                    \susuka\exception\Core::raise('Auth adapter %s should throw a SecurityException if no identity was set!', get_class($auth));
                }
                return $this->redirectUser();
            } catch(SecurityException $ex) {
                switch($ex->getCode()) {
                case \susuka\auth\Auth::CODE_IDENTITY_NOT_FOUND:
                    $form->user->error('This identity does not exist');
                    break;
                case \susuka\auth\Auth::CODE_INVALID_PASSWORD:
                    $form->pass->error('Invalid password');
                    break;
                case \susuka\auth\Auth::CODE_UNKNOWN:
                default:
                    $form->adapter->error('Unknown authentication error (%1$d)', $ex->getCode());
                }
            }
        }
        
        return array(
            '$title' => 'Login',
            'form' => $form,
        );
    }
    
    public function logoutAction() {
        $auth = $this->getApplication()->getAuth();
        $auth->clearIdentity();
        return $this->redirectUser(array('message' => 'Logged out'));
    }
    
    protected function createAdapter($name) {
        $adapters = $this->getAppConfig()->find('app.module', array('user', 'auth', 'adapters'));
        if(!in_array($name, $adapters)) {
            SecurityException::raise(1000, 'Unsupported adapter');
        }
        switch($name) {
        case 'openid':
            return new \susuka\auth\adapter\OpenId;
        case 'local':
            return new \susuka\module\user\auth\adapter\Local(User::getMapper($this));
        default:
            SecurityException::raise(1001, 'Unsupported adapter');
        }
    }
    
    /**
     * @todo Redirect to previous location if possible
     */
    protected function redirectUser(array $args = array()) {
        $this->redirect('_index');
    }
}

