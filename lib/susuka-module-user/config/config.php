<?php

$this->set('dependencies', array(
#    'susuka',
));

$this->set('auth', array(
    'adapters' => array('local', 'openid'),
    'crypt_salt' => '$6$rounds=5000$You should override this salt in your app configuration$',
));
