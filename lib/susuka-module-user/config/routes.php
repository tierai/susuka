<?php

$this->route('_user_api', array(
    'format' => '/_user/api/:id',
    'target' => array('@user/controller/UserApi', 'id' => null),
));

$this->route('_user', array(
    'format' => array('/_user/:action/:id', '/_user/:action'),
    'target' => array('@user/controller/User', 'action' => 'view', 'id' => 0),
));

$this->route('_auth', array(
    'format' => '/_auth/:action',
    'target' => array('@user/controller/Auth', 'action' => 'login'),
));
