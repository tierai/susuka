<?php

namespace susuka\module\user\auth\adapter;

use \susuka\auth\adapter\Adapter as AdapterInterface;
use \susuka\module\user\model\User as User;
use \susuka\core\Registry as Registry;
use \susuka\exception\Security as SecurityException;

class Local implements AdapterInterface {
    protected $mapper;
    
    public function __construct($mapper) {
        $this->mapper = $mapper;
    }
    
    public function authenticate(array $args) {
        $name = $args['username'];
        if($user = $this->mapper->load(array('name' => $name))) {
            if($user->checkPassword($args['password'])) {
                return $user;
            }
            if(SU_LOG) \suLog::d('User "%s" login fail: Wrong password', $name);
            SecurityException::raise(\susuka\auth\Auth::CODE_INVALID_PASSWORD);
        }
        if(SU_LOG) \suLog::d('User "%s" login fail: User not found', $name);
        SecurityException::raise(\susuka\auth\Auth::CODE_IDENTITY_NOT_FOUND);
    }
}
