<?php

namespace susuka\module\user\model;

use \susuka\data\model\Dynamic as Model;

class Group extends Model {
    public function __toString() {
        return $this->get('name');
    }
}
