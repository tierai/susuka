<?php

namespace susuka\module\user\model;

use \susuka\data\model\Dynamic as Model;

class User extends Model {
    
    function id($value = null) {
        return $this->getOrSet(__FUNCTION__, $value, func_num_args() == 1);
    }
    
    function name($value = null) {
        return $this->getOrSet(__FUNCTION__, $value, func_num_args() == 1);
    }
    
    function pwhash($value = null) {
        return $this->getOrSet(__FUNCTION__, $value, func_num_args() == 1);
    }
    
    function password($value) {
        return $this->pwhash($this->hashPassword($value));
    }
    
    function checkPassword($password) {
        return strcmp($this->hashPassword($password), $this->pwhash()) === 0;
    }
    
    function hashPassword($password) {
        $config = \susuka\core\Registry::get('config');
        $salt = $config->find('app.module', array('user', 'auth', 'crypt_salt'));
        $nonce = $this->get('pwnonce');
        if(empty($nonce)) {
            $nonce = \susuka\core\Random::string(32);
            $this->set('pwnonce', $nonce);
        }
        return crypt($password.$nonce, $salt);
    }
    
    function __toString() {
        return $this->get('name');
    }
}
