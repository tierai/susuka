/**
 * JavaScript enhancements for the user module.
 * 
 * @note View code should NOT rely on this script beeing loaded.
 * This is not a required script and has to be enabled in the application.
 * 
 * @todo Use Ajax & UserAPI for registration, login and other forms 
 */
 
// Override registration submit
if(su.ajax.submit) {
    $('#susuka_module_user_form_Login').live('submit', function(){
        // TODO: Post..
    });
    
    $('#susuka_module_user_form_Register').live('submit', function(){
        // TODO: Post..
    });
    
    // TODO: Logout button?
}

// Verify form fields
if(su.ajax.fields) {
    $('#susuka_module_user_form_Register_name, #susuka_module_user_form_Register_email').live('change', function(){
        // TODO: Check (after a delay, or check on leave)
    });
}
