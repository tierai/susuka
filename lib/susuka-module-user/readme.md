Basic user & group management


TODO: ACL, plugins

We should be able to hook controller pre-dispatch function and do something like:

    OnControllerPreDispatch($request) {
        $acl = Registry::get('acl');
        $identity = Registry::get('auth')->getIdentity();
        if(!$acl->isAllowed($request->controller, $identity->identity)) {
            throw OMFG(...);
        }
    }
    
And also in the controller:

    ForumController::OnPreDispatch(...) {
        if($forum) {
            # Something like: ....
            $acl->allow($forum->roles)
        }
    }
    
    ForumController::editPostAction() {
        $acl->checkAgainstPostOwner(...)
    }
