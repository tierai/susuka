<?php

namespace susuka\module\user\form;

class Login extends \susuka\helper\form\Form {
    public $user;
    public $pass;
    public $adapter;
    public $login;
    
    function __construct($request = null) {
        parent::__construct($request);
        //$this->message = $this->text('message')->visibility('auto');
        $this->user = $this->text('user')->label('Username')->title('Enter your username here')->rule(array('length' => array(1)));
        $this->pass = $this->password('pass')->label('Password')->title('Enter your password here');
        $this->adapter = $this->hidden('adapter', 'local');
        $this->login = $this->submit('login', 'Login');
        $this->group('buttons', array(
            $this->login,
            $this->note('note1', 'Please note that passwords are case sensitive'),
        ));
        $this->group('links', array(
            $this->link('register', array('_user', 'action' => 'register'), 'Register a new account'),
            $this->link('recover', array('_user', 'action' => 'recover'), 'I have lost my account details'),
        ));
    }
}
