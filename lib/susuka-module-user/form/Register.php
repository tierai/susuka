<?php

namespace susuka\module\user\form;

use \susuka\module\user\model\User as UserModel;

class Register extends \susuka\helper\form\Form {
    public $name;
    public $email;
    public $pass;
    public $pass2;
    public $submit;
    
    function __construct($mapper, $request = null) {
        parent::__construct($request);
        //$this->message = $this->text('message')->visibility('auto');
        $this->name = $this->text('name')->label('Username')->title('Enter your username here')->rule(array('length' => array(1)))->rule(function($e) use($mapper) {
            if($mapper->load(array('name' => $e->val()))) {
                $e->error('Username %1$s is already registered', $e->val());
            }
            return true;
        });
        $this->email = $this->text('email')->label('Email address')->title('Enter your email address here')->rule(array('length' => array(1)))->rule(function($e) use($mapper) {
            if($mapper->load(array('email' => $e->val()))) {
                $e->error('This email address is already registered');
            } else if(!filter_var($e->val(), FILTER_VALIDATE_EMAIL)) {
                $e->error('This is not a valid email address');
            }
            return true;
        });
        $this->pass = $this->password('pass')->label('Password')->title('Enter your password here')->rule(array('length' => array(8)));
        $this->pass2 = $this->password('pass2')->label('Confirm password')->title('Confirm your password')->rule(array('equal' => $this->pass));
        $this->submit = $this->submit('submit', 'Register');
        $this->group('buttons', array(
            $this->submit,
        ));
        $this->csrf(true);
    }
}
