<?php

namespace susuka\module\error\controller;

/**
 * @todo Simple content-template override ($action)
 */
class Index extends \susuka\controller\View {
    
    protected function action($action, array $args) {
        $message = $this->response->_getHttpMessage($action);
        if(is_numeric($action)) {
            $this->response->code($action);
        }
        return array(
            '$title' => array('Error %d', $action),
            '$message' => $message,
        );
    }
    
    protected function renderView($action, array $values) {
        return parent::renderView('index', $values);
    }
}
