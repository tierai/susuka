<?php

/**
 * @file
 *
 * This is the main .php file that is called when a webserver request is made.
 * You can rewrite it, or modify the defines to suit your needs (using SetEnv in Apache).
 *
 */
 
@umask(0000);

$define = function($name, $default = false) {
    if(!defined($name)) {
        $env = getenv($name);
        define($name, $env ? $env : $default);
    }
};

// Include app.user.php if it exists
$define('SU_APP_USER', '.user.php');

if(is_file(SU_APP_USER)) {
    require SU_APP_USER;
}

// Define load timer (false to disable)
$define('SU_DEBUG_TIMER', microtime(true));

// Define the autoloader include (required)
$define('SU_AUTOLOAD_INCLUDE', __DIR__.'/../lib/susuka/autoload.php');

// Define the application name (required)
$define('SU_APP_NAME', 'demo');

// Define the application base path (required)
$define('SU_APP_PATH', __DIR__.'/../app/'.SU_APP_NAME);

// Define the application environment (recommended)
$define('SU_APP_ENVIRONMENT', 'dev');

// Define the public directory (required)
$define('SU_PUBLIC_PATH', __DIR__);

// Override the default file-cache path (recommended)
//$define('SU_CACHE_FILE_PATH', '/tmp/my-awesome-app');

// Avoid accidental public access to a development site, add your local IP here or replace it with something else.
if(SU_APP_ENVIRONMENT === 'dev' && !in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
    header('HTTP/1.0 403 Forbidden');
    die('You are not allowed to access '.basename(__FILE__).' on this server');
}

// Boot and run
try {
    require_once(SU_AUTOLOAD_INCLUDE);
    $app = \susuka\core\Application::boot();
    $app->run();
} catch(\Exception $ex) {
    \suInternalError($ex);
}
